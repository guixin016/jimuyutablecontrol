package com.example.jimuyutabletcontrol;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import com.example.jimuyutabletcontrol.adapter.my_file.MyFileAdapter;
import com.example.jimuyutabletcontrol.dialog.FileDeleteTipDialogFragment;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

public class FileActivity extends AppCompatActivity {

    public static final String KEY_FILE_PATH = "key_file_path";

    public static final String KEY_LIMIT = "key_limit";

    public static final String KEY_RESULT_DATA = "key_result_data";

    private RecyclerView mRecyclerView;

    private TextView stateTip;

    private String filePath;

    private List<String> limits;

    private List<File> files;

    private MyFileAdapter mAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_file);
        filePath = getIntent().getStringExtra(KEY_FILE_PATH);
        limits = getIntent().getStringArrayListExtra(KEY_LIMIT);
        if (TextUtils.isEmpty(filePath)) {
            finish();
            return;
        }
        initView();
        setAdapter();
        setListeners();
        File f = new File(filePath);
        if (!f.exists()) {
            finish();
            return;
        }
        getData(f);
        if (files.size() > 0) {
            mAdapter.notifyDataSetChanged();
        }
        stateTip.setVisibility(files == null || files.size() == 0 ? View.VISIBLE : View.GONE);
    }

    private void setListeners() {
        findViewById(R.id.back).setOnClickListener(v -> finish());
        mAdapter.setOnMyFileListener(new MyFileAdapter.OnMyFileListener() {
            @Override
            public void onMyFileListener(int position) {
                Intent intent = new Intent();
                intent.putExtra(KEY_RESULT_DATA, files.get(position).getAbsolutePath());
                setResult(RESULT_OK, intent);
                finish();
            }

            @Override
            public void onLongClickMyFileListener(int position) {
                showDeleteDialog(position);
            }
        });
    }

    private void showDeleteDialog(int position) {
        FileDeleteTipDialogFragment fileDeleteTipDialogFragment = new FileDeleteTipDialogFragment();
        fileDeleteTipDialogFragment.setOnFileDeleteTipDialogListener(() -> {
            File file = files.get(position);
            boolean isOk = file.delete();
            if (isOk){
                files.remove(file);
                mAdapter.notifyItemRemoved(position);
            }
            Toast.makeText(FileActivity.this, isOk ? "删除成功" : "删除失败", Toast.LENGTH_SHORT).show();
        });
        fileDeleteTipDialogFragment.show(getSupportFragmentManager(),"fileDeleteTipDialogFragment");
    }

    private void setAdapter() {
        files = new ArrayList<>();
        mAdapter = new MyFileAdapter(files);
        mRecyclerView.setAdapter(mAdapter);
    }

    private void initView() {
        mRecyclerView = findViewById(R.id.mRecyclerView);
        ((TextView) findViewById(R.id.title)).setText("文件选择器");
        stateTip = findViewById(R.id.stateTip);
    }

    private void getData(File file) {
        if (null == files) {
            files = new ArrayList<>();
        }
        if (file.isDirectory()) {
            File[] fs;
            if (limits == null || limits.size() == 0) {
                fs = file.listFiles();
            } else {
                fs = file.listFiles(pathname -> {
                    if (pathname.isDirectory()) {
                        return true;
                    }
                    return isOk(pathname);
                });
            }
            if (null != fs && fs.length > 0) {
                for (File f : fs) {
                    if (!f.isDirectory()) {
                        files.add(f);
                    } else {
                        getData(f);
                    }
                }
            }
        } else {
            if (limits == null || limits.size() == 0) {
                files.add(file);
            } else {
                if (isOk(file)) {
                    files.add(file);
                }
            }
        }
    }

    private boolean isOk(File file) {
        boolean ok = true;
        for (String l : limits) {
            if (!file.getName().contains(l)) {
                ok = false;
            }
        }
        return ok;
    }
}
