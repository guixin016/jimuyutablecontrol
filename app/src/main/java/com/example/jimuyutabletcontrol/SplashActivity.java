package com.example.jimuyutabletcontrol;

import android.app.Activity;
import android.content.Intent;
import android.media.MediaPlayer;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationSet;
import android.view.animation.TranslateAnimation;
import android.widget.ImageView;

import com.example.jimuyutabletcontrol.utils.MsgType;
import com.example.jimuyutabletcontrol.utils.Permission;
import com.example.jimuyutabletcontrol.views.explosionfield.ExplosionField;
import com.example.jimuyutabletcontrol.views.wavedrawable.WaveDrawable;

import java.io.IOException;

import androidx.annotation.RequiresApi;


public class SplashActivity extends Activity {
    private ImageView mlabeltiantheView;
    private ImageView mloadauvView;
    private ImageView mAuv1View;
    private ImageView mAuv2View;
    private ImageView mAuv3View;
    private ImageView mAuv4View;
    private ImageView mAuv5View;
    private WaveDrawable mlabeltianheDrawable;
    private WaveDrawable mloadauvDrawable;
    private ExplosionField mExplosionField;
    private static final int WHAT_DELAY = 0x11;
    //private static final int DELAY_TIME = 9500;
    private static final int DELAY_TIME = 500;
    private MediaPlayer mp1 = null;
    private MediaPlayer mp2 = null;

    private static SplashActivity self;
    public static SplashActivity getInstance() {
        return self;
    }

    @RequiresApi(api = Build.VERSION_CODES.O)
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.splashinterface_activity);

        this.self = this;

        Permission.requestPermission(this);

        mExplosionField = ExplosionField.attach2Window(this);

        mlabeltiantheView = findViewById(R.id.label);
        mlabeltianheDrawable = new WaveDrawable(this, R.drawable.labeltianhe);
        mlabeltiantheView.setImageDrawable(mlabeltianheDrawable);
        setlabeltianheIndeterminateMode(true);

        mloadauvView = findViewById(R.id.load);
        mloadauvDrawable = new WaveDrawable(this, R.drawable.loadauv);
        mloadauvView.setImageDrawable(mloadauvDrawable);
        setloadauvIndeterminateMode(true);

        mAuv1View = findViewById(R.id.auv1);
        mAuv2View = findViewById(R.id.auv2);
        mAuv3View = findViewById(R.id.auv3);
        mAuv4View = findViewById(R.id.auv4);
        mAuv5View = findViewById(R.id.auv5);

        mp1 = MediaPlayer.create(this, R.raw.communication);
        mp2 = MediaPlayer.create(this, R.raw.explosion);

        try {
            mp1.prepare();
            mp2.prepare();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (IllegalStateException e) {

        }

        mp1.start();

        new Thread(new Runnable() {
            @Override
            public void run() {
                try {
                    Thread.sleep(3800);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                Message msg = new Message();
                msg.what = MsgType.AUVWAVEDRAWABLESTOP;
                mHandler.sendMessage(msg);
            }
        }).start();

        new Thread(new Runnable() {
            @Override
            public void run() {
                try {
                    Thread.sleep(3000);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                Message msg = new Message();
                msg.what = MsgType.LABELWAVEDRAWABLESTOP;
                mHandler.sendMessage(msg);
            }
        }).start();

        mHandler.sendEmptyMessageDelayed(WHAT_DELAY, DELAY_TIME);
    }

    private void setlabeltianheIndeterminateMode(boolean indeterminate) {
        mlabeltianheDrawable.setIndeterminate(indeterminate);

        mlabeltianheDrawable.setWaveAmplitude(40);
        mlabeltianheDrawable.setWaveLength(210);
        mlabeltianheDrawable.setWaveSpeed(3);
    }

    private void setloadauvIndeterminateMode(boolean indeterminate) {
        mloadauvDrawable.setIndeterminate(indeterminate);

        mloadauvDrawable.setWaveAmplitude(40);
        mloadauvDrawable.setWaveLength(210);
        mloadauvDrawable.setWaveSpeed(5);
    }

    private AnimationSet setSelfAnimation(int duration, float xleft, float xright, float yleft, float yright) {
        AnimationSet animationSet = new AnimationSet(true);
        TranslateAnimation translateAnimation = new TranslateAnimation(
                Animation.RELATIVE_TO_SELF, xleft,
                Animation.RELATIVE_TO_SELF, xright,
                Animation.RELATIVE_TO_SELF, yleft,
                Animation.RELATIVE_TO_SELF, yright);
        animationSet.setFillAfter(true);
        animationSet.setDuration(duration);
        animationSet.addAnimation(translateAnimation);
        return animationSet;
    }

    private void goHome() {
        startActivity(new Intent(SplashActivity.this, MainActivity.class));
        finish();// 销毁当前活动界面
    }

    private Handler mHandler = new Handler() {
        @Override
        public void handleMessage(Message msg) {
            switch (msg.what) {
                case MsgType.LABELWAVEDRAWABLESTOP:
                    mlabeltianheDrawable.setIndeterminate(false);
                    mlabeltianheDrawable.setVisible(true, false);
                    break;
                case MsgType.AUVWAVEDRAWABLESTOP:
                    mloadauvDrawable.setIndeterminate(false);
                    mloadauvDrawable.setVisible(true, false);
                    mloadauvView.setImageDrawable(getResources().getDrawable(R.drawable.loadauv));
                    View v = findViewById(R.id.load);
                    mExplosionField.explode(v);
                    mp2.start();
                    v.setOnClickListener(null);
                    mAuv1View.setImageDrawable(getResources().getDrawable(R.drawable.auv1));
                    mAuv2View.setImageDrawable(getResources().getDrawable(R.drawable.auv2));
                    mAuv3View.setImageDrawable(getResources().getDrawable(R.drawable.auv3));
                    mAuv4View.setImageDrawable(getResources().getDrawable(R.drawable.auv4));
                    mAuv5View.setImageDrawable(getResources().getDrawable(R.drawable.auv5));
                    Message msg1 = new Message();
                    msg1.what = MsgType.AUVSMOVE;
                    mHandler.sendMessage(msg1);
                    break;
                case MsgType.AUVSMOVE:
                    mAuv1View.setAnimation(setSelfAnimation(4000, 0.0f, 3.4f, 0.0f, 0.9f));
                    mAuv2View.startAnimation(setSelfAnimation(3500, 0.0f, 1.9f, 0.0f, 0.2f));
                    mAuv3View.startAnimation(setSelfAnimation(4000, 0.0f, 1.2f, 0.0f, 0.7f));
                    mAuv4View.startAnimation(setSelfAnimation(4500, 0.0f, 2.0f, 0.0f, -0.2f));
                    mAuv5View.startAnimation(setSelfAnimation(5000, 0.0f, 1.34f, 0.0f, 0.3f));
                    break;
                case WHAT_DELAY:
                    goHome();
                    break;
            }
            super.handleMessage(msg);
        }
    };

    @Override
    public void onDestroy() {
        super.onDestroy();
        mp1.stop();
        mp2.stop();
    }
}
