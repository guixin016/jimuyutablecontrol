package com.example.jimuyutabletcontrol.activity;

import android.graphics.Color;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.Toast;

import com.example.jimuyutabletcontrol.R;
import com.example.jimuyutabletcontrol.utils.Constants;
import com.github.mikephil.charting.charts.LineChart;
import com.github.mikephil.charting.data.Entry;
import com.github.mikephil.charting.data.LineData;
import com.github.mikephil.charting.data.LineDataSet;
import com.github.mikephil.charting.interfaces.datasets.ILineDataSet;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

public class AnalysingSensorDHActivity extends AppCompatActivity {
    private File file;
    private LineChart lineChart;
    private BufferedReader bufferedReader;
    private int number = 0;
    private HashMap<Integer, List<Entry>> hashMap;
    private int count = 0;
    private ArrayList<String> title;
    private ArrayList<String> color;
    private ArrayList<ILineDataSet> dataSets;

    private CheckBox cbYaw;
    private CheckBox cbPitch;
    private CheckBox cbRoll;
    private CheckBox cbDepth;
    private CheckBox cbTopRudder;
    private CheckBox cbBottomRudder;
    private CheckBox cbLeftRudder;
    private CheckBox cbRightRudder;
    private CheckBox cbRotation;
    private Button submit;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.analysingsensordhinterface_activity);

        cbYaw = findViewById(R.id.yawdh);
        cbPitch = findViewById(R.id.pitchdh);
        cbRoll = findViewById(R.id.rolldh);
        cbDepth = findViewById(R.id.depthdh);
        cbTopRudder = findViewById(R.id.toprudderdh);
        cbBottomRudder = findViewById(R.id.bottomrudderdh);
        cbLeftRudder = findViewById(R.id.leftrudderdh);
        cbRightRudder = findViewById(R.id.rightrudderdh);
        cbRotation = findViewById(R.id.rotationdh);
        submit = findViewById(R.id.submitdh);

        lineChart = findViewById(R.id.lineChartdh);
        lineChart.getDescription().setEnabled(false);
        String fileName = getIntent().getStringExtra("fileName");
        file = new File(Constants.FILE_PATH + Constants.DH + fileName);
        if (file.exists()) {
            loadAvailableSensor();
            loadResource();
            setClickListener();
        } else {
            toastMsg("请确认所选的文件是否存在！！！");
        }
    }

    private void loadResource() {
        hashMap = new HashMap<>();
        loadSensorValue();

        title = new ArrayList<>();
        title.add(0, "Yaw");
        title.add(1, "Pitch");
        title.add(2, "Roll");
        title.add(3, "Depth");
        title.add(4, "TopRudder");
        title.add(5, "BottomRudder");
        title.add(6, "LeftRudder");
        title.add(7, "RightRudder");
        title.add(8, "Rotation");


        color = new ArrayList<>();
        color.add(0,"#0099EE");
        color.add(1,"#ff5500");
        color.add(2,"#FFFF33");
        color.add(3,"#99FFFF");
        color.add(4,"#FFCCCC");
        color.add(5,"#0033CC");
        color.add(6,"#330033");
        color.add(7,"#99CCCC");
        color.add(8,"#CCFFCC");

        dataSets = new ArrayList<ILineDataSet>();
    }

    private void setClickListener() {

        submit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String[] content = new String[9];
                if (cbYaw.isChecked()) {
                    content[0] = "0";
                }
                if (cbPitch.isChecked()) {
                    content[1] = "1";
                }
                if (cbRoll.isChecked()) {
                    content[2] = "2";
                }
                if (cbDepth.isChecked()) {
                    content[3] = "3";
                }
                if (cbTopRudder.isChecked()) {
                    content[4] = "4";
                }
                if (cbBottomRudder.isChecked()) {
                    content[5] = "5";
                }
                if (cbLeftRudder.isChecked()) {
                    content[6] = "6";
                }
                if (cbRightRudder.isChecked()) {
                    content[7] = "7";
                }
                if (cbRotation.isChecked()) {
                    content[8] = "8";
                }

                ArrayList<ILineDataSet> dataSets = new ArrayList<ILineDataSet>();
                int length = content.length;
                for (int i = 0; i < length; i++) {
                    if (content[i] != null) {
                        int tip = Integer.parseInt(content[i]);
                        Log.e("dasdasdasd",hashMap.get(tip).size()+"");
                        LineDataSet dataSet = new LineDataSet(hashMap.get(tip), title.get(tip));
                        dataSet.setColor(Color.parseColor(color.get(tip)));
                        dataSet.setCircleHoleRadius(1.8f);
                        dataSet.setColor(Color.parseColor(color.get(tip)));
                        dataSet.setLineWidth(1f);
                        dataSets.add(dataSet);
                    }
                }
                LineData data = new LineData(dataSets);
                lineChart.setData(data);
                lineChart.invalidate();
            }
        });
    }

    private void loadSensorValue() {
        for (int i = 0; i < number; i ++) {
            List<Entry> entries = new ArrayList<>();
            hashMap.put(i, entries);
        }
        if (number == 9) {
            List<Entry> entries = new ArrayList<>();
            hashMap.put(12, entries);
        }

        try {
            String line = bufferedReader.readLine();

            int index = 1;
            while (line != null) {
                String[] content = line.split(" ");

                for (int i = 0; i < number; i ++) {
                    hashMap.get(i).add(new Entry(count, Float.parseFloat(content[i])));
                }
                if (number == 9) {
                    hashMap.get(12).add(new Entry(count, Float.parseFloat(content[15])));
                }
                count ++;
                line = bufferedReader.readLine();
                index+=1;
            }
            Log.e("阿达所多啊所",(index-1)+"");
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private void loadAvailableSensor() {
        try {
            bufferedReader = new BufferedReader(new FileReader(file));
            StringBuilder sb = new StringBuilder();
            String[] content = bufferedReader.readLine().split(" ");
            number = content.length;
            int i = 0;
            for (i = 0; i < content.length; i ++) {
                sb.append(i +" "+content[i] + (i+1==content.length?"":","));
            }
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private Handler mHandler = new Handler() {

        @Override
        public void handleMessage(Message msg) {
            super.handleMessage(msg);
        }
    };

    public void sendMsgToMainThread(int what, String obj) {
        Message message = mHandler.obtainMessage();
        message.what = what;
        message.obj = obj;
        mHandler.sendMessage(message);
    }

    private void toastMsg(final String msg) {
        mHandler.post(new Runnable() {
            @Override
            public void run() {
                Toast.makeText(getApplicationContext(), msg, Toast.LENGTH_SHORT).show();
            }
        });
    }
}
