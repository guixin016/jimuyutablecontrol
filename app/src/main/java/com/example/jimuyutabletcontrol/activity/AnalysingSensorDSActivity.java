package com.example.jimuyutabletcontrol.activity;

import android.graphics.Color;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.Toast;

import com.example.jimuyutabletcontrol.R;
import com.example.jimuyutabletcontrol.utils.Constants;
import com.github.mikephil.charting.charts.LineChart;
import com.github.mikephil.charting.data.Entry;
import com.github.mikephil.charting.data.LineData;
import com.github.mikephil.charting.data.LineDataSet;
import com.github.mikephil.charting.interfaces.datasets.ILineDataSet;
import com.google.gson.Gson;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

public class AnalysingSensorDSActivity extends AppCompatActivity {
    private File file;
    private LineChart lineChart;
    private BufferedReader bufferedReader;
    private int number = 0;
    private HashMap<Integer, List<Entry>> hashMap;
    private int count = 0;
    private ArrayList<String> title;
    private ArrayList<String> color;
    private ArrayList<ILineDataSet> dataSets;

    private CheckBox cbYaw;
    private CheckBox cbPitch;
    private CheckBox cbRoll;
    private CheckBox cbDepth;
    private CheckBox cbTopRudder;
    private CheckBox cbBottomRudder;
    private CheckBox cbLeftRudder;
    private CheckBox cbRightRudder;
    private CheckBox cbRotation;
    private Button submit;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.analysingsensordsinterface_activity);

        cbYaw = findViewById(R.id.yawds);
        cbPitch = findViewById(R.id.pitchds);
        cbRoll = findViewById(R.id.rollds);
        cbDepth = findViewById(R.id.depthds);
        cbTopRudder = findViewById(R.id.toprudderds);
        cbBottomRudder = findViewById(R.id.bottomrudderds);
        cbLeftRudder = findViewById(R.id.leftrudderds);
        cbRightRudder = findViewById(R.id.rightrudderds);
        cbRotation = findViewById(R.id.rotationds);
        submit = findViewById(R.id.submitds);

        lineChart = findViewById(R.id.lineChartds);
        lineChart.getDescription().setEnabled(false);

        String fileName = getIntent().getStringExtra("fileName");

        file = new File(Constants.FILE_PATH + Constants.DS + fileName);


        if (file.exists()) {
            loadAvailableSensor();
            loadResource();
            setClickListener();

        } else {
            toastMsg("请确认所选的文件是否存在！！！");
        }
    }

    private void loadResource() {
        hashMap = new HashMap<>();
        loadSensorValue();
        title = new ArrayList<>();
        title.add(0, "Yaw");
        title.add(1, "Pitch");
        title.add(2, "Roll");
        title.add(3, "Depth");
        title.add(4, "TopRudder");
        title.add(5, "BottomRudder");
        title.add(6, "LeftRudder");
        title.add(7, "RightRudder");
        title.add(8, "Rotation");
//        title.add(9, "Chadong");
//        title.add(10,"AUVBattery");
        //title.add(12,"Speed");

        color = new ArrayList<>();
        color.add(0,"#0099EE");
        color.add(1,"#ff5500");
        color.add(2,"#FFFF33");
        color.add(3,"#99FFFF");
        color.add(4,"#FFCCCC");
        color.add(5,"#0033CC");
        color.add(6,"#330033");
        color.add(7,"#99CCCC");
        color.add(8,"#CCFFCC");
//        color.add(9,"#FF66FF");
//        color.add(10,"#CCCC00");
        //color.add(12,"#999999");

        dataSets = new ArrayList<ILineDataSet>();
    }

    private void setClickListener() {
        submit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String[] content = new String[11];
                if (cbYaw.isChecked()) {
                    content[0] = "0";
                }
                if (cbPitch.isChecked()) {
                    content[1] = "1";
                }
                if (cbRoll.isChecked()) {
                    content[2] = "2";
                }
                if (cbDepth.isChecked()) {
                    content[3] = "3";
                }
                if (cbTopRudder.isChecked()) {
                    content[4] = "4";
                }
                if (cbBottomRudder.isChecked()) {
                    content[5] = "5";
                }
                if (cbLeftRudder.isChecked()) {
                    content[6] = "6";
                }
                if (cbRightRudder.isChecked()) {
                    content[7] = "7";
                }
                if (cbRotation.isChecked()) {
                    content[8] = "8";
                }
                ArrayList<ILineDataSet> dataSets = new ArrayList<>();
                int length = content.length;
                for (int i = 0; i < length; i++) {
                    if (content[i] != null) {
                        int tip = Integer.parseInt(content[i]);
                        LineDataSet dataSet = new LineDataSet(hashMap.get(tip), title.get(tip));
                        dataSet.setCircleColor(Color.parseColor(color.get(tip)));
                        dataSet.setColor(Color.parseColor(color.get(tip)));
                        dataSet.setCircleHoleRadius(1.8f);
                        dataSet.setLineWidth(1f);
                        dataSets.add(dataSet);
                    }
                }
                LineData data = new LineData(dataSets);
                lineChart.setData(data);
                lineChart.invalidate();
            }
        });
    }

    private void loadSensorValue() {
        for (int i = 0; i < number; i ++) {
            List<Entry> entries = new ArrayList<>();
            hashMap.put(i, entries);
        }
        if (number == 9) {
            List<Entry> entries = new ArrayList<>();
            hashMap.put(12, entries);
        }
        try {
            String line = bufferedReader.readLine();
            while (line != null) {
                String[] content = line.split(" ");
                for (int i = 0; i < number; i ++) {
                    hashMap.get(i).add(new Entry(count, Float.parseFloat(content[i])));
                }
                if (number == 9) {
                    hashMap.get(12).add(new Entry(count, Float.parseFloat(content[15])));
                }
                count ++;
                line = bufferedReader.readLine();
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private void loadAvailableSensor() {
        try {
            bufferedReader = new BufferedReader(new FileReader(file));
            StringBuilder sb = new StringBuilder();
            String[] content = bufferedReader.readLine().split(" ");
            number = content.length;
            int i = 0;
            for (i = 0; i < content.length - 1; i ++) {
                sb.append(i +" "+content[i] + ",");
            }
            sb.append(i + " " + content[i]);
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private void toastMsg(final String msg) {
        runOnUiThread(()->Toast.makeText(getApplicationContext(), msg, Toast.LENGTH_SHORT).show());
    }
}
