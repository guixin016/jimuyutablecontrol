package com.example.jimuyutabletcontrol.activity;

import android.animation.AnimatorSet;
import android.animation.ValueAnimator;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.Point;
import android.os.Bundle;
import android.view.Display;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;

import com.example.jimuyutabletcontrol.R;
import com.example.jimuyutabletcontrol.adapter.selectAUVMakeMap.SelectAUVMakeMapAdapter;
import com.example.jimuyutabletcontrol.network.udp.UdpMessageHandle;
import com.example.jimuyutabletcontrol.network.udp.UdpMessageType;
import com.example.jimuyutabletcontrol.network.udp.UdpSocket;
import com.scwang.smartrefresh.layout.SmartRefreshLayout;
import com.scwang.smartrefresh.layout.api.RefreshLayout;
import com.scwang.smartrefresh.layout.listener.OnRefreshLoadmoreListener;

import java.util.ArrayList;
import java.util.List;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.recyclerview.widget.RecyclerView;

public class SelectAUVMakeMapActivity extends AppCompatActivity implements
        Toolbar.OnMenuItemClickListener, View.OnClickListener, OnRefreshLoadmoreListener {

    private UdpMessageHandle handle;

    private Toolbar toolbar;

    private SmartRefreshLayout mSmartRefreshLayout;

    private RecyclerView mRecyclerView;

    private View noData;

    private List<String> infos;

    private SelectAUVMakeMapAdapter adapter;

    /**
     * ==0,下拉刷新；==1，上拉加载
     */
    private int loadType = 0;


    private Point[] pentagonVertices;
    private Button[] buttons;
    private int width;
    private int radius;
    private int startPositionX;
    private int startPositionY;
    private int whichAnimation;
    private int NUM_OF_SIDES;
    private int[] enterDelay = {80, 120, 160, 40, 0, 200, 240};
    private int[] exitDelay = {80, 40, 0, 120, 160, 200, 240};
    private String[] content = {"修改任务", "发送任务", "实地测绘"};

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.selectauvmakemapinterface_pull_to_refresh);
        initView();
        setAdapter();
        setListeners();
        mSmartRefreshLayout.autoRefresh();
        prepareFloatButton();
    }

    private void setAdapter() {
        infos = new ArrayList<>();
        adapter = new SelectAUVMakeMapAdapter(infos);
        mRecyclerView.setAdapter(adapter);
    }

    private void initView() {
        toolbar = findViewById(R.id.toolbarmakemap);
        toolbar.setTitle("AUV导航任务制作");
        setSupportActionBar(toolbar);
        mSmartRefreshLayout = findViewById(R.id.mSmartRefreshLayout);
        mRecyclerView = findViewById(R.id.mRecyclerView);
        noData = findViewById(R.id.noData);
    }

    private void setListeners() {
        if (toolbar != null) {
            toolbar.setOnMenuItemClickListener(this);
        }
        handle = new UdpMessageHandle(UdpMessageType.IMEI, msg -> {
            if (loadType == 0) {
                mSmartRefreshLayout.finishRefresh(true);
            } else {
                mSmartRefreshLayout.finishLoadmore(true);
            }
            updateListData(msg);
        });
        UdpSocket.getInstance().setMessageCallback(handle);
        adapter.setOnSelectAUVMakeMapListener(position -> {
            if (position < infos.size()) {
                Intent intent = new Intent(SelectAUVMakeMapActivity.this, MakeTaskMapActivity.class);
                intent.putExtra("IMEI", infos.get(position));
                startActivity(intent);
            }
        });
        mSmartRefreshLayout.setOnRefreshLoadmoreListener(this);
    }


    @Override
    public void onLoadmore(RefreshLayout refreshlayout) {
        UdpSocket.getInstance().sendMessage("INSTANTIMEI");
        loadType = 1;
    }

    @Override
    public void onRefresh(RefreshLayout refreshlayout) {
        UdpSocket.getInstance().sendMessage("INSTANTIMEI");
        loadType = 0;
    }

    private void updateListData(String message) {
        runOnUiThread(() -> {
            String[] contents = message.split(",");
            List<String> strs = new ArrayList<>();
            for (int i = 1; i < contents.length; i++) {
                if (!infos.contains(contents[i])) {
                    strs.add(contents[i]);
                }
            }
            if (strs.size() > 0) {
                int refreshindex = infos.size();
                infos.addAll(strs);
                adapter.notifyItemRangeInserted(refreshindex, strs.size());
            }
            noData.setVisibility((null == infos || infos.size() == 0) ? View.VISIBLE : View.GONE);
        });
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.selectauvmakemapinterface_menu, menu);
        return true;
    }

    @Override
    public boolean onMenuItemClick(MenuItem menuItem) {
        if (menuItem.getItemId() == R.id.auvmakemap_clear) {
            if (null != infos && infos.size() > 0) {
                infos.clear();
                adapter.notifyDataSetChanged();
                mSmartRefreshLayout.autoRefresh();
            }
        } else if (menuItem.getItemId() == R.id.send_makemap) {
            if (whichAnimation == 0) {
                startPositionX = 2600;
                startPositionY = 0;
                for (Button button : buttons) {
                    button.setX(startPositionX);
                    button.setY(startPositionY);
                    button.setVisibility(View.VISIBLE);
                }
                for (int i = 0; i < buttons.length; i++) {
                    playEnterAnimation(buttons[i], i);
                }
                whichAnimation = 1;
            } else {
                for (int i = 0; i < buttons.length; i++) {
                    playExitAnimation(buttons[i], i);
                }
                whichAnimation = 0;
            }
            return true;
        }
        return false;
    }

    @Override
    public void onClick(View view) {
        Intent intent = null;
        switch ((int) view.getTag()) {
            case 0:
                intent = new Intent(SelectAUVMakeMapActivity.this, SelectAUVFixMapActivity.class);
                startActivity(intent);
                finish();
                break;
            case 1:
                intent = new Intent(SelectAUVMakeMapActivity.this, SelectAUVSendMapActivity.class);
                startActivity(intent);
                finish();
                break;
            case 2:
                intent = new Intent(SelectAUVMakeMapActivity.this, MappingActivity.class);
                startActivity(intent);
                finish();
                break;
        }
    }

    private void prepareFloatButton() {
        startPositionX = 0;
        startPositionY = 0;
        whichAnimation = 0;
        NUM_OF_SIDES = 3;
        width = 200;
        radius = 200;
        calculatePentagonVertices(radius, 11);
    }

    private void calculatePentagonVertices(int radius, int rotation) {
        pentagonVertices = new Point[NUM_OF_SIDES];
        Display display = getWindowManager().getDefaultDisplay();
        int centerX = display.getWidth() / 2;
        int centerY = display.getHeight() / 2;
        for (int i = 0; i < NUM_OF_SIDES; i++) {
            pentagonVertices[i] = new Point((int) (radius * Math.cos(rotation + i * 2 * Math.PI / NUM_OF_SIDES)) + centerX,
                    (int) (radius * Math.sin(rotation + i * 2 * Math.PI / NUM_OF_SIDES)) + centerY - 100);
        }
        buttons = new Button[pentagonVertices.length];
        for (int i = 0; i < buttons.length; i++) {
            buttons[i] = new Button(SelectAUVMakeMapActivity.this);
            buttons[i].setLayoutParams(new RelativeLayout.LayoutParams(100, 100));
            buttons[i].setX(0);
            buttons[i].setY(0);
            buttons[i].setTag(i);
            buttons[i].setOnClickListener(this);
            buttons[i].setVisibility(View.INVISIBLE);
            buttons[i].setBackgroundResource(R.drawable.circular_background);
            buttons[i].setTextColor(Color.DKGRAY);
            buttons[i].setText(content[i]);
            buttons[i].setTextSize(14);
            ((LinearLayout) findViewById(R.id.main_makemap)).addView(buttons[i]);
        }
    }

    private void playEnterAnimation(final Button button, int position) {
        AnimatorSet buttonAnimator = new AnimatorSet();
        ValueAnimator buttonAnimatorX = ValueAnimator.ofFloat(startPositionX + button.getLayoutParams().width / 2,
                pentagonVertices[position].x);
        buttonAnimatorX.addUpdateListener(animation -> {
            button.setX((float) animation.getAnimatedValue() - button.getLayoutParams().width / 2);
            button.requestLayout();
        });
        buttonAnimatorX.setDuration(300);
        ValueAnimator buttonAnimatorY = ValueAnimator.ofFloat(startPositionY + 5,
                pentagonVertices[position].y);
        buttonAnimatorY.addUpdateListener(animation -> {
            button.setY((float) animation.getAnimatedValue());
            button.requestLayout();
        });
        buttonAnimatorY.setDuration(300);
        ValueAnimator buttonSizeAnimator = ValueAnimator.ofInt(5, width);
        buttonSizeAnimator.addUpdateListener(animation -> {
            button.getLayoutParams().width = (int) animation.getAnimatedValue();
            button.getLayoutParams().height = (int) animation.getAnimatedValue();
            button.requestLayout();
        });
        buttonSizeAnimator.setDuration(300);
        buttonAnimator.play(buttonAnimatorX).with(buttonAnimatorY).with(buttonSizeAnimator);
        buttonAnimator.setStartDelay(enterDelay[position]);
        buttonAnimator.start();
    }

    private void playExitAnimation(final Button button, int position) {
        AnimatorSet buttonAnimator = new AnimatorSet();
        ValueAnimator buttonAnimatorX = ValueAnimator.ofFloat(pentagonVertices[position].x - button.getLayoutParams().width / 2,
                startPositionX);
        buttonAnimatorX.addUpdateListener(animation -> {
            button.setX((float) animation.getAnimatedValue());
            button.requestLayout();
        });
        buttonAnimatorX.setDuration(300);
        ValueAnimator buttonAnimatorY = ValueAnimator.ofFloat(pentagonVertices[position].y,
                startPositionY + 5);
        buttonAnimatorY.addUpdateListener(animation -> {
            button.setY((float) animation.getAnimatedValue());
            button.requestLayout();
        });
        buttonAnimatorY.setDuration(300);
        ValueAnimator buttonSizeAnimator = ValueAnimator.ofInt(width, 5);
        buttonSizeAnimator.addUpdateListener(animation -> {
            button.getLayoutParams().width = (int) animation.getAnimatedValue();
            button.getLayoutParams().height = (int) animation.getAnimatedValue();
            button.requestLayout();
        });
        buttonSizeAnimator.setDuration(300);
        buttonAnimator.play(buttonAnimatorX).with(buttonAnimatorY).with(buttonSizeAnimator);
        buttonAnimator.setStartDelay(exitDelay[position]);
        buttonAnimator.start();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if (null != handle) {
            UdpSocket.getInstance().removeMessageCallback(handle);
            handle = null;
        }
    }

}

