package com.example.jimuyutabletcontrol.activity;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.recyclerview.widget.RecyclerView;

import android.graphics.Rect;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;

import com.example.jimuyutabletcontrol.R;
import com.example.jimuyutabletcontrol.adapter.filedetail.TaskDetailAdapter;
import com.example.jimuyutabletcontrol.bean.MakeTaskMapPoint;
import com.example.jimuyutabletcontrol.bean.TaskDetailInfo;
import com.example.jimuyutabletcontrol.utils.DensityUtil;
import com.example.jimuyutabletcontrol.utils.TaskUtil;

import java.util.ArrayList;
import java.util.List;

public class TaskDetailActivity extends AppCompatActivity {

    public static final String KEY_PARAMS = "key_params"+TaskDetailActivity.class.getSimpleName();

    private RecyclerView recyclerView;

    private TaskDetailAdapter mAdapter;

    private List<MakeTaskMapPoint.PackageAuxiliaryPoint> infos;

    private String url;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_task_detail);
        Toolbar toolbar = findViewById(R.id.toolbarsail);
        toolbar.setTitle("AUV任务详情");
        setSupportActionBar(toolbar);
        url = getIntent().getStringExtra(KEY_PARAMS);
        if (TextUtils.isEmpty(url)){
            finish();
            return;
        }
        initView();
        initRecy();
        setAdapter();
        getData();
    }

    private void initRecy() {
        recyclerView.addItemDecoration(new RecyclerView.ItemDecoration() {
            @Override
            public void getItemOffsets(@NonNull Rect outRect, @NonNull View view, @NonNull RecyclerView parent, @NonNull RecyclerView.State state) {
                super.getItemOffsets(outRect, view, parent, state);
                int index = parent.getChildAdapterPosition(view);
                if (index == 0){
                    outRect.top = DensityUtil.dp2px(TaskDetailActivity.this,5);
                } else {
                    outRect.top = 0;
                }
                outRect.bottom = DensityUtil.dp2px(TaskDetailActivity.this,5);
            }
        });
    }

    private void getData() {
        List<MakeTaskMapPoint.PackageAuxiliaryPoint> list = TaskUtil.getTaskWithTaskFile(url);
        infos.addAll(list);
        mAdapter.notifyDataSetChanged();
    }

    private void setAdapter() {
        infos = new ArrayList<>();
        mAdapter = new TaskDetailAdapter(infos);
        recyclerView.setAdapter(mAdapter);
    }

    private void initView() {
        recyclerView = findViewById(R.id.recyclerView);
    }
}
