package com.example.jimuyutabletcontrol.adapter.clubadapter;

public class Club
{
    public String taskPoint;
    public String latitude;
    public String longtitude;
    public String speed;
    public String depth;

    public Club(String taskPoint, String latitude, String longtitude, String speed, String depth)
    {
        this.taskPoint= taskPoint;
        this.latitude = latitude;
        this.longtitude = longtitude;
        this.speed = speed;
        this.depth = depth;
    }
}
