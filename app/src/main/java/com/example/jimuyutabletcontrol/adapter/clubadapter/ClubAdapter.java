package com.example.jimuyutabletcontrol.adapter.clubadapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Filterable;
import android.widget.TextView;
import android.widget.Filter;

import com.example.jimuyutabletcontrol.R;

import java.util.ArrayList;
import java.util.List;

import androidx.recyclerview.widget.RecyclerView;

public class ClubAdapter extends RecyclerView.Adapter<ClubAdapter.ClubViewHolder> implements Filterable
{
    private static final int TYPE_ROW = 0;
    private static final int TYPE_ROW_COLORFUL = 1;

    private List<Club> clubList;
    private List<Club> filteredClubList;
    private Context context;

    public ClubAdapter(Context context, List<Club> clubList)
    {
        this.context = context;
        this.clubList = clubList;
        this.filteredClubList = clubList;
    }

    @Override
    public int getItemViewType(int position)
    {
        if (position % 2 == 0)
        {
            return TYPE_ROW_COLORFUL;
        }

        return TYPE_ROW;
    }

    @Override
    public ClubViewHolder onCreateViewHolder(ViewGroup viewGroup, int viewType)
    {
        if (viewType == TYPE_ROW)
        {
            View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.checktaskdata_row_club, viewGroup, false);
            return new ClubViewHolder(view);
        } else
        {
            View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.checktaskdata_row_club_colorful,
                    viewGroup, false);
            return new ClubViewHolder(view);
        }
    }

    @Override
    public void onBindViewHolder(ClubViewHolder holder, int position)
    {
        Club club = filteredClubList.get(position);

        holder.taskPoint.setText(club.taskPoint);
        holder.latitude.setText(club.latitude);
        holder.longtitude.setText(club.longtitude);
        holder.speed.setText(club.speed);
        holder.depth.setText(club.depth);
    }

    @Override
    public int getItemCount()
    {
        return filteredClubList.size();
    }

    public class ClubViewHolder extends RecyclerView.ViewHolder
    {
        public TextView taskPoint, latitude, longtitude, speed, depth;

        public ClubViewHolder(View view)
        {
            super(view);
            taskPoint = view.findViewById(R.id.taskpoint);
            latitude = view.findViewById(R.id.latitude);
            longtitude = view.findViewById(R.id.longtitude);
            speed = view.findViewById(R.id.speed);
            depth = view.findViewById(R.id.depth);

        }
    }

    @Override
    public Filter getFilter()
    {
        return new Filter()
        {
            @Override
            protected FilterResults performFiltering(CharSequence charSequence)
            {
                String charString = charSequence.toString();
                if (charString.isEmpty())
                {
                    filteredClubList = clubList;
                } else
                {
                    List<Club> filteredList = new ArrayList<>();
                    for (Club club : clubList)
                    {
                        // name match condition. this might differ depending on your requirement
                        // here we are looking for name
                        if (club.taskPoint.toLowerCase().contains(charString.toLowerCase()) )
                        {
                            filteredList.add(club);
                        }
                    }

                    filteredClubList = filteredList;
                }

                FilterResults filterResults = new FilterResults();
                filterResults.values = filteredClubList;
                return filterResults;
            }

            @Override
            protected void publishResults(CharSequence charSequence, FilterResults filterResults)
            {
                filteredClubList = (ArrayList<Club>) filterResults.values;

                // refresh the list with filtered data
                notifyDataSetChanged();
            }
        };
    }
}
