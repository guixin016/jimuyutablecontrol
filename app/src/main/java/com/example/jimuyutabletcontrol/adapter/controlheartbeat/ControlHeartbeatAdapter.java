package com.example.jimuyutabletcontrol.adapter.controlheartbeat;

import android.view.LayoutInflater;
import android.view.ViewGroup;

import com.example.jimuyutabletcontrol.R;
import com.example.jimuyutabletcontrol.base.BaseViewHolder;
import com.example.jimuyutabletcontrol.bean.HeartbeatInfo;

import java.util.List;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

/**
 * @author: HDX
 * @date: 2020\5\12 0012
 */
public class ControlHeartbeatAdapter extends RecyclerView.Adapter<BaseViewHolder<HeartbeatInfo>> {

    private List<HeartbeatInfo> myOverlays;

    public ControlHeartbeatAdapter(List<HeartbeatInfo> myOverlays) {
        this.myOverlays = myOverlays;
    }

    @NonNull
    @Override
    public BaseViewHolder<HeartbeatInfo> onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        return new ControlHeartbeatViewHolder(LayoutInflater.from(viewGroup.getContext())
                .inflate(R.layout.control_hearbeat_item, viewGroup, false));
    }

    @Override
    public void onBindViewHolder(@NonNull BaseViewHolder<HeartbeatInfo> myOverlayBaseViewHolder, int i) {
        myOverlayBaseViewHolder.setLayoutInfo(myOverlays.get(i));
    }

    @Override
    public void onBindViewHolder(@NonNull BaseViewHolder<HeartbeatInfo> holder, int position, @NonNull List<Object> payloads) {
        super.onBindViewHolder(holder, position, payloads);
        if (payloads.size() == 0) {
            onBindViewHolder(holder, position);
        } else {
            Object value = payloads.get(0);
            if (value instanceof Integer) {
                holder.setLayoutInfo(myOverlays.get(position), (int) value);
            } else {
                onBindViewHolder(holder, position);
            }
        }
    }

    @Override
    public int getItemCount() {
        return myOverlays.size();
    }
}
