package com.example.jimuyutabletcontrol.adapter.controlheartbeat;

import android.view.View;
import android.widget.TextView;

import com.example.jimuyutabletcontrol.MapApplication;
import com.example.jimuyutabletcontrol.R;
import com.example.jimuyutabletcontrol.base.BaseViewHolder;
import com.example.jimuyutabletcontrol.bean.HeartbeatInfo;
import com.example.jimuyutabletcontrol.bean.MyOverlay;

/**
 * @author: HDX
 * @date: 2020\5\12 0012
 */
public class ControlHeartbeatViewHolder extends BaseViewHolder<HeartbeatInfo> {

    private TextView tv;

    private TextView tvLowerComputerElectricQuantity;

    private TextView tvAuvElectricQuantity;

    public ControlHeartbeatViewHolder(View itemView) {
        super(itemView);
        initView();
    }

    private void initView() {
        tv = itemView.findViewById(R.id.tv);
        tvLowerComputerElectricQuantity = itemView.findViewById(R.id.tvLowerComputerElectricQuantity);
        tvAuvElectricQuantity = itemView.findViewById(R.id.tvAuvElectricQuantity);
    }

    @Override
    public void setLayoutInfo(HeartbeatInfo info) {
        tv.setText("AUV："+info.imei+"("+ info.number +")");
        tvLowerComputerElectricQuantity.setVisibility(info.isShwoElectricQuantity?View.VISIBLE:View.GONE);
        tvAuvElectricQuantity.setVisibility(info.isShwoElectricQuantity?View.VISIBLE:View.GONE);
        if (tvLowerComputerElectricQuantity.getVisibility() == View.VISIBLE){
            tvLowerComputerElectricQuantity.setText("AUV控制单元电量："+info.lowerComputerElectricQuantity);
        }
        if (tvAuvElectricQuantity.getVisibility() == View.VISIBLE){
            tvAuvElectricQuantity.setText("AUV动力单元电量："+info.auvElectricQuantity);
        }
    }

    @Override
    public void setLayoutInfo(HeartbeatInfo info, int tag) {
        super.setLayoutInfo(info, tag);
        if (tag == 1){
            tvLowerComputerElectricQuantity.setVisibility(info.isShwoElectricQuantity?View.VISIBLE:View.GONE);
            tvAuvElectricQuantity.setVisibility(info.isShwoElectricQuantity?View.VISIBLE:View.GONE);
            if (tvLowerComputerElectricQuantity.getVisibility() == View.VISIBLE){
                tvLowerComputerElectricQuantity.setText("AUV控制单元电量："+info.lowerComputerElectricQuantity);
            }
            if (tvAuvElectricQuantity.getVisibility() == View.VISIBLE){
                tvAuvElectricQuantity.setText("AUV动力单元电量："+info.auvElectricQuantity);
            }
        } else {
            tv.setText("AUV："+info.imei+"("+ info.number +")");
        }
    }
}
