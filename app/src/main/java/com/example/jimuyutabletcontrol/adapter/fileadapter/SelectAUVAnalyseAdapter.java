package com.example.jimuyutabletcontrol.adapter.fileadapter;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.example.jimuyutabletcontrol.R;

import java.util.List;

/**
 * @author: HDX
 * @date: 2020\5\4 0004
 */
public class SelectAUVAnalyseAdapter extends BaseAdapter {
    private Activity context;
    private List<String> infos;
    private LayoutInflater inflater;

    public SelectAUVAnalyseAdapter(Activity context, List<String> infos) {
        this.context = context;
        this.infos = infos;
        inflater = LayoutInflater.from(context);
    }

    @Override
    public int getCount() {
        return (this.infos == null || this.infos.isEmpty()) ? 0 : this.infos.size();
    }

    @Override
    public Object getItem(int position) {
        return this.infos.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @SuppressLint("ViewHolder")
    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder viewHolder;
        if (null == convertView) {
            convertView = inflater.inflate(R.layout.select_auv_analyse_dialog, parent, false);
            viewHolder = new ViewHolder();
            viewHolder.text_view_simpleadapter = convertView.findViewById(R.id.text_view_simpleadapter);
            convertView.setTag(viewHolder);
        } else {
            viewHolder = (ViewHolder) convertView.getTag();
        }
        String info = infos.get(position);
        viewHolder.text_view_simpleadapter.setText(info);
        return convertView;
    }

    private class ViewHolder {
        TextView text_view_simpleadapter;
    }
}
