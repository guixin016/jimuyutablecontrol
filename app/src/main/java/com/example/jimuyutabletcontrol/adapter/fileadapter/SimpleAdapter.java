package com.example.jimuyutabletcontrol.adapter.fileadapter;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.jimuyutabletcontrol.R;
import com.example.jimuyutabletcontrol.bean.AUVRecordDataInfo;

import java.util.List;

public class SimpleAdapter extends BaseAdapter {
    private Activity context;
    private List<AUVRecordDataInfo> infos;
    private LayoutInflater inflater;

    public SimpleAdapter(Activity context, List<AUVRecordDataInfo> infos) {
        this.context = context;
        this.infos = infos;
        inflater = LayoutInflater.from(context);
    }

    @Override
    public int getCount() {
        return (this.infos == null || this.infos.isEmpty()) ? 0 : this.infos.size();
    }

    @Override
    public Object getItem(int position) {
        return this.infos.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @SuppressLint("ViewHolder")
    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder viewHolder;
        if (null == convertView) {
            convertView = inflater.inflate(R.layout.selectauvrecorddatainterface_dialog, parent, false);
            viewHolder = new ViewHolder();
            viewHolder.imageView = convertView.findViewById(R.id.image_view_simpleadapter);
            viewHolder.fileViewfilename = convertView.findViewById(R.id.text_view_simpleadapter);
            viewHolder.fileViewcondition = convertView.findViewById(R.id.whether_done);
            convertView.setTag(viewHolder);
        } else {
            viewHolder = (ViewHolder) convertView.getTag();
        }
        AUVRecordDataInfo info = infos.get(position);
        viewHolder.fileViewfilename.setText(info.textName);
        viewHolder.fileViewcondition.setText(info.imei + "，" + (info.isDownload ? "已下载" : "未下载"));
        return convertView;
    }

    private class ViewHolder {
        ImageView imageView;
        TextView fileViewfilename;
        TextView fileViewcondition;
    }
}
