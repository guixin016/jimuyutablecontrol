package com.example.jimuyutabletcontrol.adapter.filedetail;

import android.view.LayoutInflater;
import android.view.ViewGroup;

import com.example.jimuyutabletcontrol.R;
import com.example.jimuyutabletcontrol.base.BaseViewHolder;
import com.example.jimuyutabletcontrol.bean.MakeTaskMapPoint;

import java.util.List;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

/**
 * @author: HDX
 * @date: 2020\5\29 0029
 */
public class TaskDetailAdapter extends RecyclerView.Adapter<BaseViewHolder<MakeTaskMapPoint.PackageAuxiliaryPoint>> {

    private List<MakeTaskMapPoint.PackageAuxiliaryPoint> infos;

    public TaskDetailAdapter(List<MakeTaskMapPoint.PackageAuxiliaryPoint> infos){
        this.infos = infos;
    }

    @NonNull
    @Override
    public BaseViewHolder<MakeTaskMapPoint.PackageAuxiliaryPoint> onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new TaskDetailViewHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.task_details_item,parent,false));
    }

    @Override
    public void onBindViewHolder(@NonNull BaseViewHolder<MakeTaskMapPoint.PackageAuxiliaryPoint> holder, int position) {
        holder.setLayoutInfo(infos.get(position));
    }

    @Override
    public int getItemCount() {
        return infos.size();
    }
}
