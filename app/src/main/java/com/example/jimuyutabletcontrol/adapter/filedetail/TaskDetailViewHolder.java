package com.example.jimuyutabletcontrol.adapter.filedetail;

import android.view.View;
import android.widget.TextView;

import com.example.jimuyutabletcontrol.R;
import com.example.jimuyutabletcontrol.base.BaseViewHolder;
import com.example.jimuyutabletcontrol.bean.MakeTaskMapPoint;

import java.text.DecimalFormat;

/**
 * @author: HDX
 * @date: 2020\5\29 0029
 */
public class TaskDetailViewHolder extends BaseViewHolder<MakeTaskMapPoint.PackageAuxiliaryPoint> {

    private TextView tvPoint;

    private TextView tvLongitude;

    private TextView tvLatitude;

    private TextView tvDepth;

    private TextView tvRotateSpeed;

    private TextView tvDepthLimit;

    private TextView tvGpsCalibration;

    private DecimalFormat decimalFormat = new DecimalFormat("0.000000");

    public TaskDetailViewHolder(View itemView) {
        super(itemView);
        initView();
    }

    private void initView() {
        tvPoint = itemView.findViewById(R.id.tvPoint);

        tvLongitude = itemView.findViewById(R.id.tvLongitude);
        tvLatitude = itemView.findViewById(R.id.tvLatitude);
        tvDepth = itemView.findViewById(R.id.tvDepth);
        tvRotateSpeed = itemView.findViewById(R.id.tvRotateSpeed);

        tvDepthLimit = itemView.findViewById(R.id.tvDepthLimit);
        tvGpsCalibration = itemView.findViewById(R.id.tvGpsCalibration);
    }

    @Override
    public void setLayoutInfo(MakeTaskMapPoint.PackageAuxiliaryPoint info) {
        tvPoint.setText(info.text + "坐标点");
        tvLongitude.setText("经度：" + decimalFormat.format(info.auxiliaryPoint.longitude));
        tvLatitude.setText("纬度：" + decimalFormat.format(info.auxiliaryPoint.latitude));
        tvDepth.setText("深度：" + info.auxiliaryPoint.powerDepth + "米");
        tvRotateSpeed.setText("转速：" + info.auxiliaryPoint.powerSpeed);

        tvDepthLimit.setText("深度限制：" + info.auxiliaryPoint.depthLimit + "米");
        tvGpsCalibration.setText("GPS校准：" + (info.auxiliaryPoint.isGpsAdjusting ? "是" : "否"));
    }
}
