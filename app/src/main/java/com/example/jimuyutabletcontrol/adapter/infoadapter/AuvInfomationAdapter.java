package com.example.jimuyutabletcontrol.adapter.infoadapter;

import android.view.LayoutInflater;
import android.view.ViewGroup;

import com.example.jimuyutabletcontrol.R;
import com.example.jimuyutabletcontrol.base.BaseViewHolder;
import com.example.jimuyutabletcontrol.bean.auv_infomation.AuvContent;
import com.example.jimuyutabletcontrol.bean.auv_infomation.AuvInfoAc;

import java.util.List;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

/**
 * @author: HDX
 * @date: 2020\6\15 0015
 */
public class AuvInfomationAdapter extends RecyclerView.Adapter<BaseViewHolder<AuvInfoAc>> {

    public List<AuvInfoAc> auvInfoAcs;

    public AuvInfomationAdapter(List<AuvInfoAc> auvInfoAcs){
        this.auvInfoAcs = auvInfoAcs;
    }

    @Override
    public int getItemViewType(int position) {
        AuvInfoAc info = auvInfoAcs.get(position);
        if (info instanceof AuvContent){
            return 1;
        } else {
            return 2;
        }
    }

    @NonNull
    @Override
    public BaseViewHolder<AuvInfoAc> onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        if (viewType == 1){
            return new AuvInfomationViewHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.auv_infomation_item,parent,false));
        } else {
            return new AuvTipViewHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.auv_tip_item,parent,false));
        }
    }

    @Override
    public void onBindViewHolder(@NonNull BaseViewHolder<AuvInfoAc> holder, int position) {
        holder.setLayoutInfo(auvInfoAcs.get(position));
    }

    @Override
    public int getItemCount() {
        return auvInfoAcs.size();
    }
}
