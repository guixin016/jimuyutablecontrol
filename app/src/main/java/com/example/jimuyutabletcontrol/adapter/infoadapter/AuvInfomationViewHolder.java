package com.example.jimuyutabletcontrol.adapter.infoadapter;

import android.text.TextUtils;
import android.view.View;
import android.widget.TextView;

import com.example.jimuyutabletcontrol.R;
import com.example.jimuyutabletcontrol.base.BaseViewHolder;
import com.example.jimuyutabletcontrol.bean.auv_infomation.AuvContent;
import com.example.jimuyutabletcontrol.bean.auv_infomation.AuvInfoAc;

/**
 * @author: HDX
 * @date: 2020\6\15 0015
 */
public class AuvInfomationViewHolder extends BaseViewHolder<AuvInfoAc> {

    private TextView tvMessage;

    private AuvContent data;

    public AuvInfomationViewHolder(View itemView) {
        super(itemView);
        initView();
    }

    private void initView() {
        tvMessage = itemView.findViewById(R.id.message);
    }

    @Override
    public void setLayoutInfo(AuvInfoAc info) {
        this.data = (AuvContent) info;
        tvMessage.setText(data.name+"       "+(TextUtils.isEmpty(data.value)?"":data.value));
    }
}
