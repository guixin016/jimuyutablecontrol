package com.example.jimuyutabletcontrol.adapter.infoadapter;

import android.util.Log;
import android.view.View;
import android.widget.TextView;

import com.example.jimuyutabletcontrol.R;
import com.example.jimuyutabletcontrol.base.BaseViewHolder;
import com.example.jimuyutabletcontrol.bean.auv_infomation.AuvInfoAc;
import com.example.jimuyutabletcontrol.bean.auv_infomation.AuvTip;

/**
 * @author: HDX
 * @date: 2020\6\15 0015
 */
public class AuvTipViewHolder extends BaseViewHolder<AuvInfoAc> {

    private TextView tv;

    private AuvTip data;

    public AuvTipViewHolder(View itemView) {
        super(itemView);
        initView();
    }

    private void initView() {
        tv = itemView.findViewById(R.id.tv);
    }

    @Override
    public void setLayoutInfo(AuvInfoAc info) {
        this.data = (AuvTip) info;
        tv.setText(data.tip);
    }
}
