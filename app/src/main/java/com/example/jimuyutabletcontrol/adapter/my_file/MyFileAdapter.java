package com.example.jimuyutabletcontrol.adapter.my_file;

import android.view.LayoutInflater;
import android.view.ViewGroup;

import com.example.jimuyutabletcontrol.R;
import com.example.jimuyutabletcontrol.base.BaseViewHolder;

import java.io.File;
import java.util.List;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

/**
 * @author: HDX
 * @date: 2020\5\14 0014
 */
public class MyFileAdapter extends RecyclerView.Adapter<BaseViewHolder<File>> {

    private List<File> files;

    private OnMyFileListener listener;

    public MyFileAdapter(List<File> files) {
        this.files = files;
    }

    @NonNull
    @Override
    public BaseViewHolder<File> onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new MyFileViewHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.file_item_layout, parent, false), listener);
    }

    @Override
    public void onBindViewHolder(@NonNull BaseViewHolder<File> holder, int position) {
        holder.setLayoutInfo(files.get(position));
    }

    @Override
    public int getItemCount() {
        return files.size();
    }

    public void setOnMyFileListener(OnMyFileListener listener) {
        this.listener = listener;
    }

    public interface OnMyFileListener {
        void onMyFileListener(int position);
        void onLongClickMyFileListener(int position);
    }
}
