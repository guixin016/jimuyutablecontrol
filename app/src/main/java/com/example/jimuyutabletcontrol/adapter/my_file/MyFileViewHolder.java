package com.example.jimuyutabletcontrol.adapter.my_file;

import android.annotation.SuppressLint;
import android.util.Log;
import android.view.View;
import android.widget.TextView;

import com.example.jimuyutabletcontrol.R;
import com.example.jimuyutabletcontrol.base.BaseViewHolder;

import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;

/**
 * @author: HDX
 * @date: 2020\5\14 0014
 */
public class MyFileViewHolder extends BaseViewHolder<File> {

    private TextView mTvFileName, mTvFileTime;

    private MyFileAdapter.OnMyFileListener listener;

    public MyFileViewHolder(View itemView) {
        super(itemView);
        initView();
        setListeners();
    }

    public MyFileViewHolder(View itemView, MyFileAdapter.OnMyFileListener listener) {
        this(itemView);
        this.listener = listener;
    }

    private void setListeners() {
        itemView.setOnClickListener(v -> {
            if (null != listener) {
                listener.onMyFileListener(getAdapterPosition());
            }
        });
        itemView.setOnLongClickListener(v -> {
            if (null != listener) {
                listener.onLongClickMyFileListener(getAdapterPosition());
                return true;
            } else return false;
        });
    }

    private void initView() {
        mTvFileName = itemView.findViewById(R.id.fileName);
        mTvFileTime = itemView.findViewById(R.id.fileTime);
    }

    @Override
    public void setLayoutInfo(File info) {
        mTvFileName.setText(info.getName());
        @SuppressLint("SimpleDateFormat") SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        mTvFileTime.setText(formatter.format(info.lastModified()));

        Log.e("MyFileViewHolder", info.getAbsolutePath());
        try {
            Log.e("MyFileViewHolder", info.getCanonicalPath());
        } catch (IOException e) {
            e.printStackTrace();
        }
        Log.e("MyFileViewHolder", info.getName());
        Log.e("MyFileViewHolder", info.getPath());
        Log.e("MyFileViewHolder", info.getFreeSpace() + "");
        Log.e("MyFileViewHolder", info.getUsableSpace() + "");
        Log.e("MyFileViewHolder", info.getAbsolutePath());
        Log.e("MyFileViewHolder", info.getAbsolutePath());
    }
}
