package com.example.jimuyutabletcontrol.adapter.navigation_dialog;

import android.view.LayoutInflater;
import android.view.ViewGroup;

import com.example.jimuyutabletcontrol.R;
import com.example.jimuyutabletcontrol.adapter.navigation_dialog.viewholder.NavigationAuvDialogViewholder;
import com.example.jimuyutabletcontrol.base.BaseViewHolder;
import com.example.jimuyutabletcontrol.bean.NavigationDialogAuvInfo;

import java.util.List;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

/**
 * @author: HDX
 * @date: 2020\5\21 0021
 */
public class NavigationAuvDialogAdapter extends RecyclerView.Adapter<BaseViewHolder<NavigationDialogAuvInfo>> {

    private List<NavigationDialogAuvInfo> strings;

    private NavigationAuvDialogListener listener;

    public NavigationAuvDialogAdapter(List<NavigationDialogAuvInfo> strings) {
        this.strings = strings;
    }

    @NonNull
    @Override
    public BaseViewHolder<NavigationDialogAuvInfo> onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new NavigationAuvDialogViewholder(LayoutInflater.from(parent.getContext())
                .inflate(R.layout.navigation_auv_item,parent,false),listener);
    }

    @Override
    public void onBindViewHolder(@NonNull BaseViewHolder<NavigationDialogAuvInfo> holder, int position) {
        holder.setLayoutInfo(strings.get(position));
    }

    @Override
    public int getItemCount() {
        return strings.size();
    }

    public void setOnNavigationAuvDialogListener(NavigationAuvDialogListener listener){
        this.listener = listener;
    }

    public interface NavigationAuvDialogListener{
        void onNavigationAuvDialogListener(int position);
    }
}
