package com.example.jimuyutabletcontrol.adapter.navigation_dialog;

import android.view.LayoutInflater;
import android.view.ViewGroup;

import com.example.jimuyutabletcontrol.R;
import com.example.jimuyutabletcontrol.adapter.navigation_dialog.viewholder.NavigationTaskDialogViewholder;
import com.example.jimuyutabletcontrol.base.BaseViewHolder;
import com.example.jimuyutabletcontrol.bean.NavigationDialogTaskInfo;

import java.util.List;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

/**
 * @author: HDX
 * @date: 2020\5\21 0021
 */
public class NavigationTaskDialogAdapter extends RecyclerView.Adapter<BaseViewHolder<NavigationDialogTaskInfo>> {

    private List<NavigationDialogTaskInfo> infos;

    private NavigationTaskDialogListener listener;

    public NavigationTaskDialogAdapter(List<NavigationDialogTaskInfo> infos) {
        this.infos = infos;
    }

    @NonNull
    @Override
    public BaseViewHolder<NavigationDialogTaskInfo> onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new NavigationTaskDialogViewholder(LayoutInflater.from(parent.getContext())
                .inflate(R.layout.navigation_task_item, parent, false), listener);
    }

    @Override
    public void onBindViewHolder(@NonNull BaseViewHolder<NavigationDialogTaskInfo> holder, int position) {
        holder.setLayoutInfo(infos.get(position));
    }

    @Override
    public int getItemCount() {
        return infos.size();
    }

    public void setOnNavigationTaskDialogListener(NavigationTaskDialogListener listener) {
        this.listener = listener;
    }

    public interface NavigationTaskDialogListener {
        void onNavigationTaskDialogListener(int position);
    }
}
