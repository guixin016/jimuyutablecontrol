package com.example.jimuyutabletcontrol.adapter.navigation_dialog.viewholder;

import android.graphics.Color;
import android.view.View;
import android.widget.TextView;

import com.example.jimuyutabletcontrol.R;
import com.example.jimuyutabletcontrol.adapter.navigation_dialog.NavigationAuvDialogAdapter;
import com.example.jimuyutabletcontrol.base.BaseViewHolder;
import com.example.jimuyutabletcontrol.bean.NavigationDialogAuvInfo;

/**
 * @author: HDX
 * @date: 2020\5\21 0021
 */
public class NavigationAuvDialogViewholder extends BaseViewHolder<NavigationDialogAuvInfo> {

    private TextView auvName;

    private NavigationAuvDialogAdapter.NavigationAuvDialogListener listener;

    public NavigationAuvDialogViewholder(View itemView) {
        super(itemView);
        initView();
        setListeners();
    }

    public NavigationAuvDialogViewholder(View itemView, NavigationAuvDialogAdapter.NavigationAuvDialogListener listener) {
        this(itemView);
        this.listener = listener;
    }

    private void setListeners() {
        itemView.setOnClickListener(v -> {
            if (null != listener){
                listener.onNavigationAuvDialogListener(getAdapterPosition());
            }
        });
    }

    private void initView() {
        auvName = itemView.findViewById(R.id.auvName);
    }

    @Override
    public void setLayoutInfo(NavigationDialogAuvInfo info) {
        auvName.setText("AUV:"+info.imei);
        itemView.setBackgroundColor(info.isSelected?Color.YELLOW:Color.WHITE);
    }
}
