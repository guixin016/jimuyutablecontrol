package com.example.jimuyutabletcontrol.adapter.navigation_dialog.viewholder;

import android.view.View;
import android.widget.TextView;

import com.example.jimuyutabletcontrol.R;
import com.example.jimuyutabletcontrol.adapter.navigation_dialog.NavigationTaskDialogAdapter;
import com.example.jimuyutabletcontrol.base.BaseViewHolder;
import com.example.jimuyutabletcontrol.bean.NavigationDialogTaskInfo;

/**
 * @author: HDX
 * @date: 2020\5\21 0021
 */
public class NavigationTaskDialogViewholder extends BaseViewHolder<NavigationDialogTaskInfo> {

    private TextView fileName;

    private TextView fileState;

    private NavigationTaskDialogAdapter.NavigationTaskDialogListener listener;

    public NavigationTaskDialogViewholder(View itemView) {
        super(itemView);
        initView();
        setListener();
    }

    public NavigationTaskDialogViewholder(View itemView, NavigationTaskDialogAdapter.NavigationTaskDialogListener listener) {
        this(itemView);
        this.listener = listener;
    }

    private void setListener() {
        itemView.setOnClickListener(v -> {
            if (null != listener){
                listener.onNavigationTaskDialogListener(getAdapterPosition());
            }
        });
    }

    private void initView() {
        fileName = itemView.findViewById(R.id.fileName);
        fileState = itemView.findViewById(R.id.fileState);
    }

    @Override
    public void setLayoutInfo(NavigationDialogTaskInfo info) {
        fileName.setText(info.taskName);
        fileState.setVisibility(info.isExecute?View.VISIBLE:View.GONE);
    }
}
