package com.example.jimuyutabletcontrol.adapter.pulltorefreshadapter;

import android.content.Context;
import android.os.Handler;
import android.widget.ArrayAdapter;

import java.util.ArrayList;
import java.util.List;


public class BaseListAdapter<T> extends ArrayAdapter<T> {

    protected Context mContext;
    protected Handler mHandler;
    protected List<T> mDataList;
    protected boolean hasNoData;
    protected int showAnnotation;
    protected List<String> allAnnotation;

    public BaseListAdapter(Context context, List<T> objects, int annotation) {
        super(context, 0, objects);
        this.mContext = context;
        this.mDataList = objects;
        this.showAnnotation = annotation;
        this.allAnnotation = new ArrayList<>();
        this.allAnnotation.add("点击获取该在线AUV信息");
        this.allAnnotation.add("点击制作该在线AUV导航任务");
        this.allAnnotation.add("点击查看或者修改该在线AUV导航任务");
        this.allAnnotation.add("点击发送该在线AUV导航任务");
        this.allAnnotation.add("点击获取该在线AUV内记数据");
        this.allAnnotation.add("点击分析该在线AUV内记数据");
        if (this.mDataList == null || this.mDataList.isEmpty())
            hasNoData = true;
    }

    public BaseListAdapter(Context context, List<T> objects, Handler handler) {
        super(context, 0, objects);
        this.mContext = context;
        this.mDataList = objects;
        this.mHandler = handler;
        if (this.mDataList == null || this.mDataList.isEmpty())
            hasNoData = true;
    }

    public void updateListView(List<T> dataList) {
        this.mDataList = dataList;
        hasNoData = (this.mDataList == null || this.mDataList.isEmpty());
        notifyDataSetChanged();
    }

    @Override
    public int getCount() {
        if (hasNoData)
            return 1;
        return (mDataList == null || mDataList.isEmpty()) ? 0 : mDataList.size();
    }

    @Override
    public T getItem(int position) {
        if (hasNoData)
            return null;
        return mDataList.get(position);
    }

    @Override
    public int getItemViewType(int position) {
        if (hasNoData)
            return 0;
        return 1;
    }

    @Override
    public int getViewTypeCount() {
        return 2;
    }
}
