package com.example.jimuyutabletcontrol.adapter.pulltorefreshadapter;

import android.app.Activity;
import android.content.Context;
import android.content.res.Resources;
import android.util.DisplayMetrics;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AbsListView;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;


import com.example.jimuyutabletcontrol.R;

import java.util.List;


public class ListAdapter extends BaseListAdapter<String> {

    public ListAdapter(Context context, List<String> objects, int annotation) {
        super(context, objects, annotation);
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder holder;
        if (convertView == null) {
            holder = new ViewHolder();
            if (getItemViewType(position) == 0) {
                convertView = ((Activity) (mContext)).getLayoutInflater().inflate(R.layout.onlineinterface_has_no_data, parent, false);
                holder.noDataRootLayout = (LinearLayout) convertView.findViewById(R.id.root_layout);
            } else {
                convertView = ((Activity) (mContext)).getLayoutInflater().inflate(R.layout.onlineinterface_list_item, parent, false);
                holder.imageView = (ImageView) convertView.findViewById(R.id.image_view);
                holder.titleView = (TextView) convertView.findViewById(R.id.ItemTitelText);
                holder.showView = (TextView) convertView.findViewById(R.id.ItemShowText);
            }
            convertView.setTag(holder);
        } else {
            holder = (ViewHolder) convertView.getTag();
        }

        if (hasNoData) {
            AbsListView.LayoutParams lp = new AbsListView.LayoutParams(getScreenWidth(), getScreenHeight() * 2 / 3);
            holder.noDataRootLayout.setLayoutParams(lp);
        } else {
            holder.titleView.setText(mDataList.get(position));
            holder.showView.setText(this.allAnnotation.get(this.showAnnotation));
        }

        return convertView;
    }

    private static final class ViewHolder {
        ImageView imageView;
        TextView titleView;
        TextView showView;

        LinearLayout noDataRootLayout;
    }

    private int getScreenWidth() {
        DisplayMetrics displayMetric = Resources.getSystem().getDisplayMetrics();
        return displayMetric.widthPixels;
    }

    private int getScreenHeight() {
        DisplayMetrics displayMetric = Resources.getSystem().getDisplayMetrics();
        return displayMetric.heightPixels;
    }
}
