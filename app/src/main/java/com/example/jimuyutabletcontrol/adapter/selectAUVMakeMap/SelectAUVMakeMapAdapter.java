package com.example.jimuyutabletcontrol.adapter.selectAUVMakeMap;

import android.view.LayoutInflater;
import android.view.ViewGroup;

import com.example.jimuyutabletcontrol.R;
import com.example.jimuyutabletcontrol.base.BaseViewHolder;

import java.util.List;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

/**
 * @author: HDX
 * @date: 2020\5\8 0008
 */
public class SelectAUVMakeMapAdapter extends RecyclerView.Adapter<BaseViewHolder<String>> {

    private List<String> strings;

    private SelectAUVMakeMapListener listener;

    public SelectAUVMakeMapAdapter(List<String> strings){
        this.strings = strings;
    }

    @NonNull
    @Override
    public BaseViewHolder<String> onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        return new SelectAUVMakeMapViewHolder(LayoutInflater.from(viewGroup.getContext()).
                inflate(R.layout.onlineinterface_list_item,viewGroup,false),listener);
    }

    @Override
    public void onBindViewHolder(@NonNull BaseViewHolder<String> stringBaseViewHolder, int i) {
        stringBaseViewHolder.setLayoutInfo(strings.get(i));
    }

    @Override
    public int getItemCount() {
        return strings.size();
    }

    public void setOnSelectAUVMakeMapListener(SelectAUVMakeMapListener listener){
        this.listener = listener;
    }

    public interface SelectAUVMakeMapListener{
        void onSelectAUVMakeMapListener(int position);
    }
}
