package com.example.jimuyutabletcontrol.adapter.selectAUVMakeMap;

import android.view.View;
import android.widget.TextView;

import com.example.jimuyutabletcontrol.R;
import com.example.jimuyutabletcontrol.base.BaseViewHolder;

/**
 * @author: HDX
 * @date: 2020\5\8 0008
 */
public class SelectAUVMakeMapViewHolder extends BaseViewHolder<String> {

    private TextView itemTitelText;

    private SelectAUVMakeMapAdapter.SelectAUVMakeMapListener listener;

    public SelectAUVMakeMapViewHolder(View itemView) {
        super(itemView);
        initView();
        setListeners();
    }

    public SelectAUVMakeMapViewHolder(View itemView, SelectAUVMakeMapAdapter.SelectAUVMakeMapListener listener) {
        this(itemView);
        this.listener = listener;
    }

    private void setListeners() {
        itemView.setOnClickListener(v -> {
            if (null != listener){
                listener.onSelectAUVMakeMapListener(getAdapterPosition());
            }
        });
    }

    private void initView() {
        itemTitelText = itemView.findViewById(R.id.ItemTitelText);
    }

    @Override
    public void setLayoutInfo(String info) {
        itemTitelText.setText(null == info?"":info);
    }
}
