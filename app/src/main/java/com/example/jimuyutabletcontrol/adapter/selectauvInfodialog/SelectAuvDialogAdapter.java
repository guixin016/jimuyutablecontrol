package com.example.jimuyutabletcontrol.adapter.selectauvInfodialog;

import android.view.LayoutInflater;
import android.view.ViewGroup;

import com.example.jimuyutabletcontrol.R;
import com.example.jimuyutabletcontrol.base.BaseViewHolder;
import com.example.jimuyutabletcontrol.bean.AuvDialogInfo;

import java.util.List;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

/**
 * @author: HDX
 * @date: 2020\4\29 0029
 */
public class SelectAuvDialogAdapter extends RecyclerView.Adapter<BaseViewHolder<AuvDialogInfo>> {

    private List<AuvDialogInfo> infos;

    private SelectAuvDialogListener listener;

    public SelectAuvDialogAdapter(List<AuvDialogInfo> infos) {
        this.infos = infos;
    }

    @NonNull
    @Override
    public BaseViewHolder<AuvDialogInfo> onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        return new SelectAuvDialogViewHolder(LayoutInflater.from(viewGroup.getContext()).
                inflate(R.layout.select_auv_dialog_layout_item, viewGroup, false), listener);
    }

    @Override
    public void onBindViewHolder(@NonNull BaseViewHolder<AuvDialogInfo> auvDialogInfoBaseViewHolder, int i) {
        auvDialogInfoBaseViewHolder.setLayoutInfo(infos.get(i));
    }

    @Override
    public void onBindViewHolder(@NonNull BaseViewHolder<AuvDialogInfo> holder, int position, @NonNull List<Object> payloads) {
        super.onBindViewHolder(holder, position, payloads);
        if (payloads.size() == 0) {
            onBindViewHolder(holder, position);
        } else {
            Object tab = payloads.get(0);
            if (tab instanceof Integer) {
                holder.setLayoutInfo(infos.get(position), (int) tab);
            }
        }
    }

    @Override
    public int getItemCount() {
        return infos.size();
    }

    public void setOnSelectAuvDialogListener(SelectAuvDialogListener listener) {
        this.listener = listener;
    }

    public interface SelectAuvDialogListener {
        void onSelectAuvDialogListener(int position);
    }
}
