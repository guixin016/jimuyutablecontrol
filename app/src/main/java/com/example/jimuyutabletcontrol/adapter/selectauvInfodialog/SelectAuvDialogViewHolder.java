package com.example.jimuyutabletcontrol.adapter.selectauvInfodialog;

import android.view.View;
import android.widget.TextView;

import com.example.jimuyutabletcontrol.R;
import com.example.jimuyutabletcontrol.base.BaseViewHolder;
import com.example.jimuyutabletcontrol.bean.AuvDialogInfo;

/**
 * @author: HDX
 * @date: 2020\4\29 0029
 */
public class SelectAuvDialogViewHolder extends BaseViewHolder<AuvDialogInfo> {

    private TextView number;

    private TextView auvImei;

    private TextView state;

    private SelectAuvDialogAdapter.SelectAuvDialogListener listener;

    public SelectAuvDialogViewHolder(View itemView) {
        super(itemView);
        initView();
        setListener();
    }

    public SelectAuvDialogViewHolder(View itemView, SelectAuvDialogAdapter.SelectAuvDialogListener listener) {
        this(itemView);
        this.listener = listener;
    }

    private void setListener() {
        itemView.setOnClickListener(v -> {
            if (null != listener) {
                listener.onSelectAuvDialogListener(getAdapterPosition());
            }
        });
    }

    private void initView() {
        number = itemView.findViewById(R.id.number);
        auvImei = itemView.findViewById(R.id.auvImei);
        state = itemView.findViewById(R.id.state);
    }

    @Override
    public void setLayoutInfo(AuvDialogInfo info) {
        number.setText((getAdapterPosition() + 1) + "");
        auvImei.setText("IMEI：" + info.imei);
        state.setText(info.isSelected ? "已选中" : "");
    }

    @Override
    public void setLayoutInfo(AuvDialogInfo info, int tag) {
        super.setLayoutInfo(info, tag);
        state.setText(info.isSelected ? "已选中" : "");
    }
}
