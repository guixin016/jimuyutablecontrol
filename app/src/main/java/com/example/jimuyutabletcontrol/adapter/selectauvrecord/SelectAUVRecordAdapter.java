package com.example.jimuyutabletcontrol.adapter.selectauvrecord;

import android.view.LayoutInflater;
import android.view.ViewGroup;

import com.example.jimuyutabletcontrol.R;
import com.example.jimuyutabletcontrol.base.BaseViewHolder;

import java.util.List;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

/**
 * @author: HDX
 * @date: 2020\5\2 0002
 */
public class SelectAUVRecordAdapter extends RecyclerView.Adapter<BaseViewHolder<String>> {

    private List<String> strs;

    private SelectAUVRecordListener listener;

    public SelectAUVRecordAdapter(List<String> strs) {
        this.strs = strs;
    }

    @NonNull
    @Override
    public BaseViewHolder<String> onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        return new SelectAUVRecordViewHolder(LayoutInflater.from(viewGroup.getContext()).
                inflate(R.layout.onlineinterface_list_item, viewGroup, false),listener);
    }

    @Override
    public void onBindViewHolder(@NonNull BaseViewHolder<String> stringBaseViewHolder, int i) {
        stringBaseViewHolder.setLayoutInfo(strs.get(i));
    }

    @Override
    public int getItemCount() {
        return strs.size();
    }

    public void setOnSelectAUVRecordListener(SelectAUVRecordListener listener) {
        this.listener = listener;
    }

    public interface SelectAUVRecordListener {
        void onSelectAUVRecordListener(int position);
    }
}
