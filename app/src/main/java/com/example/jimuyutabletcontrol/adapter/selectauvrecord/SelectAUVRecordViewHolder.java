package com.example.jimuyutabletcontrol.adapter.selectauvrecord;

import android.view.View;
import android.widget.TextView;

import com.example.jimuyutabletcontrol.R;
import com.example.jimuyutabletcontrol.base.BaseViewHolder;

/**
 * @author: HDX
 * @date: 2020\5\2 0002
 */
public class SelectAUVRecordViewHolder extends BaseViewHolder<String> {

    private TextView ItemTitelText;

    private SelectAUVRecordAdapter.SelectAUVRecordListener listener;

    public SelectAUVRecordViewHolder(View itemView) {
        super(itemView);
        initView();
        setListeners();
    }

    public SelectAUVRecordViewHolder(View itemView, SelectAUVRecordAdapter.SelectAUVRecordListener listener) {
        this(itemView);
        this.listener = listener;
    }

    private void setListeners() {
        itemView.setOnClickListener(v -> {
            if (null != listener) {
                listener.onSelectAUVRecordListener(getAdapterPosition());
            }
        });
    }

    private void initView() {
        ItemTitelText = itemView.findViewById(R.id.ItemTitelText);
    }

    @Override
    public void setLayoutInfo(String info) {
        ItemTitelText.setText(info);
    }
}
