package com.example.jimuyutabletcontrol.adapter.setauvtask;

import android.view.LayoutInflater;
import android.view.ViewGroup;

import com.example.jimuyutabletcontrol.R;
import com.example.jimuyutabletcontrol.base.BaseViewHolder;

import java.util.List;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

/**
 * @author: HDX
 * @date: 2020\5\2 0002
 */
public class SettingAuvListAdapter extends RecyclerView.Adapter<BaseViewHolder<String>> {

    private List<String> strs;

    public SettingAuvListAdapter(List<String> strs) {
        this.strs = strs;
    }

    @NonNull
    @Override
    public BaseViewHolder<String> onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        return new SettingAuvListViewHolder(LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.setting_auv_dialog_item, viewGroup, false));
    }

    @Override
    public void onBindViewHolder(@NonNull BaseViewHolder<String> stringBaseViewHolder, int i) {
        if (null == strs) return;
        stringBaseViewHolder.setLayoutInfo(strs.get(i));
    }

    @Override
    public int getItemCount() {
        if (null == strs) return 0;
        return strs.size();
    }
}
