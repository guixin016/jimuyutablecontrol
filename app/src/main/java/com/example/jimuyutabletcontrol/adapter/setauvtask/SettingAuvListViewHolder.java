package com.example.jimuyutabletcontrol.adapter.setauvtask;

import android.view.View;
import android.widget.TextView;

import com.example.jimuyutabletcontrol.R;
import com.example.jimuyutabletcontrol.base.BaseViewHolder;

/**
 * @author: HDX
 * @date: 2020\5\2 0002
 */
public class SettingAuvListViewHolder extends BaseViewHolder<String> {

    private TextView auvName;

    public SettingAuvListViewHolder(View itemView) {
        super(itemView);
        initView();
    }

    private void initView() {
        auvName = itemView.findViewById(R.id.auvName);
    }

    @Override
    public void setLayoutInfo(String info) {
        auvName.setText("AUV:" + info);
    }
}
