package com.example.jimuyutabletcontrol.adapter.taskdialogadapter;

import android.view.LayoutInflater;
import android.view.ViewGroup;

import com.example.jimuyutabletcontrol.R;
import com.example.jimuyutabletcontrol.base.BaseViewHolder;
import com.example.jimuyutabletcontrol.bean.TaskDialogInfo;

import java.util.List;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

/**
 * @author: HDX
 * @date: 2020\5\20 0020
 */
public class TaskDialogAdapter extends RecyclerView.Adapter<BaseViewHolder<TaskDialogInfo>> {

    private List<TaskDialogInfo> strings;

    private TaskDialogListener listener;

    public TaskDialogAdapter(List<TaskDialogInfo> strings){
        this.strings = strings;
    }

    @NonNull
    @Override
    public BaseViewHolder<TaskDialogInfo> onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new TaskDialogViewHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.task_item,parent,false),listener);
    }

    @Override
    public void onBindViewHolder(@NonNull BaseViewHolder<TaskDialogInfo> holder, int position) {
        holder.setLayoutInfo(strings.get(position));
    }

    @Override
    public int getItemCount() {
        return strings.size();
    }

    public void setOnTaskDialogListener(TaskDialogListener listener){
        this.listener = listener;
    }

    public interface TaskDialogListener{
        void onTaskDialogListener(int position);
        void onTaskDialogLongListener(int position);
        void onCheckTaskDialogLongListener(int position);
    }
}
