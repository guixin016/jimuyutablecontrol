package com.example.jimuyutabletcontrol.adapter.taskdialogadapter;

import android.content.Intent;
import android.graphics.Color;
import android.view.View;
import android.widget.TextView;

import com.example.jimuyutabletcontrol.R;
import com.example.jimuyutabletcontrol.activity.TaskDetailActivity;
import com.example.jimuyutabletcontrol.base.BaseViewHolder;
import com.example.jimuyutabletcontrol.bean.TaskDialogInfo;

/**
 * @author: HDX
 * @date: 2020\5\20 0020
 */
public class TaskDialogViewHolder extends BaseViewHolder<TaskDialogInfo> implements View.OnClickListener {

    private TextView fileName , yetDownload;

    private TaskDialogInfo info;

    private TaskDialogAdapter.TaskDialogListener listener;

    public TaskDialogViewHolder(View itemView) {
        super(itemView);
        initView();
        setListeners();
    }

    public TaskDialogViewHolder(View itemView, TaskDialogAdapter.TaskDialogListener listener) {
        this(itemView);
        this.listener = listener;
    }

    private void setListeners() {
        itemView.setOnClickListener(this);
        itemView.setOnLongClickListener(v -> {
            if (null != listener) {
                listener.onTaskDialogLongListener(getAdapterPosition());
                return true;
            } else
                return false;
        });
        yetDownload.setOnClickListener(v -> {
            if (null != info && info.isPad&&null != listener){
                listener.onCheckTaskDialogLongListener(getAdapterPosition());
            }
        });
    }

    private void initView() {
        fileName = itemView.findViewById(R.id.fileName);
        yetDownload = itemView.findViewById(R.id.yetDownload);
    }

    @Override
    public void setLayoutInfo(TaskDialogInfo info) {
        this.info = info;
        if (info.isPad){
            yetDownload.setText("查看");
        } else {
            yetDownload.setText(info.isDownLoad?"已下载":"未下载");
        }
        fileName.setText(info.fileName);
        itemView.setBackgroundColor(info.isSelected ? Color.YELLOW : Color.WHITE);
    }

    @Override
    public void onClick(View v) {
        if (null != listener) {
            listener.onTaskDialogListener(getAdapterPosition());
        }
    }
}
