package com.example.jimuyutabletcontrol.app;

import android.app.Activity;

import java.util.Collections;
import java.util.LinkedList;
import java.util.List;

/**
 * @author: HDX
 * @date: 2020\3\31 0031
 */
public abstract class ActivityManage {
    public List<Activity> mActivitys = Collections
            .synchronizedList(new LinkedList<Activity>());

    public abstract void pushActivity(Activity activity);

    public abstract void popActivity(Activity activity);

    public abstract Activity currentActivity();

    public abstract void finishCurrentActivity();

    public abstract void finishActivity(Activity activity);

    public abstract void finishActivity(Class<?> cls);

    public abstract Activity findActivity(Class<?> cls);

    public abstract Activity getTopActivity();

    public abstract String getTopActivityName();

    public abstract void finishAllActivity();

    public abstract void appExit();
}