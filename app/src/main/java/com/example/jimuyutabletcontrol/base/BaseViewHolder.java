package com.example.jimuyutabletcontrol.base;

import android.content.Context;
import android.view.View;

import androidx.recyclerview.widget.RecyclerView;

/**
 * @author: HDX
 * @date: 2020\4\29 0029
 */
public abstract class BaseViewHolder<T> extends RecyclerView.ViewHolder {
    protected Context mContext;

    public BaseViewHolder(View itemView) {
        super(itemView);
        mContext = itemView.getContext();
    }

    public abstract void setLayoutInfo(T info);

    public void setLayoutInfo(T info, int tag){

    }
}
