package com.example.jimuyutabletcontrol.bean;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * @author: HDX
 * @date: 2020\4\29 0029
 */
public class AuvDialogInfo implements Parcelable {
    public String imei;
    public boolean isSelected = false;

    public AuvDialogInfo(){}

    protected AuvDialogInfo(Parcel in) {
        imei = in.readString();
        isSelected = in.readByte() != 0;
    }

    public static final Creator<AuvDialogInfo> CREATOR = new Creator<AuvDialogInfo>() {
        @Override
        public AuvDialogInfo createFromParcel(Parcel in) {
            return new AuvDialogInfo(in);
        }

        @Override
        public AuvDialogInfo[] newArray(int size) {
            return new AuvDialogInfo[size];
        }
    };

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(imei);
        dest.writeByte((byte) (isSelected ? 1 : 0));
    }
}
