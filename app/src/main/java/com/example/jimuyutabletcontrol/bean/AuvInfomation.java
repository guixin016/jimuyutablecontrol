package com.example.jimuyutabletcontrol.bean;

/**
 * @author: HDX
 * @date: 2020\6\15 0015
 */
public class AuvInfomation {
    public String longitude;
    public String latitude;
    public String accuracy;
    public String speed;
    public String time;
    public String  courseAngle;
    public String angleOfPitch;
    public String  rollAngle;
    public String phoneBattery;
    public String auvBattery;
    public String depth;
    public String selfPropeller;
    public String selfDepth;
    public String selfBattery;
}
