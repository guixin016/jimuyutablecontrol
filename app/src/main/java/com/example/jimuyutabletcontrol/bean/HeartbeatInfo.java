package com.example.jimuyutabletcontrol.bean;

/**
 * @author: HDX
 * @date: 2020\5\13 0013
 */
public class HeartbeatInfo {
    public String imei;
    public int number;
    public String lowerComputerElectricQuantity;
    public String auvElectricQuantity;

    public boolean isShwoElectricQuantity = false;
}
