package com.example.jimuyutabletcontrol.bean;

import android.graphics.Color;
import android.util.Log;

import com.baidu.mapapi.map.Overlay;
import com.baidu.mapapi.map.OverlayOptions;
import com.baidu.mapapi.map.Polyline;
import com.baidu.mapapi.map.PolylineOptions;
import com.baidu.mapapi.model.LatLng;
import com.baidu.mapapi.utils.DistanceUtil;
import com.example.jimuyutabletcontrol.utils.Constants;
import com.example.jimuyutabletcontrol.utils.DecimalFormatUtil;
import com.example.jimuyutabletcontrol.utils.MapUtil;

import java.util.ArrayList;
import java.util.List;

/**
 * @author: HDX
 * @date: 2020\5\8 0008
 */
public class MakeTaskMapLine {
    public MakeTaskMapPoint firstPoint;
    public MakeTaskMapPoint secondPoint;
    public OverlayOptions overlayOptions;
    public Overlay mPolyline;

    public void recycle() {
        this.firstPoint = null;
        this.secondPoint = null;
        this.overlayOptions = null;
        if (null != mPolyline && !mPolyline.isRemoved()) {
            mPolyline.remove();
        }
        mPolyline = null;
    }

    public OverlayOptions getLineOverlayOptions(int priority) {
        return new PolylineOptions()
                .width(3)
                .color(Color.BLUE)
                .points(getLinePointList())
                .zIndex(1)
                .dottedLine(true);
    }

    private List<LatLng> getLinePointList() {
        if (null == firstPoint || null == secondPoint) return null;
        List<LatLng> latLngs = new ArrayList<>();
        latLngs.add(firstPoint.selfPosition);
        latLngs.add(secondPoint.selfPosition);
        return latLngs;
    }

    public boolean isResetLine() {
        if (null == firstPoint || null == firstPoint.selfPosition
                || null == secondPoint || null == secondPoint.selfPosition
                || null == mPolyline) return false;
        LatLng fLatLng = firstPoint.selfPosition;
        LatLng sLatLng = secondPoint.selfPosition;
        if (mPolyline instanceof Polyline) {
            Polyline polyline = (Polyline) mPolyline;
            List<LatLng> latLngs = polyline.getPoints();
            if (latLngs.size() > 1) {
                if (fLatLng.longitude != latLngs.get(0).longitude
                        || fLatLng.latitude != latLngs.get(0).latitude
                        || sLatLng.longitude != latLngs.get(1).longitude
                        || sLatLng.latitude != latLngs.get(1).latitude) {
                    return true;
                } else {
                    return false;
                }
            } else {
                return false;
            }
        } else {
            return false;
        }
    }

    public void resetLine() {
        List<LatLng> latLngs = getLinePointList();
        if (null == latLngs || latLngs.size() < 2) return;
        if (mPolyline instanceof Polyline) {
            Polyline polyline = (Polyline) mPolyline;
            polyline.setPoints(latLngs);
        }
        if (overlayOptions instanceof PolylineOptions) {
            PolylineOptions polylineOptions = (PolylineOptions) overlayOptions;
            polylineOptions.points(latLngs);
        }
    }

    public int getDistance() {
        if (null == firstPoint || null == firstPoint.selfPosition
                || null == secondPoint || null == secondPoint.selfPosition) return 0;
        return (int) DistanceUtil.getDistance(firstPoint.selfPosition, secondPoint.selfPosition);//单位：米
    }

    public double getDistance2() {
        if (null == firstPoint || null == firstPoint.selfPosition
                || null == secondPoint || null == secondPoint.selfPosition) return 0;
        return DistanceUtil.getDistance(firstPoint.selfPosition, secondPoint.selfPosition);//单位：米
    }

    public double getSpeed() {
        if (null == secondPoint||secondPoint.powerSpeed == 0) return 0;
        return MapUtil.getSpeedWithZhuanSpeed(secondPoint.powerSpeed);
    }

    public double getRotateSpeed() {
        if (null == secondPoint) return 0;
        return secondPoint.powerSpeed;
    }

    public double getNeedTimeSecond() {
        double speed = getSpeed();
        if (speed == 0)return 0;
        return getDistance2()/speed;
    }

    public double getNeedTimeMinute(){
        double speed = getSpeed();
        if (speed == 0)return 0;
        return getDistance2()/speed/60;
    }

    public int getDepth() {
        if (null == secondPoint) return 0;
        return secondPoint.powerDepth;
    }
}
