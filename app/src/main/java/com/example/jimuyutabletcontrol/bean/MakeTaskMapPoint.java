package com.example.jimuyutabletcontrol.bean;


import com.baidu.mapapi.map.BitmapDescriptor;
import com.baidu.mapapi.map.MarkerOptions;
import com.baidu.mapapi.map.Overlay;
import com.baidu.mapapi.map.OverlayOptions;
import com.baidu.mapapi.model.LatLng;
import com.baidu.mapapi.utils.CoordinateConverter;
import com.example.jimuyutabletcontrol.utils.CoordinateTransformUtil;

import java.io.Serializable;

/**
 * @author: HDX
 * @date: 2020\5\8 0008
 */
public class MakeTaskMapPoint implements Serializable {
    public OverlayOptions overlayOptions;
    public Overlay overlay;
    public BitmapDescriptor bitmapDescriptor;
    public int position;

    public String text;

    /**
     * 是否进行Gps校准
     */
    public boolean isGpsAdjusting = false;

    /**
     * 深度限制
     */
    public int depthLimit = 40;

    /**
     * 下沉深度
     */
    public int powerDepth;

    /**
     * 转速rpm
     */
    public int powerSpeed;

    /**
     * 经纬度
     */
    public LatLng selfPosition;


    public OverlayOptions getPointOverlayOptions(int priority) {
        if (null == selfPosition || null == bitmapDescriptor) return null;
        return new MarkerOptions()
                .position(selfPosition)
                .icon(bitmapDescriptor)
                .zIndex(priority);
    }


    public void recycle() {
        this.selfPosition = null;
        this.overlayOptions = null;
        if (null != overlay && !overlay.isRemoved()) {
            overlay.remove();
        }
        overlay = null;
        if (null != bitmapDescriptor) {
            bitmapDescriptor.recycle();
        }
        bitmapDescriptor = null;
        this.text = null;
    }
    public static MakeTaskMapPoint createMapPoint(PackageAuxiliaryPoint packageAuxiliaryPoint) {
        MakeTaskMapPoint makeTaskMapPoint = new MakeTaskMapPoint();
        makeTaskMapPoint.text = packageAuxiliaryPoint.text;
        AuxiliaryPoint auxiliaryPoint = packageAuxiliaryPoint.auxiliaryPoint;
        makeTaskMapPoint.powerSpeed = auxiliaryPoint.powerSpeed;
        makeTaskMapPoint.powerDepth = auxiliaryPoint.powerDepth;
        makeTaskMapPoint.depthLimit = auxiliaryPoint.depthLimit;
        makeTaskMapPoint.isGpsAdjusting = auxiliaryPoint.isGpsAdjusting;
        LatLng latLng = new LatLng(auxiliaryPoint.latitude, auxiliaryPoint.longitude);
        makeTaskMapPoint.selfPosition = CoordinateTransformUtil.gpsToBaidu(latLng);
        return makeTaskMapPoint;
    }

    public PackageAuxiliaryPoint getAuxiliaryPoint() {
        PackageAuxiliaryPoint packageAuxiliaryPoint = new PackageAuxiliaryPoint();
        packageAuxiliaryPoint.text = text;
        AuxiliaryPoint auxiliaryPoint = new AuxiliaryPoint();
        packageAuxiliaryPoint.auxiliaryPoint = auxiliaryPoint;
        auxiliaryPoint.depthLimit = depthLimit;
        auxiliaryPoint.isGpsAdjusting = isGpsAdjusting;
        if (null != selfPosition) {
            LatLng gpeLa = CoordinateTransformUtil.baiduToGps(selfPosition);
            auxiliaryPoint.latitude = gpeLa.latitude;
            auxiliaryPoint.longitude = gpeLa.longitude;
        }
        auxiliaryPoint.powerSpeed = powerSpeed;
        auxiliaryPoint.powerDepth = powerDepth;
        return packageAuxiliaryPoint;
    }

    public static class PackageAuxiliaryPoint implements Serializable{
        public AuxiliaryPoint auxiliaryPoint;
        public String text;
    }

    public static class AuxiliaryPoint implements Serializable{
        public double longitude;
        public double latitude;
        public int powerSpeed;
        public int depthLimit;
        public boolean isGpsAdjusting;
        public int powerDepth;
    }
}
