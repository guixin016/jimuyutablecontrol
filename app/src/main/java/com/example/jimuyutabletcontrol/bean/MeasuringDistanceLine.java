package com.example.jimuyutabletcontrol.bean;

import com.baidu.mapapi.map.Overlay;
import com.baidu.mapapi.map.Polyline;
import com.baidu.mapapi.map.PolylineOptions;

/**
 * @author: HDX
 * @date: 2020\5\11 0011
 */
public class MeasuringDistanceLine {
    public PolylineOptions polylineOptions;
    public Overlay polyline;
}
