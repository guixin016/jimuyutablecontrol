package com.example.jimuyutabletcontrol.bean;

import com.baidu.mapapi.map.BitmapDescriptor;
import com.baidu.mapapi.map.Overlay;
import com.baidu.mapapi.map.OverlayOptions;
import com.baidu.mapapi.model.LatLng;

/**
 * @author: HDX
 * @date: 2020\5\11 0011
 */
public class MeasuringDistancePoint {
    public LatLng selfPosition;
    public OverlayOptions overlayOptions;
    public Overlay overlay;
    public BitmapDescriptor bitmapDescriptor;
    public String text;

    public void recycle() {
        selfPosition = null;
        overlayOptions = null;
        if (null != overlay && !overlay.isRemoved()) {
            overlay.remove();
            overlay = null;
        }
        if (null != bitmapDescriptor) {
            bitmapDescriptor.recycle();
            bitmapDescriptor = null;
        }
        text = null;
    }
}
