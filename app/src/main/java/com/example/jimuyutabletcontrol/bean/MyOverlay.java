package com.example.jimuyutabletcontrol.bean;

import com.baidu.mapapi.map.BitmapDescriptor;
import com.baidu.mapapi.map.MarkerOptions;
import com.baidu.mapapi.map.Overlay;

import java.io.Serializable;

/**
 * @author: HDX
 * @date: 2020\4\28 0028
 */
public class MyOverlay implements Serializable {
    public String imei;
    public Overlay targetOverlay;
    public boolean isSelect = false;
    public BitmapDescriptor bitmapDescriptor;
    public MarkerOptions markerOptions;
}
