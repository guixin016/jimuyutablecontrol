package com.example.jimuyutabletcontrol.bean;

import com.baidu.mapapi.map.BitmapDescriptor;
import com.baidu.mapapi.map.MarkerOptions;
import com.baidu.mapapi.map.Overlay;

/**
 * @author: HDX
 * @date: 2020\5\27 0027
 */
public class MyReturnPoint {
    public Overlay targetOverlay;
    public boolean isSelect = false;
    public BitmapDescriptor bitmapDescriptor;
    public MarkerOptions markerOptions;
}
