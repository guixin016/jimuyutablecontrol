package com.example.jimuyutabletcontrol.bean;

import java.io.Serializable;

/**
 * @author: HDX
 * @date: 2020\5\22 0022
 */
public class NavigationDialogTaskInfo implements Serializable {
    public String taskName;
    public boolean isExecute = false;
    public String testParam;
    public double maxTime;
}
