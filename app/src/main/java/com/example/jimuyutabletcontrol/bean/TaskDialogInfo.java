package com.example.jimuyutabletcontrol.bean;

/**
 * @author: HDX
 * @date: 2020\5\21 0021
 */
public class TaskDialogInfo {
    public String fileName;
    public long createTime;
    public boolean isSelected = false;
    /**
     * 是否下载
     */
    public boolean isDownLoad = false;

    /**
     * 是否是pad端规划的文件
     */
    public boolean isPad = true;
}
