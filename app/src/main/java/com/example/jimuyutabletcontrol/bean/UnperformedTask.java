package com.example.jimuyutabletcontrol.bean;

import com.example.jimuyutabletcontrol.network.udp.UdpSocket;

import java.util.List;

/**
 * @author: HDX
 * @date: 2020\6\12 0012
 */
public class UnperformedTask {

    public static UnperformedTask unperformedTask;

    public synchronized static UnperformedTask getInstance() {
        if (null == unperformedTask) {
            unperformedTask = new UnperformedTask();
        }
        return unperformedTask;
    }

    private UnperformedTask() {
    }

    public boolean isDo = true;

    public List<String> imeis;

    /**
     * ==1，直航任务;==2，导航任务
     */
    public int taskType = 1;

    /**
     * 直航所需时间
     */
    public int directTime;
    /**
     * 直航转速
     */
    public int directPowerSpeed;
    /**
     * 直航深度
     */
    public int directDepth;
    /**
     * 直航测试数据
     */
    public String directTestParams;

    //////////////导航数据/////////////////////
    ////////////////////////////////////////////
    public String navigationTaskName;
    /**
     * 最大时间单位分
     */
    public double navigationMaxTime;
    /**
     * 测试数据
     */
    public String navigationTestParam;


    /**
     * 设置直航数据
     */
    public void setDirectRoute(int time, int powerSpeed, int depth, String testParams, List<String> imeis) {
        this.directTime = time;
        this.directPowerSpeed = powerSpeed;
        this.directDepth = depth;
        this.directTestParams = testParams;
        this.imeis = imeis;
        this.taskType = 1;
        this.isDo = false;
    }

    /**
     * 设置导航数据
     */
    public void setNavigation(String taskName, double maxTime, String testParam, List<String> imeis) {
        this.navigationTaskName = taskName;
        this.navigationMaxTime = maxTime;
        this.navigationTestParam = testParam;
        this.imeis = imeis;
        this.taskType = 2;
        this.isDo = false;
    }

    /**
     * 获取任务类型
     *
     * @return
     */
    public int getTaskType() {
        return taskType;
    }

    /**
     * 判断任务是否执行
     *
     * @return
     */
    public boolean isDoTask() {
        return isDo;
    }


    /**
     * 执行命令
     */
    public void doOrder() {
        if (null == imeis || imeis.size() == 0 || isDo) return;
        this.isDo = true;
        for (String imei : imeis) {
            if (taskType == 1) {
                sendOrder("B" + imei + "afa," + directTime + "," + directPowerSpeed + "," + directDepth + "," + directTestParams);
            } else {
                sendOrder("B" + imei + "gps," + "testParam:" +
                        navigationTestParam + ",max_duration_of_voyage:" + navigationMaxTime + ",fileName:" + navigationTaskName);
            }
        }
    }

    private void sendOrder(String order) {
        UdpSocket.getInstance().sendMessage(order);
    }
}
