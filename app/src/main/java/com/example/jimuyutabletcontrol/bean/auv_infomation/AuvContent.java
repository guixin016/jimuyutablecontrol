package com.example.jimuyutabletcontrol.bean.auv_infomation;

/**
 * @author: HDX
 * @date: 2020\6\15 0015
 */
public class AuvContent extends AuvInfoAc{

    public AuvContent(){

    }
    public AuvContent(String name , String value){
        this.name = name;
        this.value = value;
    }

    public String name;
    public String value;
}
