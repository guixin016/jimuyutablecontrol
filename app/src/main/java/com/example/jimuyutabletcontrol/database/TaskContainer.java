package com.example.jimuyutabletcontrol.database;

import com.baidu.mapapi.model.LatLng;

import java.util.ArrayList;

public class TaskContainer {
    private static ArrayList<LatLng> points;
    private static ArrayList<String> depths;
    private static ArrayList<String> rotates;

    public TaskContainer() {
        points = new ArrayList<>();
        depths = new ArrayList<>();
        rotates = new ArrayList<>();
    }

    public void loadData(String gpsInfo) {
        String[] processInfo = gpsInfo.split("\n");
        for(String line : processInfo) {
            String[] contents = line.split(":")[1].split(",");
            points.add(new LatLng(Double.valueOf(contents[0]), Double.valueOf(contents[1])));
            rotates.add(contents[2]);
            depths.add(contents[3]);
        }
    }

    public void cancleSet() {
        int pSize = points.size();
        int rSize = rotates.size();

        if (pSize > 0) {
            if (pSize > rSize) {
                points.remove(pSize - 1);
            }

            if (pSize == rSize) {
                points.remove(pSize -1);
                rotates.remove(pSize -1);
                depths.remove(pSize - 1);
            }
        }
    }

    public void cancleSets() {
        int pSize = points.size();
        if (pSize != 0) {
            points.clear();
            rotates.clear();
            depths.clear();
        }
        points = new ArrayList<>();
        rotates = new ArrayList<>();
        depths = new ArrayList<>();
    }

    public boolean checkData(int position) {
        if (position < rotates.size()) {
            return true;
        } else {
            return false;
        }
    }

    public void addPoint(LatLng point) {
        if (points != null) {
            points.add(point);
        }
    }

    public void addRotate(String rotate) {
        if (rotates != null) {
            rotates.add(rotate);
        }
    }

    public void addDepth(String depth) {
        if (depths != null) {
            depths.add(depth);
        }
    }

    public void updateRotate(String rotate, int position) {
        if (rotates != null) {
            rotates.set(position, rotate);
        }
    }

    public void updateDepth(String depth, int position) {
        if (depths != null) {
            depths.set(position, depth);
        }
    }

    public void updatePoint(LatLng point, int position) {
        if (points != null) {
            points.set(position, point);
        }
    }

    public ArrayList<LatLng> getPoints() {
        return points;
    }

    public ArrayList<String> getRotates() {
        return rotates;
    }

    public ArrayList<String> getDepths() {
        return depths;
    }

    public int getTaskSize() {
        return points.size();
    }

    public int getDepthsSize() {
        return depths.size();
    }

    public int getRotatesSize() {
        return rotates.size();
    }

    public void clear() {
        if (points != null) {
            points.clear();
        }
        if (rotates != null) {
            rotates.clear();
        }
        if (depths != null) {
            depths.clear();
        }
    }
}
