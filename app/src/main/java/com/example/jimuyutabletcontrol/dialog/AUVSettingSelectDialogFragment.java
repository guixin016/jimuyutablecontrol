package com.example.jimuyutabletcontrol.dialog;


import android.annotation.SuppressLint;
import android.content.Context;
import android.graphics.Rect;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.example.jimuyutabletcontrol.R;
import com.example.jimuyutabletcontrol.adapter.setauvtask.SettingAuvListAdapter;
import com.example.jimuyutabletcontrol.bean.MyOverlay;
import com.example.jimuyutabletcontrol.utils.DensityUtil;

import java.util.ArrayList;
import java.util.List;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.DialogFragment;
import androidx.recyclerview.widget.RecyclerView;

/**
 * A simple {@link } subclass.
 */
public class AUVSettingSelectDialogFragment extends DialogFragment implements View.OnClickListener {

    private static final String KEY_PARAM = "key_" + AUVSettingSelectDialogFragment.class.getSimpleName();

    public static AUVSettingSelectDialogFragment getInstance(List<MyOverlay> myOverlays) {
        if (null == myOverlays || myOverlays.size() == 0) return null;
        AUVSettingSelectDialogFragment f = new AUVSettingSelectDialogFragment();
        ArrayList<String> strings = new ArrayList<>();
        for (MyOverlay myOverlay : myOverlays) {
            if (!TextUtils.isEmpty(myOverlay.imei)) {
                strings.add(myOverlay.imei);
            }
        }
        Bundle params = new Bundle();
        params.putStringArrayList(KEY_PARAM, strings);
        f.setArguments(params);
        return f;
    }


    private View rootView;

    private TextView title;

    private TextView routeNavigation;

    private TextView navigation;

    private TextView cancel;

    private RecyclerView mRecyclerView;

    private List<String> infos;

    private SettingAuvListAdapter mAdapter;

    private AUVSettingSelectDialogListener listener;

    private Context mContext;

    @SuppressLint("ValidFragment")
    private AUVSettingSelectDialogFragment() {
        // Required empty public constructor
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        this.mContext = context;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        infos = getArguments().getStringArrayList(KEY_PARAM);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_auvsetting_select_dialog, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        this.rootView = view;
        initView();
        initRecy();
        setListeners();
        setAdapter();
        initData();
    }

    private void initData() {
        if (null == infos) {
            title.setText("下达任务");
        } else {
            title.setText("下达任务(" + infos.size() + ")");
        }
    }

    private void initRecy() {
        mRecyclerView.addItemDecoration(new RecyclerView.ItemDecoration() {
            @Override
            public void getItemOffsets(@NonNull Rect outRect, @NonNull View view,
                                       @NonNull RecyclerView parent, @NonNull RecyclerView.State state) {
                super.getItemOffsets(outRect, view, parent, state);
                int index = parent.getChildAdapterPosition(view);
                if (index == 0) {
                    outRect.left = DensityUtil.dp2px(mContext, 10);
                }
                outRect.right = DensityUtil.dp2px(mContext, 10);
                outRect.bottom = DensityUtil.dp2px(mContext, 10);
                outRect.top = DensityUtil.dp2px(mContext, 10);
            }
        });
    }

    private void setAdapter() {
        mAdapter = new SettingAuvListAdapter(infos);
        mRecyclerView.setAdapter(mAdapter);
    }

//    private void initData() {
//        StringBuilder builder = new StringBuilder();
//        for (String str : infos) {
//            if (infos.indexOf(str) == infos.size() - 1) {
//                builder.append("auv:" + str + "。");
//            } else {
//                builder.append("auv:" + str + "、");
//            }
//        }
//        moreAuvImei.setText(builder.toString());
//    }

    private void setListeners() {
        routeNavigation.setOnClickListener(this);
        navigation.setOnClickListener(this);
        cancel.setOnClickListener(this);
    }

    private void initView() {
        title = rootView.findViewById(R.id.title);
        routeNavigation = rootView.findViewById(R.id.routeNavigation);
        navigation = rootView.findViewById(R.id.navigation);
        cancel = rootView.findViewById(R.id.cancel);
        mRecyclerView = rootView.findViewById(R.id.mRecyclerView);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.routeNavigation:
                if (null != listener && null != infos) {
                    listener.onAUVRouteNavigationListener(infos);
                }
                break;
            case R.id.navigation:
                if (null != listener && null != infos) {
                    listener.onAUVNavigationListener(infos);
                }
                break;
        }
        dismiss();
    }

    public void setOnAUVSettingSelectDialogListener(AUVSettingSelectDialogListener listener) {
        this.listener = listener;
    }


    public interface AUVSettingSelectDialogListener {
        void onAUVRouteNavigationListener(List<String> imeis);

        void onAUVNavigationListener(List<String> imeis);
    }
}
