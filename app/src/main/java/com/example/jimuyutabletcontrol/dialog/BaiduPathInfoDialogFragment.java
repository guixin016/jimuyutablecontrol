package com.example.jimuyutabletcontrol.dialog;


import android.annotation.SuppressLint;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.baidu.mapapi.model.LatLng;
import com.example.jimuyutabletcontrol.R;
import com.example.jimuyutabletcontrol.utils.CoordinateTransformUtil;
import com.example.jimuyutabletcontrol.utils.DecimalFormatUtil;

import java.text.DecimalFormat;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.DialogFragment;

/**
 * A simple {@link } subclass.
 */
public class BaiduPathInfoDialogFragment extends DialogFragment {

    private static final String KEY_START_TEXT = "key_start_text";
    private static final String KEY_END_TEXT = "key_end_text";
    private static final String KEY_START_POINT = "key_start_point";
    private static final String KEY_END_POINT = "key_end_point";
    private static final String KEY_DISTANCE = "key_distance";
    private static final String KEY_SPEED = "key_speed";
    private static final String KEY_TIME = "key_time";
    private static final String KEY_DEPTH = "key_depth";

    public static BaiduPathInfoDialogFragment getInstance(String startText, String endText, LatLng startPoint, LatLng endPoint
            , double distance, double speed, double time, int depth) {
        BaiduPathInfoDialogFragment f = new BaiduPathInfoDialogFragment();
        Bundle params = new Bundle();
        params.putString(KEY_START_TEXT, startText);
        params.putString(KEY_END_TEXT, endText);
        params.putParcelable(KEY_START_POINT, CoordinateTransformUtil.baiduToGps(startPoint));
        params.putParcelable(KEY_END_POINT, CoordinateTransformUtil.baiduToGps(endPoint));
        params.putDouble(KEY_DISTANCE, distance);
        params.putDouble(KEY_SPEED, speed);
        params.putDouble(KEY_TIME, time);
        params.putInt(KEY_DEPTH, depth);
        f.setArguments(params);
        return f;
    }

    private View rootView;

    private TextView tvStartPoint;

    private TextView tvEndPoint;

    private TextView tvDistance;

    private TextView tvSpeed;

    private TextView tvDepth;

    private TextView tvApproximateTime;

    private String startText;

    private String endText;

    private LatLng startPoint;

    private LatLng endPoint;

    private double distance;

    private double speed;

    private double time;

    private int depth;

    @SuppressLint("ValidFragment")
    private BaiduPathInfoDialogFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        startText = getArguments().getString(KEY_START_TEXT);
        endText = getArguments().getString(KEY_END_TEXT);
        startPoint = getArguments().getParcelable(KEY_START_POINT);
        endPoint = getArguments().getParcelable(KEY_END_POINT);
        distance = getArguments().getDouble(KEY_DISTANCE);
        speed = getArguments().getDouble(KEY_SPEED);
        time = getArguments().getDouble(KEY_TIME);
        depth = getArguments().getInt(KEY_DEPTH);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_baidu_path_info_dialog, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        this.rootView = view;
        initView();
        setListeners();
        initData();
    }

    private void initData() {
        LatLng startLatlng = CoordinateTransformUtil.baiduToGps(startPoint);
        LatLng endLatlng = CoordinateTransformUtil.baiduToGps(endPoint);

        DecimalFormat decimalFormat =new DecimalFormat("0.000000");
        tvStartPoint.setText("起始点" + startText + "(" + decimalFormat.format(startLatlng.longitude) + "," + decimalFormat.format(startLatlng.latitude) + ")");
        tvEndPoint.setText("结束点" + endText + "(" + decimalFormat.format(endLatlng.longitude) + "," + decimalFormat.format(endLatlng.latitude) + ")");
        tvDistance.setText("距离：" + DecimalFormatUtil.getFormatData(distance) + "米");
        tvSpeed.setText("速度：" + DecimalFormatUtil.getFormatData(speed) + "m/s");
        tvDepth.setText("深度：" + depth + "米");
        tvApproximateTime.setText("预计到达时间：" + DecimalFormatUtil.getFormatData(time) + "分");
    }

    private void setListeners() {
        rootView.findViewById(R.id.cancel).setOnClickListener(v -> dismiss());
    }

    private void initView() {
        tvStartPoint = rootView.findViewById(R.id.tvStartPoint);
        tvEndPoint = rootView.findViewById(R.id.tvEndPoint);
        tvDistance = rootView.findViewById(R.id.tvDistance);
        tvSpeed = rootView.findViewById(R.id.tvSpeed);
        tvDepth = rootView.findViewById(R.id.tvDepth);
        tvApproximateTime = rootView.findViewById(R.id.tvApproximateTime);
    }
}
