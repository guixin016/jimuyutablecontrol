package com.example.jimuyutabletcontrol.dialog;


import android.content.Context;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.DialogFragment;
import androidx.fragment.app.Fragment;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.example.jimuyutabletcontrol.R;
import com.example.jimuyutabletcontrol.network.udp.UdpSocket;

/**
 * A simple {@link Fragment} subclass.
 */
public class FlickerSettingDialogFragment extends DialogFragment implements View.OnClickListener {

    public static final String KEY_PARAMS = "key_params" + FlickerSettingDialogFragment.class.getSimpleName();

    public static FlickerSettingDialogFragment getInstance(String imei) {
        FlickerSettingDialogFragment f = new FlickerSettingDialogFragment();
        Bundle params = new Bundle();
        params.putString(KEY_PARAMS, imei);
        f.setArguments(params);
        return f;
    }

    private String imei;

    private Context mContext;

    private View rootView;

    private TextView tvTitle;

    public FlickerSettingDialogFragment() {
        // Required empty public constructor
    }

    @Override
    public void onAttach(@NonNull Context context) {
        super.onAttach(context);
        this.mContext = context;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        imei = getArguments().getString(KEY_PARAMS);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_flicker_setting_dialog, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        this.rootView = view;
        initView();
        initData();
        setListeners();
    }

    private void initData() {
        tvTitle.setText("AUV"+imei+"屏幕控制");
    }

    private void setListeners() {
        rootView.findViewById(R.id.ToChangeAn).setOnClickListener(this);
        rootView.findViewById(R.id.toChangeLiang).setOnClickListener(this);
        rootView.findViewById(R.id.toFlicker).setOnClickListener(this);
        rootView.findViewById(R.id.toChangeNormal).setOnClickListener(this);
        rootView.findViewById(R.id.cancel).setOnClickListener(this);
    }

    private void initView() {
        tvTitle = rootView.findViewById(R.id.tvTitle);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.ToChangeAn:
                UdpSocket.getInstance().sendMessage("B"+imei+"toChangeAn");
                dismiss();
                break;
            case R.id.toChangeLiang:
                UdpSocket.getInstance().sendMessage("B"+imei+"toChangeLight");
                dismiss();
                break;
            case R.id.toFlicker:
                UdpSocket.getInstance().sendMessage("B"+imei+"toChangeFlicker");
                dismiss();
                break;
            case R.id.toChangeNormal:
                UdpSocket.getInstance().sendMessage("B"+imei+"toChangeNormal");
                dismiss();
                break;
            case R.id.cancel:
                dismiss();
                break;
            default:
                break;
        }
    }
}
