package com.example.jimuyutabletcontrol.dialog;


import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.DialogFragment;
import androidx.fragment.app.Fragment;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.jimuyutabletcontrol.R;

/**
 * A simple {@link Fragment} subclass.
 */
public class HomeSettingDialogFragment extends DialogFragment implements View.OnClickListener {

    private View rootView;

    private HomeSettingDialogListener listener;

    public HomeSettingDialogFragment() {
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_home_setting_dialog, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        this.rootView = view;
        setListeners();
    }

    private void setListeners() {
        rootView.findViewById(R.id.searchInformation).setOnClickListener(this);
        rootView.findViewById(R.id.taskSetting).setOnClickListener(this);
        rootView.findViewById(R.id.cancel).setOnClickListener(this);
        rootView.findViewById(R.id.otherTaskSetting).setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.searchInformation:
                if (null != listener) {
                    listener.onClickSearchInformationListener();
                }
                dismiss();
                break;
            case R.id.taskSetting:
                if (null != listener) {
                    listener.onClickTaskSettingListener();
                }
                dismiss();
                break;
            case R.id.otherTaskSetting:
                if (null != listener) {
                    listener.onClickOtherTaskSettingListener();
                }
                dismiss();
                break;
            case R.id.cancel:
                dismiss();
                break;
        }
    }

    public void setOnHomeSettingDialogListener(HomeSettingDialogListener listener) {
        this.listener = listener;
    }

    public interface HomeSettingDialogListener {
        void onClickSearchInformationListener();

        void onClickTaskSettingListener();

        void onClickOtherTaskSettingListener();
    }

}
