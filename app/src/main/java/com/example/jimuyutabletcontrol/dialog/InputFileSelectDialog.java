package com.example.jimuyutabletcontrol.dialog;


import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.DialogFragment;
import androidx.fragment.app.Fragment;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.jimuyutabletcontrol.R;

/**
 * A simple {@link Fragment} subclass.
 */
public class InputFileSelectDialog extends DialogFragment {

    private View rootView;

    private OnInputFileSelectListener listener;

    public InputFileSelectDialog() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_input_file_select_dialog, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        this.rootView = view;
        setListeners();
    }

    private void setListeners() {
        rootView.findViewById(R.id.fileInput).setOnClickListener(v -> {
            if (null != listener) {
                listener.onFileInputListener();
            }
            dismiss();
        });
        rootView.findViewById(R.id.cancel).setOnClickListener(v -> dismiss());
    }

    public void setOnInputFileSelectListener(OnInputFileSelectListener listener) {
        this.listener = listener;
    }

    public interface OnInputFileSelectListener {
        void onFileInputListener();
    }

}
