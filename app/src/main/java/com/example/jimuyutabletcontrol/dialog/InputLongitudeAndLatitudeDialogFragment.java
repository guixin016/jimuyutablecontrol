package com.example.jimuyutabletcontrol.dialog;


import android.annotation.SuppressLint;
import android.content.Context;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.baidu.mapapi.model.LatLng;
import com.example.jimuyutabletcontrol.R;
import com.example.jimuyutabletcontrol.utils.CoordinateTransformUtil;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.DialogFragment;

/**
 * A simple {@link } subclass.
 */
public class InputLongitudeAndLatitudeDialogFragment extends DialogFragment implements View.OnClickListener {

    private static final String KEY_PARAM = "key" + InputLongitudeAndLatitudeDialogFragment.class.getSimpleName();

    public static InputLongitudeAndLatitudeDialogFragment getInstance(String text) {
        InputLongitudeAndLatitudeDialogFragment f = new InputLongitudeAndLatitudeDialogFragment();
        Bundle params = new Bundle();
        params.putString(KEY_PARAM, text);
        f.setArguments(params);
        return f;
    }

    private View rootView;

    private TextView title;

    private EditText inputLongitude;

    private EditText inputLatitude;

    private Context mContext;

    private String text;

    private InputLongitudeAndLatitudeDialogListener listener;

    @SuppressLint("ValidFragment")
    private InputLongitudeAndLatitudeDialogFragment() {
        // Required empty public constructor
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        this.mContext = context;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        text = getArguments().getString(KEY_PARAM);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_input_longitude_and_latitude_dialog, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        this.rootView = view;
        initView();
        setListeners();
        initData();
    }

    private void initData() {
        title.setText(text + "点经纬度设置");
    }

    private void setListeners() {
        rootView.findViewById(R.id.cancel).setOnClickListener(this);
        rootView.findViewById(R.id.confirm).setOnClickListener(this);
    }

    private void initView() {
        title = rootView.findViewById(R.id.title);
        inputLongitude = rootView.findViewById(R.id.inputLongitude);
        inputLatitude = rootView.findViewById(R.id.inputLatitude);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.confirm:
                if (null != listener) {
                    String longitude = inputLongitude.getText().toString().trim();
                    String latitude = inputLatitude.getText().toString().trim();
                    if (TextUtils.isEmpty(longitude)) {
                        Toast.makeText(mContext, "请输入经度", Toast.LENGTH_SHORT).show();
                        return;
                    }
                    if (TextUtils.isEmpty(latitude)) {
                        Toast.makeText(mContext, "请输入纬度", Toast.LENGTH_SHORT).show();
                        return;
                    }
                    double lo = Double.valueOf(longitude);
                    double la = Double.valueOf(latitude);
                    if (la > 90 || la < -90 || lo > 180 || lo < -180) {
                        Toast.makeText(mContext, "请输入正确的经纬度", Toast.LENGTH_SHORT).show();
                        return;
                    }
                    listener.onInputLongitudeAndLatitudeDialogListener(CoordinateTransformUtil.gpsToBaidu(new LatLng(la, lo)));
                }
            case R.id.cancel:
                dismiss();
                break;
        }
    }

    public void setOnInputLongitudeAndLatitudeDialogListener(InputLongitudeAndLatitudeDialogListener listener) {
        this.listener = listener;
    }

    public interface InputLongitudeAndLatitudeDialogListener {
        void onInputLongitudeAndLatitudeDialogListener(LatLng point);
    }

}
