package com.example.jimuyutabletcontrol.dialog;


import android.annotation.SuppressLint;
import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.baidu.mapapi.model.LatLng;
import com.example.jimuyutabletcontrol.R;
import com.example.jimuyutabletcontrol.utils.CoordinateTransformUtil;

import java.text.DecimalFormat;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.DialogFragment;

/**
 * A simple {@link } subclass.
 */
public class MakeTaskMapDialogFragment extends DialogFragment implements View.OnClickListener {

    private static final String KEY_TITLE_PARAMS = "key_title" + MakeTaskMapDialogFragment.class.getSimpleName();

    private static final String KEY_POSITION_PARAMS = "key_position" + MakeTaskMapDialogFragment.class.getSimpleName();

    public static MakeTaskMapDialogFragment getInstance(String title, LatLng latLng) {
        MakeTaskMapDialogFragment f = new MakeTaskMapDialogFragment();
        Bundle params = new Bundle();
        params.putString(KEY_TITLE_PARAMS, title);
        params.putParcelable(KEY_POSITION_PARAMS,CoordinateTransformUtil.baiduToGps(latLng));
        f.setArguments(params);
        return f;
    }

    private Context mContext;

    private View rootView;

    private TextView speedDepthSetting;

    private TextView longitudeAndLatitudeSetting;

    private TextView title;

    private TextView cancel;

    private TextView otherSetting;

    private String titleText;

    private MakeTaskMapDialogListener listener;

    private LatLng latLng;

    @SuppressLint("ValidFragment")
    private MakeTaskMapDialogFragment() {
        // Required empty public constructor
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        this.mContext = context;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        titleText = getArguments().getString(KEY_TITLE_PARAMS);
        latLng = getArguments().getParcelable(KEY_POSITION_PARAMS);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_make_task_map_dialog, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        this.rootView = view;
        initView();
        setListeners();
        initData();
    }

    private void initData() {
        title.setText(titleText + "点设置");
        LatLng curLatlng = CoordinateTransformUtil.gpsToBaidu(latLng);
        DecimalFormat decimalFormat =new DecimalFormat("0.000000");
        ((TextView)rootView.findViewById(R.id.tvLongitude)).setText("当前经度："+decimalFormat.format(curLatlng.longitude));
        ((TextView)rootView.findViewById(R.id.tvLatitude)).setText("当前纬度："+decimalFormat.format(curLatlng.latitude));
    }

    private void setListeners() {
        speedDepthSetting.setOnClickListener(this);
        longitudeAndLatitudeSetting.setOnClickListener(this);
        cancel.setOnClickListener(this);
        otherSetting.setOnClickListener(this);
    }

    private void initView() {
        speedDepthSetting = rootView.findViewById(R.id.speedDepthSetting);
        longitudeAndLatitudeSetting = rootView.findViewById(R.id.longitudeAndLatitudeSetting);
        otherSetting = rootView.findViewById(R.id.otherSetting);
        title = rootView.findViewById(R.id.title);
        cancel = rootView.findViewById(R.id.cancel);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.speedDepthSetting:
                if (null != listener) {
                    listener.onSpeedDepthSettingDialogListener(titleText);
                }
                dismiss();
                break;
            case R.id.longitudeAndLatitudeSetting:
                if (null != listener) {
                    listener.onlongitudeAndLatitudeSettingDialogListener(titleText);
                }
                dismiss();
                break;
            case R.id.otherSetting:
                if (null != listener) {
                    listener.onOtherSettingDialogListener(titleText);
                }
                dismiss();
                break;
            case R.id.cancel:
                dismiss();
                break;
        }
    }

    public void setOnMakeTaskMapDialogListener(MakeTaskMapDialogListener listener) {
        this.listener = listener;
    }

    public interface MakeTaskMapDialogListener {
        void onSpeedDepthSettingDialogListener(String pointText);

        void onlongitudeAndLatitudeSettingDialogListener(String pointText);

        void onOtherSettingDialogListener(String pointText);
    }

}
