package com.example.jimuyutabletcontrol.dialog;


import android.content.Context;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.DialogFragment;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;
import androidx.recyclerview.widget.RecyclerView;

import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;

import com.example.jimuyutabletcontrol.MapApplication;
import com.example.jimuyutabletcontrol.R;
import com.example.jimuyutabletcontrol.adapter.navigation_dialog.NavigationAuvDialogAdapter;
import com.example.jimuyutabletcontrol.adapter.navigation_dialog.NavigationTaskDialogAdapter;
import com.example.jimuyutabletcontrol.bean.NavigationDialogAuvInfo;
import com.example.jimuyutabletcontrol.fragment.NavigationDialogTaskFragment;

import java.util.ArrayList;
import java.util.List;

/**
 * A simple {@link Fragment} subclass.
 */
public class NavigationDialogFragment extends DialogFragment {

    public static final String KEY_PARAM = "key_params_" + NavigationDialogFragment.class.getSimpleName();

    public static NavigationDialogFragment getInstance(List<String> imeis) {
        NavigationDialogFragment f = new NavigationDialogFragment();
        Bundle params = new Bundle();
        params.putStringArrayList(KEY_PARAM, (ArrayList<String>) imeis);
        f.setArguments(params);
        return f;
    }

    private View rootView;

    private RecyclerView auvRecyclerView;

    private NavigationAuvDialogAdapter auvDialogAdapter;

    private NavigationTaskDialogAdapter taskDialogAdapter;

    private List<String> taskList;

    private List<String> imeis;

    private Context mContext;

    private List<NavigationDialogAuvInfo> auvInfos;

    private int clickPoint = 0;

    private NavigationDialogFragment() {
        // Required empty public constructor
    }

    @Override
    public void onAttach(@NonNull Context context) {
        super.onAttach(context);
        this.mContext = context;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        imeis = getArguments().getStringArrayList(KEY_PARAM);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_navigation_dialog, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        this.rootView = view;
        initView();
        setAdapter();
        setListeners();
        initData();
    }

    private void setListeners() {
        auvDialogAdapter.setOnNavigationAuvDialogListener(position ->{
            if (clickPoint != position){
                NavigationDialogAuvInfo info = auvInfos.get(position);
                info.isSelected = !info.isSelected;
                auvDialogAdapter.notifyItemChanged(position);
                NavigationDialogAuvInfo lastInfo = auvInfos.get(clickPoint);
                lastInfo.isSelected = !lastInfo.isSelected;
                auvDialogAdapter.notifyItemChanged(clickPoint);
                setFragment(imeis.get(position), clickPoint == -1 ? null : imeis.get(clickPoint));
                clickPoint = position;
            }
        });
        rootView.findViewById(R.id.exit).setOnClickListener(v -> dismiss());
        rootView.findViewById(R.id.refreshData).setOnClickListener(v -> {
            if (null == imeis||imeis.size()==0||imeis.size()<=clickPoint)return;
            String imei = imeis.get(clickPoint);
            MapApplication.refreshFileDataWithImei(imei, true);
        });
    }

    private void initData() {
        for (String imei : imeis) {
            NavigationDialogAuvInfo auvInfo = new NavigationDialogAuvInfo();
            auvInfo.imei = imei;
            auvInfo.isSelected = imeis.indexOf(imei) == 0;
            auvInfos.add(auvInfo);
        }
        auvDialogAdapter.notifyDataSetChanged();
        setFragment(imeis.get(0), null);
    }

    private void setAdapter() {
        auvInfos = new ArrayList<>();
        taskList = new ArrayList<>();
        auvDialogAdapter = new NavigationAuvDialogAdapter(auvInfos);
        auvRecyclerView.setAdapter(auvDialogAdapter);
    }

    private void initView() {
        auvRecyclerView = rootView.findViewById(R.id.auvRecyclerView);
    }

    public void setFragment(String showTag, String HideTag) {
        if (TextUtils.isEmpty(showTag)) return;
        FragmentManager fragmentManager = getChildFragmentManager();
        Fragment showFragment = fragmentManager.findFragmentByTag(showTag);
        Fragment hideFragment = null;
        if (!TextUtils.isEmpty(HideTag)) {
            hideFragment = fragmentManager.findFragmentByTag(HideTag);
        }
        FragmentTransaction transaction = fragmentManager.beginTransaction();
        if (null != hideFragment) {
            transaction.hide(hideFragment);
        }
        if (null == showFragment) {
            transaction.add(R.id.myContainer, NavigationDialogTaskFragment.getInstance(showTag), showTag);
        } else {
            transaction.show(showFragment);
        }
        transaction.commit();
    }
}
