package com.example.jimuyutabletcontrol.dialog;


import android.content.Context;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.DialogFragment;
import androidx.fragment.app.Fragment;

import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.Switch;
import android.widget.TextView;
import android.widget.Toast;

import com.example.jimuyutabletcontrol.R;
import com.example.jimuyutabletcontrol.bean.MakeTaskMapPoint;

/**
 * A simple {@link Fragment} subclass.
 */
public class OtherTaskSettingDialogFragment extends DialogFragment implements View.OnClickListener {

    private static final String KEY_PARAMS = "key_param"+OtherTaskSettingDialogFragment.class.getSimpleName();

    public static OtherTaskSettingDialogFragment getInstance(MakeTaskMapPoint makeTaskMapPoint){
        OtherTaskSettingDialogFragment f = new OtherTaskSettingDialogFragment();
        Bundle params = new Bundle();
        params.putSerializable(KEY_PARAMS,makeTaskMapPoint);
        f.setArguments(params);
        return f;
    }

    private View rootView;

    private TextView tvTitle;

    private Switch gpsSwitch;

    private EditText edDepthLimit;

    private MakeTaskMapPoint makeTaskMapPoint;

    private OtherTaskSettingDialogListener listener;

    private Context mContext;

    private OtherTaskSettingDialogFragment() {
        // Required empty public constructor
    }

    @Override
    public void onAttach(@NonNull Context context) {
        super.onAttach(context);
        this.mContext = context;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        makeTaskMapPoint = (MakeTaskMapPoint) getArguments().getSerializable(KEY_PARAMS);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_other_task_setting_dialog, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        this.rootView = view;
        initView();
        setData();
        setListener();
    }

    private void setListener() {
        rootView.findViewById(R.id.confirm).setOnClickListener(this);
        rootView.findViewById(R.id.cancel).setOnClickListener(this);
    }

    private void setData() {
        tvTitle.setText(makeTaskMapPoint.text+"点设置");
        gpsSwitch.setChecked(makeTaskMapPoint.isGpsAdjusting);
    }

    private void initView() {
        tvTitle = rootView.findViewById(R.id.tvTitle);
        edDepthLimit = rootView.findViewById(R.id.edDepthLimit);
        gpsSwitch = rootView.findViewById(R.id.gpsSwitch);
    }

    public void setOnOtherTaskSettingDialogListener(OtherTaskSettingDialogListener listener){
        this.listener = listener;
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.confirm:
                if (null != listener){
                    String depthLimit = edDepthLimit.getText().toString().trim();
                    if (TextUtils.isEmpty(depthLimit)){
                        Toast.makeText(mContext,"请输入深度限度",Toast.LENGTH_SHORT).show();
                        return;
                    }
                    listener.onOtherTaskSettingDialogListener(Integer.valueOf(depthLimit),gpsSwitch.isChecked());
                }
            case R.id.cancel:
                dismiss();
                break;
        }
    }

    public interface OtherTaskSettingDialogListener{
        void onOtherTaskSettingDialogListener(int depth , boolean isGps);
    }
}
