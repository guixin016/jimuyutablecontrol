package com.example.jimuyutabletcontrol.dialog;


import android.annotation.SuppressLint;
import android.content.Context;
import android.graphics.Rect;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.example.jimuyutabletcontrol.R;
import com.example.jimuyutabletcontrol.adapter.selectauvInfodialog.SelectAuvDialogAdapter;
import com.example.jimuyutabletcontrol.bean.AuvDialogInfo;
import com.example.jimuyutabletcontrol.bean.MyOverlay;
import com.example.jimuyutabletcontrol.utils.DensityUtil;

import java.util.ArrayList;
import java.util.List;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.DialogFragment;
import androidx.recyclerview.widget.RecyclerView;

/**
 * A simple {@link } subclass.
 */
public class SelectAuvDialogFragment extends DialogFragment implements View.OnClickListener {

    private static final String KEY_PARAMS = "key_" + SelectAuvDialogFragment.class.getSimpleName();

    public static SelectAuvDialogFragment getInstance(List<MyOverlay> myOverlays) {
        SelectAuvDialogFragment selectAuvDialogFragment = new SelectAuvDialogFragment();
        ArrayList<AuvDialogInfo> auvDialogInfos = new ArrayList<>();
        for (MyOverlay myOverlay : myOverlays) {
            AuvDialogInfo info = new AuvDialogInfo();
            info.imei = myOverlay.imei;
            info.isSelected = false;
            auvDialogInfos.add(info);
        }
        Bundle params = new Bundle();
        params.putParcelableArrayList(KEY_PARAMS, auvDialogInfos);
        selectAuvDialogFragment.setArguments(params);
        return selectAuvDialogFragment;
    }

    private View rootView;
    private RecyclerView mRecyclerView;

    private List<AuvDialogInfo> infos;

    private SelectAuvDialogAdapter mAdapter;

    private Context mContext;

    private int clickPosition = -1;

    private SelectAuvDialogListener listener;

    @SuppressLint("ValidFragment")
    private SelectAuvDialogFragment() {
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        this.mContext = context;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        assert getArguments() != null;
        infos = getArguments().getParcelableArrayList(KEY_PARAMS);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_select_auv_dialog, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        this.rootView = view;
        initView();
        setAdapter();
        setListeners();
    }

    private void setAdapter() {
        mAdapter = new SelectAuvDialogAdapter(infos);
        mRecyclerView.setAdapter(mAdapter);
    }

    private void setListeners() {
        rootView.findViewById(R.id.confirm).setOnClickListener(this);
        rootView.findViewById(R.id.cancel).setOnClickListener(this);
        mAdapter.setOnSelectAuvDialogListener(position -> {
            if (clickPosition == -1) {
                infos.get(position).isSelected = true;
            } else {
                infos.get(clickPosition).isSelected = false;
                mAdapter.notifyItemChanged(clickPosition, 1);
                infos.get(position).isSelected = true;
            }
            mAdapter.notifyItemChanged(position, 1);
            clickPosition = position;
        });
    }

    private void initView() {
        mRecyclerView = rootView.findViewById(R.id.mRecyclerView);
        mRecyclerView.addItemDecoration(new RecyclerView.ItemDecoration() {
            @Override
            public void getItemOffsets(@NonNull Rect outRect, @NonNull View view,
                                       @NonNull RecyclerView parent, @NonNull RecyclerView.State state) {
                super.getItemOffsets(outRect, view, parent, state);
                int index = parent.indexOfChild(view);
                if (index < infos.size() - 1) {
                    outRect.bottom = DensityUtil.dp2px(mContext, 2);
                }
            }
        });
    }

    private AuvDialogInfo getSelectAuvDialogInfo() {
        for (AuvDialogInfo info : infos) {
            if (info.isSelected) {
                return info;
            }
        }
        return null;
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.confirm:
                AuvDialogInfo info = getSelectAuvDialogInfo();
                if (null == info) {
                    Toast.makeText(mContext, "请选择auv", Toast.LENGTH_SHORT).show();
                    return;
                }
                if (null != listener) {
                    listener.onSelectAuvDialogListener(info);
                }
            case R.id.cancel:
                dismiss();
                break;
        }
    }

    public void setOnSelectAuvDialogListener(SelectAuvDialogListener listener) {
        this.listener = listener;
    }

    public interface SelectAuvDialogListener {
        void onSelectAuvDialogListener(AuvDialogInfo info);
    }

}
