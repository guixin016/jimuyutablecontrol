package com.example.jimuyutabletcontrol.dialog;


import android.content.Context;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.DialogFragment;
import androidx.fragment.app.Fragment;

import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.example.jimuyutabletcontrol.R;
import com.example.jimuyutabletcontrol.bean.NavigationDialogTaskInfo;
import com.example.jimuyutabletcontrol.utils.DecimalFormatUtil;
import com.example.jimuyutabletcontrol.utils.MapUtil;

/**
 * A simple {@link Fragment} subclass.
 */
public class SendNavigationTaskDialogFragment extends DialogFragment implements View.OnClickListener {

    private static final String KEY_PARAMS_TASK_INFO = "key_params_taskInfo" + SendNavigationTaskDialogFragment.class.getSimpleName();

    private static final String KEY_PARAMS_RECOMMEND_TIME = "key_params_recommendTime" + SendNavigationTaskDialogFragment.class.getSimpleName();

    public static SendNavigationTaskDialogFragment getInstance(NavigationDialogTaskInfo taskInfo , double recommendTime) {
        SendNavigationTaskDialogFragment f = new SendNavigationTaskDialogFragment();
        Bundle params = new Bundle();
        params.putSerializable(KEY_PARAMS_TASK_INFO, taskInfo);
        params.putDouble(KEY_PARAMS_RECOMMEND_TIME , recommendTime);
        f.setArguments(params);
        return f;
    }

    private View rootView;

    private TextView fileName;

    private TextView tvMaxTime;

    private TextView tvRecommendTime;

    private EditText testData;

    private NavigationDialogTaskInfo taskInfo;

    private SendNavigationTaskDialogListener listener;

    private Context mContext;

    private double recommendTime;

    private SendNavigationTaskDialogFragment() {
        // Required empty public constructor
    }

    @Override
    public void onAttach(@NonNull Context context) {
        super.onAttach(context);
        this.mContext = context;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        taskInfo = (NavigationDialogTaskInfo) getArguments().getSerializable(KEY_PARAMS_TASK_INFO);
        recommendTime = getArguments().getDouble(KEY_PARAMS_RECOMMEND_TIME);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_send_navigation_task_dialog, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        this.rootView = view;
        initView();
        setListeners();
        initData();
    }

    private void initData() {
        if (null == taskInfo) return;
        fileName.setText("任务文件：" + taskInfo.taskName);
        if (recommendTime == -1){
            tvRecommendTime.setText("推荐时间："+"控制端未发现指定文件");
        } else {
            tvRecommendTime.setText("推荐时间："+DecimalFormatUtil.getFormatData(recommendTime)+"分");
        }
    }

    private void setListeners() {
        rootView.findViewById(R.id.cancel).setOnClickListener(this);
        rootView.findViewById(R.id.confirm).setOnClickListener(this);
    }

    private void initView() {
        fileName = rootView.findViewById(R.id.fileName);
        testData = rootView.findViewById(R.id.testData);
        tvMaxTime = rootView.findViewById(R.id.tvMaxTime);
        tvRecommendTime = rootView.findViewById(R.id.tvRecommendTime);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.confirm:
                if (null != listener && null != taskInfo) {
                    String test = testData.getText().toString().trim();
                    String maxTime = tvMaxTime.getText().toString().trim();
                    taskInfo.testParam = test;
                    taskInfo.maxTime = TextUtils.isEmpty(maxTime)?recommendTime:Double.valueOf(maxTime);
                    listener.onSendNavigationTaskDialogListener(taskInfo);
                }
            case R.id.cancel:
                dismiss();
                break;
        }
    }

    public void setOnSendNavigationTaskDialogListener(SendNavigationTaskDialogListener listener) {
        this.listener = listener;
    }

    public interface SendNavigationTaskDialogListener {
        void onSendNavigationTaskDialogListener(NavigationDialogTaskInfo taskInfo);
    }

}
