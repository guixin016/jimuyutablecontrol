package com.example.jimuyutabletcontrol.dialog;


import android.content.Context;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.DialogFragment;
import androidx.fragment.app.Fragment;

import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.Toast;

import com.example.jimuyutabletcontrol.R;

/**
 * A simple {@link Fragment} subclass.
 */
public class SetSpeedDialogFragment extends DialogFragment implements View.OnClickListener {

    private Context mContext;

    private View rootView;

    private EditText edSpeed;

    private SetSpeedDialogListener listener;

    public SetSpeedDialogFragment() {
        // Required empty public constructor
    }

    @Override
    public void onAttach(@NonNull Context context) {
        super.onAttach(context);
        this.mContext = context;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_set_speed_dialog, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        this.rootView = view;
        initView();
        setListeners();
    }

    private void setListeners() {
        rootView.findViewById(R.id.canel).setOnClickListener(this);
        rootView.findViewById(R.id.confirm).setOnClickListener(this);
    }

    private void initView() {
        edSpeed = rootView.findViewById(R.id.edSpeed);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.confirm:
                String speedStr = edSpeed.getText().toString().trim();
                if (TextUtils.isEmpty(speedStr)||Integer.valueOf(speedStr)==0){
                    Toast.makeText(mContext,"请输入转速",Toast.LENGTH_SHORT).show();
                    return;
                }
                if (null != listener){
                    listener.onSetSpeedDialogListener(Integer.valueOf(speedStr));
                }
            case R.id.canel:
                dismiss();
                break;
        }
    }

    public void setOnSetSpeedDialogListener(SetSpeedDialogListener listener){
        this.listener = listener;
    }

    public interface SetSpeedDialogListener{
        void onSetSpeedDialogListener(int speed);
    }

}
