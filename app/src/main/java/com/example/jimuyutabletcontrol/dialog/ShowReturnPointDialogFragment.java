package com.example.jimuyutabletcontrol.dialog;


import android.content.Context;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.DialogFragment;
import androidx.fragment.app.Fragment;

import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.baidu.mapapi.model.LatLng;
import com.example.jimuyutabletcontrol.R;

/**
 * A simple {@link Fragment} subclass.
 */
public class ShowReturnPointDialogFragment extends DialogFragment implements View.OnClickListener {

    private static final String KEY_PARAMS = "key_params"+ShowReturnPointDialogFragment.class.getSimpleName();

    public static ShowReturnPointDialogFragment getInstance(LatLng latLng){
        ShowReturnPointDialogFragment f = new ShowReturnPointDialogFragment();
        Bundle params = new Bundle();
        params.putParcelable(KEY_PARAMS,latLng);
        f.setArguments(params);
        return f;
    }

    private View rootView;

    private EditText etLongitude,etLatitude;

    private TextView tvLongitude , tvLatitude;

    private LatLng latLng;

    private Context mContext;

    private ShowReturnPointDialogListener listener;

    private ShowReturnPointDialogFragment() {
        // Required empty public constructor
    }

    @Override
    public void onAttach(@NonNull Context context) {
        super.onAttach(context);
        this.mContext = context;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        latLng = getArguments().getParcelable(KEY_PARAMS);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_show_return_point_dialog, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        this.rootView = view;
        initView();
        setListeners();
        initData();
    }

    private void initData() {
        tvLatitude.setText("当前纬度："+latLng.latitude);
        tvLongitude.setText("当前经度："+latLng.longitude);
    }

    private void setListeners() {
        rootView.findViewById(R.id.confirm).setOnClickListener(this);
        rootView.findViewById(R.id.cancel).setOnClickListener(this);
    }

    private void initView() {
        etLongitude = rootView.findViewById(R.id.etLongitude);
        etLatitude = rootView.findViewById(R.id.etLatitude);
        tvLongitude = rootView.findViewById(R.id.tvLongitude);
        tvLatitude = rootView.findViewById(R.id.tvLatitude);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.confirm:
                String longitude = etLongitude.getText().toString().trim();
                String latitude = etLatitude.getText().toString().trim();
                if (TextUtils.isEmpty(longitude)){
                    Toast.makeText(mContext,"请输入经度",Toast.LENGTH_SHORT).show();
                    return;
                }
                if (TextUtils.isEmpty(latitude)){
                    Toast.makeText(mContext,"请输入纬度",Toast.LENGTH_SHORT).show();
                    return;
                }
                if (null != listener){
                    listener.onShowReturnPointDialogListener(new LatLng(Double.valueOf(latitude),Double.valueOf(longitude)));
                }
            case R.id.cancel:
                dismiss();
                break;
        }
    }

    public void setOnShowReturnPointDialogListener(ShowReturnPointDialogListener listener){
        this.listener = listener;
    }

    public interface ShowReturnPointDialogListener{
        void onShowReturnPointDialogListener(LatLng latLng);
    }
}
