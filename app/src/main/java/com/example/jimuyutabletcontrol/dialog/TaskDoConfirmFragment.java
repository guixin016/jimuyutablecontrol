package com.example.jimuyutabletcontrol.dialog;


import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.DialogFragment;
import androidx.fragment.app.Fragment;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.example.jimuyutabletcontrol.R;
import com.example.jimuyutabletcontrol.bean.UnperformedTask;
import com.example.jimuyutabletcontrol.utils.DecimalFormatUtil;

/**
 * A simple {@link Fragment} subclass.
 */
public class TaskDoConfirmFragment extends DialogFragment implements View.OnClickListener {

    public static TaskDoConfirmFragment getInstance() {
        TaskDoConfirmFragment f = new TaskDoConfirmFragment();
        return f;
    }

    private View rootView;

    private TextView title;

    private TextView auvCodes;

    private LinearLayout zhiHanglayout;

    private LinearLayout daoHangLayout;

    private TextView zhiHangTime;

    private TextView zhiHangSpeed;

    private TextView zhiHangDepth;

    private TextView daoHangName;

    private TextView daoHangTime;

    private UnperformedTask unperformedTask;

    private TaskDoConfirmListener listener;

    private TaskDoConfirmFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_task_do_confirm, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        this.rootView = view;
        initView();
        setListeners();
        initData();
    }

    private void initData() {
        unperformedTask = UnperformedTask.getInstance();
        if (null != unperformedTask) {
            if (null != unperformedTask.imeis) {
                StringBuilder builder = new StringBuilder();
                for (String imei : unperformedTask.imeis) {
                    if (unperformedTask.imeis.indexOf(imei) == unperformedTask.imeis.size() - 1) {
                        builder.append(imei);
                    } else {
                        builder.append(imei + "；");
                    }
                }
                auvCodes.setText(builder.toString());
            }
            if (unperformedTask.getTaskType() == 1) {
                title.setText("直航任务");
                zhiHanglayout.setVisibility(View.VISIBLE);
                daoHangLayout.setVisibility(View.GONE);
                setZhiHangData();
            } else {
                title.setText("导航任务");
                zhiHanglayout.setVisibility(View.GONE);
                daoHangLayout.setVisibility(View.VISIBLE);
                setDaoHangData();
            }
        }
    }

    private void setDaoHangData() {
        daoHangName.setText("导航任务：" + unperformedTask.navigationTaskName);
        daoHangTime.setText("推荐时间：" + DecimalFormatUtil.getFormatData(unperformedTask.navigationMaxTime) + "分");
    }

    private void setZhiHangData() {
        zhiHangTime.setText("直航时间：" + unperformedTask.directTime + "秒");
        zhiHangSpeed.setText("直航转速：" + unperformedTask.directPowerSpeed);
        zhiHangDepth.setText("直航深度：" + unperformedTask.directDepth + "米");
    }

    private void setListeners() {
        rootView.findViewById(R.id.cancel).setOnClickListener(this);
        rootView.findViewById(R.id.confirm).setOnClickListener(this);
    }

    private void initView() {
        title = rootView.findViewById(R.id.title);
        auvCodes = rootView.findViewById(R.id.auvCodes);
        zhiHanglayout = rootView.findViewById(R.id.zhiHanglayout);
        daoHangLayout = rootView.findViewById(R.id.daoHangLayout);

        zhiHangTime = rootView.findViewById(R.id.zhiHangTime);
        zhiHangSpeed = rootView.findViewById(R.id.zhiHangSpeed);
        zhiHangDepth = rootView.findViewById(R.id.zhiHangDepth);

        daoHangName = rootView.findViewById(R.id.daoHangName);
        daoHangTime = rootView.findViewById(R.id.daoHangTime);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.confirm:
                if (null != unperformedTask) {
                    if (!unperformedTask.isDoTask()) {
                        UnperformedTask.getInstance().doOrder();
                    }
                }
                if (null != listener) {
                    listener.onDo();
                }
            case R.id.cancel:
                dismiss();
                break;
        }
    }

    public void setOnTaskDoConfirmListener(TaskDoConfirmListener listener) {
        this.listener = listener;
    }

    public interface TaskDoConfirmListener {
        void onDo();
    }
}
