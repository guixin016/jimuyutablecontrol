package com.example.jimuyutabletcontrol.dialog;


import android.content.Context;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.DialogFragment;
import androidx.fragment.app.Fragment;

import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.Switch;
import android.widget.TextView;
import android.widget.Toast;

import com.baidu.mapapi.model.LatLng;
import com.baidu.mapapi.utils.DistanceUtil;
import com.example.jimuyutabletcontrol.R;
import com.example.jimuyutabletcontrol.bean.MakeTaskMapPoint;

/**
 * A simple {@link Fragment} subclass.
 */
public class TaskProjectSettingDialogFragment extends DialogFragment implements View.OnClickListener {

    private static final String KEY_INFO_PARAMS = "key_params_info"+TaskProjectSettingDialogFragment.class.getSimpleName();

    private static final String KEY_CURRENT_POINT_PARAMS = "key_params_current_point"+TaskProjectSettingDialogFragment.class.getSimpleName();

    public static TaskProjectSettingDialogFragment getInstance(MakeTaskMapPoint.PackageAuxiliaryPoint packageAuxiliaryPoint, LatLng currentLatlng){
        TaskProjectSettingDialogFragment f = new TaskProjectSettingDialogFragment();
        Bundle params = new Bundle();
        params.putSerializable(KEY_INFO_PARAMS,packageAuxiliaryPoint);
        params.putParcelable(KEY_CURRENT_POINT_PARAMS,currentLatlng);
        f.setArguments(params);
        return f;
    }

    private View rootView;

    private TextView tvTitle;

    private EditText edSpeed , edDepth , edLongitude , edLatitude , edDepthLimit;

    private Switch gpsSwitch;

    private MakeTaskMapPoint.PackageAuxiliaryPoint packageAuxiliaryPoint;

    private LatLng currentLatlng;

    private TaskProjectSettingDialogListener listener;

    private Context context;

    private TaskProjectSettingDialogFragment() {
        // Required empty public constructor
    }

    @Override
    public void onAttach(@NonNull Context context) {
        super.onAttach(context);
        this.context = context;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        packageAuxiliaryPoint = (MakeTaskMapPoint.PackageAuxiliaryPoint) getArguments().getSerializable(KEY_INFO_PARAMS);
        currentLatlng = getArguments().getParcelable(KEY_CURRENT_POINT_PARAMS);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_task_project_setting_dialog, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        this.rootView = view;
        initView();
        setListeners();
        initData();
    }

    private void initData() {
        if (null == packageAuxiliaryPoint)return;
        tvTitle.setText("任务点"+packageAuxiliaryPoint.text+"参数设置");
        MakeTaskMapPoint.AuxiliaryPoint point = packageAuxiliaryPoint.auxiliaryPoint;
        if (null == point)return;
        if (point.powerSpeed != 0){
            edSpeed.setText(point.powerSpeed+"");
        }
        if (point.powerDepth != 0){
            edDepth.setText(point.powerDepth+"");
        }
        if (point.longitude != 0){
            edLongitude.setText(point.longitude+"");
        }
        if (point.latitude != 0){
            edLatitude.setText(point.latitude+"");
        }
        if (point.depthLimit != 0){
            edDepthLimit.setText(point.depthLimit+"");
        }
        gpsSwitch.setChecked(point.isGpsAdjusting);
    }

    private void setListeners() {
        rootView.findViewById(R.id.cancel).setOnClickListener(this);
        rootView.findViewById(R.id.confirm).setOnClickListener(this);
    }

    private void initView() {
        tvTitle = rootView.findViewById(R.id.tvTitle);
        edSpeed = rootView.findViewById(R.id.edSpeed);
        edDepth = rootView.findViewById(R.id.edDepth);
        edLongitude = rootView.findViewById(R.id.edLongitude);
        edLatitude = rootView.findViewById(R.id.edLatitude);
        edDepthLimit = rootView.findViewById(R.id.edDepthLimit);
        gpsSwitch = rootView.findViewById(R.id.gpsSwitch);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.confirm:
                String longitude = edLongitude.getText().toString().trim();
                if (!TextUtils.isEmpty(longitude)){
                    packageAuxiliaryPoint.auxiliaryPoint.longitude = Double.valueOf(longitude);
                } else {
                    Toast.makeText(context,"请输入经度",Toast.LENGTH_SHORT).show();
                    return;
                }
                String latitude = edLatitude.getText().toString().trim();
                if (!TextUtils.isEmpty(latitude)){
                    packageAuxiliaryPoint.auxiliaryPoint.latitude = Double.valueOf(latitude);
                } else {
                    Toast.makeText(context,"请输入纬度",Toast.LENGTH_SHORT).show();
                    return;
                }
                if (DistanceUtil.getDistance(currentLatlng,
                        new LatLng(packageAuxiliaryPoint.auxiliaryPoint.latitude,packageAuxiliaryPoint.auxiliaryPoint.longitude))>10*1000){
                    Toast.makeText(context,"只能设置周边10km范围",Toast.LENGTH_SHORT).show();
                    return;
                }
                String speed = edSpeed.getText().toString().trim();
                if (!TextUtils.isEmpty(speed)){
                    packageAuxiliaryPoint.auxiliaryPoint.powerSpeed = Integer.valueOf(speed);
                } else {
                    Toast.makeText(context,"请输入转速",Toast.LENGTH_SHORT).show();
                    return;
                }
                String depth = edDepth.getText().toString().trim();
                if (!TextUtils.isEmpty(depth)){
                    Integer de = Integer.valueOf(depth);
                    if (de > 100){
                        Toast.makeText(context,"深度值超范围",Toast.LENGTH_SHORT).show();
                        return;
                    }
                    packageAuxiliaryPoint.auxiliaryPoint.powerDepth = de;
                }
                String depthLimit = edDepthLimit.getText().toString().trim();
                if (!TextUtils.isEmpty(depthLimit)){
                    Integer deLimit = Integer.valueOf(depthLimit);
                    if (deLimit > 100){
                        Toast.makeText(context,"深度限度值超范围",Toast.LENGTH_SHORT).show();
                        return;
                    }
                    if (deLimit <= packageAuxiliaryPoint.auxiliaryPoint.powerDepth){
                        Toast.makeText(context,"深度值不能大于等于深度限制",Toast.LENGTH_SHORT).show();
                        return;
                    }
                    packageAuxiliaryPoint.auxiliaryPoint.depthLimit = deLimit;
                }
                packageAuxiliaryPoint.auxiliaryPoint.isGpsAdjusting = gpsSwitch.isChecked();
                if (null != listener){
                    listener.onTaskProjectSettingDialogListener(packageAuxiliaryPoint);
                }
            case R.id.cancel:
                dismiss();
                break;
        }
    }

    public void setOnTaskProjectSettingDialogListener(TaskProjectSettingDialogListener listener){
        this.listener = listener;
    }

    public interface TaskProjectSettingDialogListener{
        void onTaskProjectSettingDialogListener(MakeTaskMapPoint.PackageAuxiliaryPoint packageAuxiliaryPoint);
    }
}
