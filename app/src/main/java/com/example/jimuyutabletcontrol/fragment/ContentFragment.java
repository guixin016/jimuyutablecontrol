package com.example.jimuyutabletcontrol.fragment;

import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.os.Build;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.TextView;

import com.example.jimuyutabletcontrol.utils.Constants;
import com.example.jimuyutabletcontrol.R;
import com.example.jimuyutabletcontrol.sidemenu.interfaces.ScreenShotable;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.annotation.RequiresApi;
import androidx.fragment.app.Fragment;

public class ContentFragment extends Fragment implements ScreenShotable {
    public static final String CLOSE = "Close";
    public static final String CONTROL = "Control";
    public static final String TASK = "Task";
    public static final String REPORT = "Report";
    public static final String INTRODUCTION = "Introduction";


    private View containerView;
    protected String res;
    private Bitmap bitmap;

    public static ContentFragment newInstance(String res) {
        ContentFragment contentFragment = new ContentFragment();
        Bundle bundle = new Bundle();
        bundle.putString(Integer.class.getName(), res);
        contentFragment.setArguments(bundle);
        return contentFragment;
    }


    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        this.containerView = view.findViewById(R.id.container);
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        res = getArguments().getString(Integer.class.getName());
        System.out.println("res: " + res);
    }

    @RequiresApi(api = Build.VERSION_CODES.O)
    @Override
    public View onCreateView(final LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        /*
        View rootView = inflater.inflate(R.layout., container, false);
        mImageView = (ImageView) rootView.findViewById(R.id.image_content);
        mImageView.setClickable(true);
        mImageView.setFocusable(true);
        mImageView.setImageResource(res);
        return rootView;
        */

        Map<String, String> map;
        List<Map<String, String>> sampleList = new ArrayList<>();

        for (int i = 0; i < Constants.title.get(res).size(); i++) {
            map = new HashMap<>();
            map.put(SampleAdapter.KEY_NAME, Constants.title.get(res).get(i));
            map.put(SampleAdapter.KEY_COLOR, Constants.color.get(res).get(i));
            sampleList.add(map);
        }

        View contentView = inflater.inflate(R.layout.maininterface_fragment, container, false);
        //View contentView = inflater.inflate(R.layout.controlinterface_activity, container, false);


        final ListView listView = (ListView) contentView.findViewById(R.id.list_view);
        listView.setAdapter(new SampleAdapter(contentView.getContext(), R.layout.maininterface_list_item, sampleList));

        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Map<String, Integer> item = (Map<String, Integer>)listView.getItemAtPosition(position);
                try {
                    Class clz = Class.forName(Constants.activity.get(res).get(position));
                    Intent intent = new Intent(inflater.getContext(), clz);
                    inflater.getContext().startActivity(intent);
                } catch (ClassNotFoundException e) {
                    e.printStackTrace();
                }
                //Intent intent = new Intent(inflater.getContext(), TextActivity.class);
                //inflater.getContext().startActivity(intent);
            }
        });

        return contentView;
    }

    @Override
    public void takeScreenShot() {
        Thread thread = new Thread() {
            @Override
            public void run() {
                Bitmap bitmap = Bitmap.createBitmap(containerView.getWidth(),
                        containerView.getHeight(), Bitmap.Config.ARGB_8888);
                Canvas canvas = new Canvas(bitmap);
                containerView.draw(canvas);
                ContentFragment.this.bitmap = bitmap;
            }
        };
        thread.start();
    }

    @Override
    public Bitmap getBitmap() {
        return bitmap;
    }
}


class SampleAdapter extends ArrayAdapter<Map<String, String>> {

    public static final String KEY_NAME = "name";
    public static final String KEY_COLOR = "color";

    private final LayoutInflater mInflater;
    private final List<Map<String, String>> mData;

    public SampleAdapter(Context context, int layoutResourceId, List<Map<String, String>> data) {
        super(context, layoutResourceId, data);
        mData = data;
        mInflater = LayoutInflater.from(context);
    }

    @Override
    public View getView(final int position, View convertView, @NonNull ViewGroup parent) {
        final ViewHolder viewHolder;
        if (convertView == null) {
            viewHolder = new ViewHolder();
            convertView = mInflater.inflate(R.layout.maininterface_list_item, parent, false);
            viewHolder.textViewName = (TextView) convertView.findViewById(R.id.text_view_name);
            convertView.setTag(viewHolder);
        } else {
            viewHolder = (ViewHolder) convertView.getTag();
        }

        //viewHolder.textViewName.setText(mData.get(position).get(KEY_NAME));
        //convertView.setBackgroundResource(mData.get(position).get(KEY_COLOR));
        viewHolder.textViewName.setText(mData.get(position).get(KEY_NAME));
        convertView.setBackgroundColor(Color.parseColor(mData.get(position).get(KEY_COLOR)));

        return convertView;
    }

    class ViewHolder {
        TextView textViewName;
    }
}


