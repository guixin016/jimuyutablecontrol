package com.example.jimuyutabletcontrol.fragment;

import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.os.Build;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.jimuyutabletcontrol.R;
import com.example.jimuyutabletcontrol.sidemenu.interfaces.ScreenShotable;

import androidx.annotation.Nullable;
import androidx.annotation.RequiresApi;
import androidx.fragment.app.Fragment;


public class SplitFragment extends Fragment implements ScreenShotable {
    public static final String CLOSE = "Close";

    public static final String CONTROLACTIVITY = "ControlActivity";
    public static final String ONLINEAUVACTVITY = "OnlineAUVActivity";
    public static final String SAILTASKACTIVITY = "SailTaskActivity";

    public static final String SELECTAUVMAKEMAPACTIVITY = "SelectAUVMakeMapActivity";
    public static final String SELECTAUVFIXMAPACTIVITY = "SelectAUVFixMapActivity";
    public static final String SELECTAUVSENDMAPACTIVITY = "SelectAUVSendMapActivity";
    public static final String MAPPINGACTIVITY = "MappingActivity";

    public static final String SELECTAUVANALYSEACTIVITY = "SelectAUVAnalyseActivity";
    public static final String SELECTAUVRECORDDATAACTIVITY = "SelectAUVRecordDataActivity";

    private View containerView;
    protected String res;
    private Bitmap bitmap;

    public static SplitFragment newInstance(String res) {
        SplitFragment splitFragment = new SplitFragment();
        Bundle bundle = new Bundle();
        bundle.putString(Integer.class.getName(), res);
        splitFragment.setArguments(bundle);
        return splitFragment;
    }


    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        this.containerView = view.findViewById(R.id.container);
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        res = getArguments().getString(Integer.class.getName());
        System.out.println("res: " + res);
    }

    @RequiresApi(api = Build.VERSION_CODES.O)
    @Override
    public View onCreateView(final LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        /*
        View rootView = inflater.inflate(R.layout., container, false);
        mImageView = (ImageView) rootView.findViewById(R.id.image_content);
        mImageView.setClickable(true);
        mImageView.setFocusable(true);
        mImageView.setImageResource(res);
        return rootView;
        */

        View contentView = null;
        if (res.contentEquals(SplitFragment.CONTROLACTIVITY)) {
            contentView = inflater.inflate(R.layout.controlinterface_activity, container, false);
        } else if (res.contentEquals(SplitFragment.ONLINEAUVACTVITY)) {
            contentView = inflater.inflate(R.layout.onlineinterface_pull_to_refresh, container, false);
        } else if (res.contentEquals(SplitFragment.SAILTASKACTIVITY)) {
            contentView = inflater.inflate(R.layout.sailtaskinterface_activity, container, false);
        }

        //View contentView = inflater.inflate(R.layout.controlinterface_activity, container, false);


        return contentView;
    }

    @Override
    public void takeScreenShot() {
        Thread thread = new Thread() {
            @Override
            public void run() {
                Bitmap bitmap = Bitmap.createBitmap(containerView.getWidth(),
                        containerView.getHeight(), Bitmap.Config.ARGB_8888);
                Canvas canvas = new Canvas(bitmap);
                containerView.draw(canvas);
                SplitFragment.this.bitmap = bitmap;
            }
        };
        thread.start();
    }

    @Override
    public Bitmap getBitmap() {
        return bitmap;
    }
}
