package com.example.jimuyutabletcontrol.heartbeatModule;

/**
 * @author: HDX
 * @date: 2020\4\1 0001
 */
public interface HeartbeatCallback {
    void onHeartbeat();
}
