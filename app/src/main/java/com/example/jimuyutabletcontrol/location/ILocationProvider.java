package com.example.jimuyutabletcontrol.location;

import android.location.Location;

/**
 * @author: HDX
 * @date: 2020\5\27 0027
 */
public interface ILocationProvider {
    void startLocation();

    void stopLocation();

    Location getLastLocation();

    void setLocationOutSideInterface(LocationInterface locationOutSideInterface);

    void removeLocationOutSideInterface(LocationInterface locationOutSideInterface);

    void removeAllLocationOutSideInterface();

    double[] getValue();
}
