package com.example.jimuyutabletcontrol.location;

import android.location.Location;

/**
 * @author: HDX
 * @date: 2020\5\27 0027
 */
public interface LocationInterface {
    void showLocation(Location location);
}
