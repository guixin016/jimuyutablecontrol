package com.example.jimuyutabletcontrol.main;

import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.jimuyutabletcontrol.FileActivity;
import com.example.jimuyutabletcontrol.R;
import com.example.jimuyutabletcontrol.activity.ControlActivity;
import com.example.jimuyutabletcontrol.activity.MakeTaskMapActivity;
import com.example.jimuyutabletcontrol.activity.SelectAUVRecordDataActivity;
import com.example.jimuyutabletcontrol.sidemenu.interfaces.Resourceble;
import com.example.jimuyutabletcontrol.sidemenu.interfaces.ScreenShotable;
import com.example.jimuyutabletcontrol.sidemenu.util.ViewAnimator;
import com.example.jimuyutabletcontrol.utils.AppVersionUtils;
import com.example.jimuyutabletcontrol.utils.Constants;
import com.example.jimuyutabletcontrol.utils.Permission;


import java.util.ArrayList;

import androidx.annotation.Nullable;
import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;

public class HomeScreenActivity extends AppCompatActivity implements ViewAnimator.ViewAnimatorListener {

    private ImageView control;
    private ImageView plan;
    private ImageView data;

    @RequiresApi(api = Build.VERSION_CODES.O)
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.homescreeninterface_activity);

        control = findViewById(R.id.control);
        plan = findViewById(R.id.plan);
        data = findViewById(R.id.data);
        ((TextView)findViewById(R.id.text)).
                setText("西安天和防务股份有限公司  版权所有   V"+ AppVersionUtils.getAppVersionName(this));

        control.setOnClickListener(v -> startActivity(new Intent(this, ControlActivity.class)));

        plan.setOnClickListener(v -> startActivity(new Intent(this, MakeTaskMapActivity.class)));

        data.setOnClickListener(v -> startActivity(new Intent(this, SelectAUVRecordDataActivity.class)));
        Permission.requestPermission(this);
    }

    @Override
    protected void onResume() {
        super.onResume();
    }

    @Override
    public ScreenShotable onSwitch(Resourceble slideMenuItem, ScreenShotable screenShotable, int position) {
        return null;
    }

    @Override
    public void disableHomeButton() {

    }

    @Override
    public void enableHomeButton() {

    }

    @Override
    public void addViewToContainer(View view) {

    }
}
