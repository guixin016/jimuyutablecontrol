package com.example.jimuyutabletcontrol.network;

import com.example.jimuyutabletcontrol.activity.SelectAUVRecordDataActivity;
import com.example.jimuyutabletcontrol.utils.Constants;
import com.example.jimuyutabletcontrol.utils.MsgType;

import java.io.BufferedWriter;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.Socket;

public enum HFTCPBuild {
    INSTANCE;

    private HFTCPServer hftcpServer;

    HFTCPBuild() {
        hftcpServer = new HFTCPServer();
    }

    public HFTCPServer getTcpServer() {
        return hftcpServer;
    }

    public class HFTCPServer {
        private boolean threadRunning;
        private Socket socket;
        private String getDataFileName = null;
        private DataInputStream dataInputStream;

        private HFTCPServer() {
            threadRunning = true;
        }

        public void tcpWorker() {
            //reconnect();
            //receiveMessage();
        }


        public void sendMessage (final String message) {
            new Thread(new Runnable() {
                @Override
                public void run() {
                    Socket socket = null;
                    try {
                        socket = new Socket(Constants.TCP_SERVER_IP, Constants.TCP_SERVER_PORT);
                    } catch (IOException e) {
                        e.printStackTrace();
                    }

                    getDataFileName = message;
                    DataOutputStream pw = null;
                    try {
                        pw = new DataOutputStream(socket.getOutputStream());
                        System.out.println("tcp order: " + "get," + message);
                        pw.writeUTF("get," + message);
                        pw.close();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
            }).start();

        }

        public void receiveMessage () {
            threadRunning = true;
            new Thread(new Runnable() {
                public void run() {
                    File pathDS;
                    File pathDH;
                    try {
                        if (socket != null) {
                            socket.close();
                            socket = null;
                        }
                        socket = new Socket(Constants.TCP_SERVER_IP, Constants.TCP_SERVER_PORT);
                        OutputStream outputStream = socket.getOutputStream();
                        DataOutputStream dataOutputStream = new DataOutputStream(outputStream);
                        dataOutputStream.writeUTF("dataclient");
                        InputStream inputStream = socket.getInputStream();
                        dataInputStream = new DataInputStream(inputStream);
                        pathDH = new File(Constants.FILE_PATH + Constants.DH);
                        pathDS = new File(Constants.FILE_PATH + Constants.DS);
                        if (!pathDH.exists()) {
                            pathDH.mkdirs();
                        }
                        if (!pathDS.exists()) {
                            pathDS.mkdirs();
                        }

                    } catch (IOException e) {
                        e.printStackTrace();
                    }


                    while (threadRunning) {

                        try {

                            //System.out.println("1 getDataFileName: " + getDataFileName);
                            if (getDataFileName != null) {
                                File file = null;
                                BufferedWriter bw = null;
                                String content = dataInputStream.readUTF();

                                System.out.println("content: " + content);
                                while (content != null) {
                                    if (content.contains(".txt")) {
                                        if (content.contains("DH")) {
                                            file = new File(Constants.FILE_PATH + Constants.DH + getDataFileName);
                                        } else {
                                            file = new File(Constants.FILE_PATH + Constants.DS + getDataFileName);
                                        }
                                        bw = new BufferedWriter(new FileWriter(file));

                                    } else if (content.contains("END")) {

//                                        if (SelectAUVRecordDataActivity.getInstance() != null) {
//                                            SelectAUVRecordDataActivity.getInstance().runOnUiThread(new Runnable() {
//                                                @Override
//                                                public void run() {
//                                                    SelectAUVRecordDataActivity.getInstance().hud.dismiss();
//                                                    SelectAUVRecordDataActivity.getInstance().sendMsgToMainThread(MsgType.DOWNLOADDOEN, "下载完毕");
//                                                }
//                                            });
//                                        }

                                        bw.close();
                                        threadRunning = false;
                                        content = null;
                                        break;
                                    } else {
                                        bw.write(content + "\n");
                                        bw.flush();
                                    }
                                    content = dataInputStream.readUTF();
                                }
                            }
                        } catch (IOException e) {
                            e.printStackTrace();
                        }
                    }
                }
            }).start();

        }
    }
}

