package com.example.jimuyutabletcontrol.network.tcp;

import android.text.TextUtils;

import java.util.ArrayList;
import java.util.List;

/**
 * @author: HDX
 * @date: 2020\4\28 0028
 */
public abstract class ITCPSocket {

    protected List<TCPSocket.NetTask> netTasks;

    protected abstract void down();

    public ITCPSocket() {
        netTasks = new ArrayList<>();
    }

    public void setNetTask(String fileName, Runnable runnable,Runnable faileRunnable) {
        if (TextUtils.isEmpty(fileName)) return;
        boolean isHas = false;
        for (TCPSocket.NetTask task : netTasks) {
            if (fileName.equals(task.fileName)) {
                isHas = true;
            }
        }
        if (!isHas) {
            netTasks.add(new TCPSocket.NetTask(fileName, runnable,faileRunnable));
        }
    }

    public void downloadFile() {
        if (netTasks.size() == 0) return;
        down();
    }

    public static class NetTask {
        public String fileName;
        public Runnable runnable;

        public Runnable faileRunnable;

        public NetTask(String fileName, Runnable runnable,Runnable faileRunnable) {
            this.fileName = fileName;
            this.runnable = runnable;
            this.faileRunnable = faileRunnable;
        }
    }
}
