package com.example.jimuyutabletcontrol.network.tcp;



import java.net.Socket;

/**
 * @author: HDX
 * @date: 2020\4\9 0009
 */
public class TCPSocket extends ITCPSocket {

    private static ITCPSocket tcpSocket;

    private Socket socket;

    public synchronized static ITCPSocket getInstance() {
        if (null == tcpSocket) {
            tcpSocket = new TCPSocket();
        }
        return tcpSocket;
    }

    private TCPSocket() {
        super();
    }

    private int downPosition = 0;

    @Override
    public void down() {
        FileDown.getInstance().startDown(netTasks.get(downPosition), () -> {
            if (downPosition + 1 >= netTasks.size()) {
                netTasks.clear();
                downPosition = 0;
            } else {
                down();
                downPosition += 1;
            }
        });
    }
}
