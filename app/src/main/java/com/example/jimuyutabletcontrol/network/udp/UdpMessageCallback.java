package com.example.jimuyutabletcontrol.network.udp;


/**
 * @author: HDX
 * @date: 2020\4\22 0022
 */
public interface UdpMessageCallback {
    void onMessage(String msg);
}
