package com.example.jimuyutabletcontrol.network.udp;



/**
 * @author: HDX
 * @date: 2020\4\27 0027
 */
public class UdpMessageHandle implements UdpMessageCallback {

    private UdpMessageType messageType;

    private UdpMessageCallback udpMessageCallback;

    public UdpMessageHandle(UdpMessageType messageType, UdpMessageCallback udpMessageCallback) {
        this.messageType = messageType;
        this.udpMessageCallback = udpMessageCallback;
    }

    public UdpMessageCallback getUdpMessageCallback() {
        return udpMessageCallback;
    }

    @Override
    public void onMessage(String msg) {
        if (null != udpMessageCallback) {
            if (msg.contains(messageType.type)) {
                udpMessageCallback.onMessage(msg);
            }
        }
    }
}
