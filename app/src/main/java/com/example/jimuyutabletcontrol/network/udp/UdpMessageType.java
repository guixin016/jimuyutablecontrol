package com.example.jimuyutabletcontrol.network.udp;

/**
 * @author: HDX
 * @date: 2020\4\27 0027
 */
public enum UdpMessageType {
    IMEI("IMEI"),
    ANSWER("Answer#AUV"),
    LOCATION("Location"),
    TXT(".txt"),
    CHECK("CHECK"),
    HEARTBEAT("D1"),
    TASK_LIST("taskFileList_with-_-"),
    TASK_CONTENT("taskFileContent-_-"),
    DIAN_LIANG("AuvAndPhoneElectricQuantity-_-");
    public String type;

    UdpMessageType(String type) {
        this.type = type;
    }
}
