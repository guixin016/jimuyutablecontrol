package com.example.jimuyutabletcontrol.port;

import java.util.List;
import java.util.Map;

/**
 * @author: HDX
 * @date: 2020\5\21 0021
 */
public interface TaskFileNameCallback {
    void onTaskFileNameCallback(Map<String, List<String>> taskFiles);
}
