package com.example.jimuyutabletcontrol.pulltorefresh;

public interface Pullable {

    boolean canPullDown();

    boolean canPullUp();
}
