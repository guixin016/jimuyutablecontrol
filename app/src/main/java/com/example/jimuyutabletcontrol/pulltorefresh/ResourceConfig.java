package com.example.jimuyutabletcontrol.pulltorefresh;


import com.example.jimuyutabletcontrol.R;

public abstract class ResourceConfig {

    public int[] getImageResIds() {
        return configImageResIds() == null || configImageResIds().length != 5 ?
                configImageResIdsByDefault() : configImageResIds();
    }

    public int[] getTextResIds() {
        return configTextResIds() == null || configTextResIds().length != 10 ?
                configTextResIdsByDefault() : configTextResIds();
    }

    public abstract int[] configImageResIds();

    public abstract int[] configTextResIds();

    private int[] configImageResIdsByDefault() {
        return new int[]{R.drawable.pulltorefresh_arrow, R.drawable.pulltorefresh_refresh_succeeded,
                R.drawable.pulltorefresh_refresh_failed, R.drawable.pulltorefresh_load_succeeded,
                R.drawable.pulltorefresh_load_failed};
    }

    private int[] configTextResIdsByDefault() {
        return new int[]{R.string.pulltorefresh_pull_to_refresh, R.string.pulltorefresh_release_to_refresh,
                R.string.pulltorefresh_refreshing, R.string.pulltorefresh_refresh_succeeded, R.string.pulltorefresh_refresh_failed,
                R.string.pulltorefresh_pull_up_to_load, R.string.pulltorefresh_release_to_load, R.string.pulltorefresh_loading,
                R.string.pulltorefresh_load_succeeded, R.string.pulltorefresh_load_failed};
    }
}
