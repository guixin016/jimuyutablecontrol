package com.example.jimuyutabletcontrol.sensor;

import android.annotation.SuppressLint;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;

import com.example.jimuyutabletcontrol.MainActivity;
import com.example.jimuyutabletcontrol.MapApplication;
import com.example.jimuyutabletcontrol.main.HomeScreenActivity;

import static android.content.Context.LOCATION_SERVICE;

public class LocationSensor {

    private LocationManager locationManager;
    private double values[] = new double[3];
    private static int startGPS = 0;

    private LocationListener locationListener = new LocationListener() {
        @Override
        public void onLocationChanged(final Location location) {
            if (location != null) {
                values[0] = location.getLongitude();
                values[1] = location.getLatitude();
                values[2] = location.getAccuracy();
            }
            System.out.println("*************************locationChange");
        }

        @Override
        public void onStatusChanged(String provider, int status, Bundle extras) {
        }

        @Override
        public void onProviderEnabled(String provider) {

        }

        @Override
        public void onProviderDisabled(String provider) {

        }
    };

    public LocationSensor() {
        locationManager = (LocationManager) MapApplication.getContext().getSystemService(LOCATION_SERVICE);
    }

    @SuppressLint("MissingPermission") //
    public void startLocation() {
        /*
        ConnectivityManager connectivityManager = (ConnectivityManager)
                mainActivity.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetWorkInfo = connectivityManager.getActiveNetworkInfo();
        */



        /*
        if(startGPS == 0 || activeNetWorkInfo == null || (activeNetWorkInfo.getType() != ConnectivityManager.TYPE_MOBILE
                && activeNetWorkInfo.getType() != ConnectivityManager.TYPE_WIFI)) {
            locationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER,
                    1000,
                    0,
                    locationListener);
            location = locationManager.getLastKnownLocation(LocationManager.GPS_PROVIDER);
            System.out.println("I'm in GPS part");
        } else {
            startGPS --;
            locationManager.requestLocationUpdates(LocationManager.NETWORK_PROVIDER,
                    1000,//1s获取10次位置信息
                    10,
                    //位置监听器
                    locationListener);

            //从GPS获取最新的定位信息
            location = locationManager.getLastKnownLocation(LocationManager.NETWORK_PROVIDER);
            System.out.println("I'm in WIFI part");

        }
        */

        final Location location;

        locationManager = (LocationManager) MapApplication.getContext().getSystemService(LOCATION_SERVICE);

        locationManager.requestLocationUpdates(LocationManager.NETWORK_PROVIDER, 1000, 0, locationListener);

        location = locationManager.getLastKnownLocation(LocationManager.NETWORK_PROVIDER);

        if (location != null) {
            values[0] = location.getLongitude();
            values[1] = location.getLatitude();
            values[2] = location.getAccuracy();
        }
        locationManager.removeUpdates(locationListener);
        locationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, 0, 20, locationListener);
    }

    public void stopLocation() {
        System.out.println("stopLocation");
        if (locationManager != null) {
            locationManager.removeUpdates(locationListener);
            locationManager = null;
        }
    }

    @SuppressLint("MissingPermission")
    public double[] getValue() {
        if (locationManager != null) {
            final Location location = locationManager.getLastKnownLocation(LocationManager.GPS_PROVIDER);
            if (location != null) {
                values[0] = location.getLongitude();
                values[1] = location.getLatitude();
                values[2] = location.getAccuracy();

            }
            return values;
        }
        return values;
    }
}