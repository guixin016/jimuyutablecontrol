package com.example.jimuyutabletcontrol.sidemenu.interfaces;

import android.graphics.Bitmap;

public interface ScreenShotable {
    public void takeScreenShot();

    public Bitmap getBitmap();
}
