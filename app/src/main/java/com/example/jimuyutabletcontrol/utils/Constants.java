package com.example.jimuyutabletcontrol.utils;

import android.Manifest;
import android.os.Environment;

import com.baidu.mapapi.model.LatLng;
import com.example.jimuyutabletcontrol.MapApplication;

import java.util.ArrayList;
import java.util.HashMap;


public class Constants {
    public static HashMap<String, ArrayList<String>> color = new HashMap<>();
    public static HashMap<String, ArrayList<String>> title = new HashMap<>();
    public static HashMap<String, ArrayList<String>> activity = new HashMap<>();

    public static HashMap<String, ArrayList<LatLng>> auvDots;
    public static HashMap<String, Integer> auvColors;

    public static final String FILE_PATH = Environment.getExternalStorageDirectory().getAbsolutePath() +
            "/" + MapApplication.getContext().getPackageName() + "/";
    public static final String MAPPINGFOLDER = "MAPPING/";
    public static final String MAPPINGFILE = "mapping.txt";
    public static final String TASKMAPFOLDER = "TASKMAP/";//pad端规划的任务文件夹
    public static final String TASK_AUV = "TASK_AUV/";//auv端的任务文件夹

    public static final String TASKFILE = "taskmap.txt";
    public static final String TASK_SIGN = "Mission_";

    public static final String DS = "AUV_DATA_DS/";
    public static final String DH = "AUV_DATA_DH/";

    //192.168.43.34
    public static final String UDP_SERVER_IP = "118.24.72.202";

//    public static final String UDP_SERVER_IP = "192.168.43.34";
    public static final String TCP_SERVER_IP = "118.24.72.202";

    //public static final String UDP_SERVER_IP = "192.168.43.91";
    //public static final String TCP_SERVER_IP = "192.168.43.91";

    public static final int UDP_SERVER_PORT = 2000;
    public static final int TCP_SERVER_PORT = 20000;

    public static String phoneIMEI ;//TODO

    public final static String[] PERMISSIONS = {
            Manifest.permission.ACCESS_FINE_LOCATION,
            Manifest.permission.ACCESS_COARSE_LOCATION,
            Manifest.permission.ACCESS_LOCATION_EXTRA_COMMANDS,
            Manifest.permission.READ_PHONE_STATE,
            Manifest.permission.ACCESS_NETWORK_STATE,
            Manifest.permission.WRITE_EXTERNAL_STORAGE,
            Manifest.permission.READ_EXTERNAL_STORAGE,
            Manifest.permission.ACCESS_WIFI_STATE,
            Manifest.permission.ACCESS_FINE_LOCATION};

    /**
     * rotate speed 系数
     */
    public static final double ROTATE_SPEED_AND_SPEED = 0.5;
}
