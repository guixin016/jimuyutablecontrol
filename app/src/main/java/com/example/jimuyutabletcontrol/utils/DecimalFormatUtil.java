package com.example.jimuyutabletcontrol.utils;

import java.text.DecimalFormat;

/**
 * @author: HDX
 * @date: 2020\5\29 0029
 */
public class DecimalFormatUtil {
    public static String getFormatData(double value){
        return new DecimalFormat("0.00").format(value);
    }
}
