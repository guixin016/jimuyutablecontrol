package com.example.jimuyutabletcontrol.utils;

import android.content.Context;
import android.util.TypedValue;

/**
 * @ProjectName: PosApp
 * @Package: com.halixun.posapp.utils
 * @ClassName: DensityUtil
 * @Description: java类作用描述
 * @Author: hdx
 * @CreateDate: 2019/12/31 16:14
 * @UpdateUser: hdx
 * @UpdateDate: 2019/12/31 16:14
 * @UpdateRemark: 更新说明：
 * @Version: 1.0
 */
public class DensityUtil {
    private DensityUtil() {
        throw new UnsupportedOperationException("cannot be instantiated");
    }

    /**
     * dp转px
     */
    public static int dp2px(Context context, float dpVal) {
        return (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP,
                dpVal, context.getResources().getDisplayMetrics());
    }

    /**
     * sp转px
     */
    public static int sp2px(Context context, float spVal) {
        return (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_SP,
                spVal, context.getResources().getDisplayMetrics());
    }

    /**
     * px转dp
     */
    public static float px2dp(Context context, float pxVal) {
        final float scale = context.getResources().getDisplayMetrics().density;
        return (pxVal / scale);
    }

    /**
     * px转sp
     */
    public static float px2sp(Context context, float pxVal) {
        return (pxVal / context.getResources().getDisplayMetrics().scaledDensity);
    }
}
