package com.example.jimuyutabletcontrol.utils;

import android.util.Log;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;

/**
 * @author: HDX
 * @date: 2020\5\14 0014
 */
public class FileParser {
    public static void LocationFileParser(String path) {
        File file = new File(path);
        if (file.exists()) {
            try {
                BufferedReader reader = new BufferedReader(new FileReader(file));
                StringBuilder builder = new StringBuilder();
                String line = reader.readLine();
                while (null != line) {
                    Log.e("萨达", line);
                    builder.append(line + "\n");
                    line = reader.readLine();
                }
            } catch (FileNotFoundException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }
}
