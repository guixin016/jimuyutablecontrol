package com.example.jimuyutabletcontrol.utils;

import android.annotation.SuppressLint;
import android.content.Context;
import android.os.Build;
import android.telephony.TelephonyManager;
import android.text.TextUtils;
import android.util.Log;

import com.example.jimuyutabletcontrol.MapApplication;

import java.lang.reflect.Method;

/**
 * @author: HDX
 * @date: 2020\4\28 0028
 */
public class IMEIUtils {
    /**
     * 方案一（10.0无法获取到IMEI）
     *
     * @return
     */
    @SuppressLint({"MissingPermission", "HardwareIds"})
    public synchronized static String getIMEI() {
        if (!TextUtils.isEmpty(Constants.phoneIMEI)) return Constants.phoneIMEI;
        String imei = "";
        try {
            TelephonyManager tm = (TelephonyManager) MapApplication.getContext().getSystemService(Context.TELEPHONY_SERVICE);
            if (Build.VERSION.SDK_INT < Build.VERSION_CODES.LOLLIPOP) {
                if (null != tm) {
                    imei = tm.getDeviceId();
                }
            } else {
                if (null != tm) {
                    Method method = tm.getClass().getMethod("getImei");
                    imei = (String) method.invoke(tm);
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        if (TextUtils.isEmpty(imei)){
            imei = getDeviceId();
        }
        Log.e("getIMEI",imei+"");
        Constants.phoneIMEI = imei;
        return imei;
    }

    /**
     * 获取设备唯一标识符(方案二)
     *
     * @return 唯一标识符
     */
    @SuppressLint("HardwareIds")
    private static String getDeviceId() {
        String m_szDevIDShort = "35" + Build.BOARD.length() % 10
                + Build.BRAND.length() % 10 + Build.CPU_ABI.length() % 10
                + Build.DEVICE.length() % 10 + Build.DISPLAY.length() % 10
                + Build.HOST.length() % 10 + Build.ID.length() % 10
                + Build.MANUFACTURER.length() % 10 + Build.MODEL.length() % 10
                + Build.PRODUCT.length() % 10 + Build.TAGS.length() % 10
                + Build.TYPE.length() % 10 + Build.USER.length() % 10;// 13 位
        return m_szDevIDShort;
    }
}
