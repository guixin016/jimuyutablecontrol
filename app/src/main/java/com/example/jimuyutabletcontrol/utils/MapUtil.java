package com.example.jimuyutabletcontrol.utils;

import android.util.Log;

import com.baidu.mapapi.model.LatLng;
import com.baidu.mapapi.utils.DistanceUtil;
import com.example.jimuyutabletcontrol.bean.MakeTaskMapPoint;
import com.example.jimuyutabletcontrol.location.LocationProvider;

import java.util.List;

/**
 * @author: HDX
 * @date: 2020\5\29 0029
 */
public class MapUtil {
    public int getDistance(LatLng first, LatLng second) {
        if (null == first || null == second) return 0;
        return (int) DistanceUtil.getDistance(first, second);//单位：米
    }

    public static double getDistance2(LatLng first, LatLng second) {
        if (null == first || null == second) return 0;
        return DistanceUtil.getDistance(first, second);//单位：米
    }

    public static double getNeedTime(double speed, double distance) {
        if (speed == 0 || distance == 0) return 0;
        return distance / speed;
    }

    public static double getTotalTime(List<MakeTaskMapPoint.PackageAuxiliaryPoint> points) {
        if (null == points ) return 0;
        double totalTime = 0;
        double[] values = LocationProvider.getInstance().getValue();
        double curLongitud = values[0];
        double curLatitude = values[1];
        MakeTaskMapPoint.PackageAuxiliaryPoint firstPoint = points.get(0);
        if (curLongitud != firstPoint.auxiliaryPoint.longitude||curLatitude != firstPoint.auxiliaryPoint.latitude){
            double speed = getSpeedWithZhuanSpeed(firstPoint.auxiliaryPoint.powerSpeed);
            if (speed > 0) {
                double distance = getDistance2(new LatLng(curLatitude,curLongitud),
                        new LatLng(firstPoint.auxiliaryPoint.latitude,firstPoint.auxiliaryPoint.longitude));
                double time = distance / speed;
                Log.e("粉等扥",time+"");
                totalTime += time;
            }
        }
        int len = points.size();
        for (int i = 0; i < len; i++) {
            if (i + 1 < len) {
                MakeTaskMapPoint.PackageAuxiliaryPoint startPoint = points.get(i);
                MakeTaskMapPoint.PackageAuxiliaryPoint endPoint = points.get(i + 1);
                double speed = getSpeedWithZhuanSpeed(endPoint.auxiliaryPoint.powerSpeed);
                if (speed > 0) {
                    LatLng startLatlng = new LatLng(startPoint.auxiliaryPoint.latitude, startPoint.auxiliaryPoint.longitude);
                    LatLng endLatlng = new LatLng(endPoint.auxiliaryPoint.latitude, endPoint.auxiliaryPoint.longitude);
                    double distance = getDistance2(startLatlng, endLatlng);
                    double time = distance / speed;
                    totalTime += time;
                }
            }
        }
        return (totalTime == 0 ? 0 : totalTime/60);
    }

    public static double getTotalTime2(List<MakeTaskMapPoint> points) {
        if (null == points || points.size() < 2) return 0;
        int len = points.size();
        double totalTime = 0;
        for (int i = 0; i < len; i++) {
            if (i + 1 < len) {
                MakeTaskMapPoint startPoint = points.get(i);
                MakeTaskMapPoint endPoint = points.get(i + 1);
                double speed = getSpeedWithZhuanSpeed(endPoint.powerSpeed);
                double time = 0;
                if (speed > 0) {
                    double distance = getDistance2(startPoint.selfPosition, endPoint.selfPosition);
                    time = distance / speed;
                }
                totalTime += time;
            }
        }
        return (totalTime == 0 ? 0 : totalTime/60);
    }

    /**
     * 返回值为米/分
     * @param zhuanSpeed
     * @return
     */
    public static double getSpeedWithZhuanSpeed(int zhuanSpeed) {
        if (zhuanSpeed == 0) return 0;
        if (zhuanSpeed <= 500) {
            return 0.5;
        } else if (zhuanSpeed <= 1000) {
            return 1;
        } else if (zhuanSpeed <= 1500) {
            return 1.5;
        } else if (zhuanSpeed <= 2000) {
            return 2;
        } else if (zhuanSpeed <= 2500) {
            return 2.5;
        } else if (zhuanSpeed <= 3000) {
            return 3;
        } else if (zhuanSpeed <= 3500) {
            return 3.5;
        } else return 4;
    }

}
