package com.example.jimuyutabletcontrol.utils;

public class MsgType {
    public static final int LABELWAVEDRAWABLESTOP = 0;
    public static final int AUVWAVEDRAWABLESTOP = 1;
    public static final int AUVSMOVE = 2;
    public static final int AUVREFRESH = 3;
    public static final int ADDAUVINFORMATION = 4;
    public static final int TASKREFRESH = 5;
    public static final int ADDDOT = 6;
    public static final int ADDLOCATION = 7;
    public static final int AUVMAKEMAPREFRESH = 8;
    public static final int AUVFIXMAPREFRESH = 9;
    public static final int AUVSENDMAPREFRESH = 10;
    public static final int AUVRECORDDATAREFRESH = 11;
    public static final int AUVRECORDFILENAMEREFRESH = 12;
    public static final int AUVANALYSEREFRESH = 13;
    public static final int DOWNLOADDOEN = 14;
}
