package com.example.jimuyutabletcontrol.utils;
import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.pm.PackageManager;
import android.os.Build;

import java.util.ArrayList;
import java.util.List;

import androidx.annotation.RequiresApi;
import androidx.core.app.ActivityCompat;
import androidx.core.content.PermissionChecker;

public class Permission {

    @SuppressLint("WrongConstant")
    private static boolean checkPermission(String[] permissions, Activity activity) {
        if (Build.VERSION.SDK_INT < Build.VERSION_CODES.M) {
            return true;
        }

        for (String permission : permissions) {
            if (PermissionChecker.checkSelfPermission(activity, permission) != PackageManager.PERMISSION_GRANTED) {
                return false;
            }
        }
        return true;
    }

    @SuppressLint("WrongConstant")
    private static List<String> getDeniedPermission(String[] permissions ,Activity activity) {
        List<String> needRequestPermissionList = new ArrayList<>();
        for (String permission : permissions) {
            if (PermissionChecker.checkSelfPermission(activity, permission) != PackageManager.PERMISSION_GRANTED
                    || ActivityCompat.shouldShowRequestPermissionRationale(activity, permission)) {
                System.out.println(permission);
                needRequestPermissionList.add(permission);
            }
        }
        return needRequestPermissionList;
    }

    @RequiresApi(api = Build.VERSION_CODES.O)
    public static void requestPermission(Activity activity) {
        if (checkPermission(Constants.PERMISSIONS,activity)) {

        } else {
            List<String> needPermissions = getDeniedPermission(Constants.PERMISSIONS,activity);
            ActivityCompat.requestPermissions(activity, needPermissions.toArray(new String[needPermissions.size()]),
                    1);
        }
    }
}
