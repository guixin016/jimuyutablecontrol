package com.example.jimuyutabletcontrol.utils;

import android.util.Log;

import com.example.jimuyutabletcontrol.bean.MakeTaskMapPoint;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * @author: HDX
 * @date: 2020\5\11 0011
 */
public class TaskUtil {
    /**
     * 保存gps任务数据
     *
     * @param content
     */
    public static void saveTask(String content) {
        String taskPath = Constants.FILE_PATH + Constants.TASKMAPFOLDER;
        String fileName = Constants.TASK_SIGN + DateUtil.date2Str(new Date(), "yyyy_MM_dd_HH_mm");
        File pathFile = new File(taskPath);
        if (!pathFile.exists()) {
            pathFile.mkdirs();
        }
        File file = new File(pathFile, fileName + ".txt");
        boolean isOk = false;
        if (!file.exists()) {
            try {
                isOk = file.createNewFile();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        if (isOk) {
            BufferedWriter bufferedWriter = null;
            try {
                bufferedWriter = new BufferedWriter(new FileWriter(file));
                bufferedWriter.write(content);
            } catch (IOException e) {
                e.printStackTrace();
            } finally {
                if (null != bufferedWriter) {
                    try {
                        bufferedWriter.flush();
                        bufferedWriter.close();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
            }
        }
    }

    /**
     * 保存gps任务数据
     */
    public static String getTaskUrlWithName(String fileName) {
        String taskPath = Constants.FILE_PATH + Constants.TASKMAPFOLDER;
        File pathFile = new File(taskPath);
        if (!pathFile.exists()) {
            return null;
        }
        File file = new File(pathFile, fileName);
        if (!file.exists()) {
            return null;
        }
        return file.getAbsolutePath();
    }

    /**
     * 从文件里获取任务信息并解析
     *
     * @param url
     * @return
     */
    public static List<MakeTaskMapPoint.PackageAuxiliaryPoint> getTaskWithTaskFile(String url) {
        File file = new File(url);
        if (file.exists()) {
            BufferedReader reader = null;
            try {
                reader = new BufferedReader(new FileReader(file));
                return new Gson().fromJson(reader, new TypeToken<ArrayList<MakeTaskMapPoint.PackageAuxiliaryPoint>>() {
                }.getType());
            } catch (FileNotFoundException e) {
                e.printStackTrace();
            } finally {
                if (null != reader) {
                    try {
                        reader.close();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
            }
        }
        return null;
    }

    /**
     * 从文件里获取任务信息并解析
     *
     * @return
     */
    public static List<MakeTaskMapPoint.PackageAuxiliaryPoint> getTaskWithString(String content) {
        return new Gson().fromJson(content, new TypeToken<ArrayList<MakeTaskMapPoint.PackageAuxiliaryPoint>>() {
        }.getType());
    }



    public static List<File> getFileListData(String path, List<String> limits) {
        List<File> files = new ArrayList<>();
        File file = new File(path);
        if (!file.exists()) return files;
        if (file.isDirectory()) {
            File[] fs;
            if (limits == null || limits.size() == 0) {
                fs = file.listFiles();
            } else {
                fs = file.listFiles(pathname -> {
                    if (pathname.isDirectory()) {
                        return true;
                    }
                    return isOk(pathname, limits);
                });
            }
            if (null != fs && fs.length > 0) {
                for (File f : fs) {
                    if (!f.isDirectory()) {
                        files.add(f);
                    }
                }
            }
        } else {
            if (limits == null || limits.size() == 0) {
                files.add(file);
            } else {
                if (isOk(file, limits)) {
                    files.add(file);
                }
            }
        }
        return files;
    }

    private static boolean isOk(File file, List<String> limits) {
        boolean ok = true;
        for (String l : limits) {
            if (!file.getName().contains(l)) {
                ok = false;
            }
        }
        return ok;
    }


    /**
     * 保存auv端的任务文件
     *
     * @param content
     */
    public static void saveAuvTask(String content, String imei, String fileName) {
        String taskPath = Constants.FILE_PATH + Constants.TASK_AUV + imei + "/";
        String fileUrl = (fileName.contains(Constants.TASK_SIGN) ? "" : Constants.TASK_SIGN) + fileName + (fileName.contains(".txt") ? "" : ".txt");
        File pathFile = new File(taskPath);
        if (!pathFile.exists()) {
            pathFile.mkdirs();
        }
        File file = new File(pathFile, fileUrl);
        boolean isOk = false;
        if (!file.exists()) {
            try {
                isOk = file.createNewFile();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        if (isOk) {
            BufferedWriter bufferedWriter = null;
            try {
                bufferedWriter = new BufferedWriter(new FileWriter(file));
                bufferedWriter.write(content);
            } catch (IOException e) {
                e.printStackTrace();
            } finally {
                if (null != bufferedWriter) {
                    try {
                        bufferedWriter.flush();
                        bufferedWriter.close();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
            }
        }
    }


    public static String getAuvTaskUrlWithName(String imei, String fileName) {
        String taskPath = Constants.FILE_PATH + Constants.TASK_AUV + imei + "/";
        File pathFile = new File(taskPath);
        if (!pathFile.exists()) {
            return null;
        }
        String fileUrl = (fileName.contains(Constants.TASK_SIGN) ? "" : Constants.TASK_SIGN) + fileName + (fileName.contains(".txt") ? "" : ".txt");
        File file = new File(pathFile, fileUrl);
        if (!file.exists()) return null;
        return file.getAbsolutePath();
    }

    public static boolean isHasAuvTaskFileWithName(String fileName, String imei) {
        String path = Constants.FILE_PATH + Constants.TASK_AUV + imei + "/";
        File f = new File(path);
        if (!f.exists()) {
            return false;
        }
        String[] fs = f.list((dir, name) -> fileName.equals(name));
        return fs != null && fs.length > 0;
    }

    public static void deleteAuvTaskFileWithName(String fileName, String imei) {
        String taskPath = Constants.FILE_PATH + Constants.TASK_AUV + imei + "/";
        File pathFile = new File(taskPath);
        if (!pathFile.exists()) {
            return;
        }
        String fileUrl = (fileName.contains(Constants.TASK_SIGN) ? "" : Constants.TASK_SIGN) + fileName + (fileName.contains(".txt") ? "" : ".txt");
        File file = new File(taskPath,fileUrl);
        if (file.exists()){
            file.delete();
        }
    }
}
