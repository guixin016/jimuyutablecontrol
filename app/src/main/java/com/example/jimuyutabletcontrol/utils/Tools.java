package com.example.jimuyutabletcontrol.utils;

import com.baidu.mapapi.model.LatLng;
import com.baidu.mapapi.utils.CoordinateConverter;

import java.io.File;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class Tools {

    public static DateFormat df = new SimpleDateFormat("yyyy-MM-dd-HH-mm-ss");
    public static DateFormat dfS = new SimpleDateFormat("yyyy-MM-dd-HH-mm-ss-S");

    public static String DateFormat(Date date, String choice) {
        DateFormat mdf = null;
        if (choice.equals("s")) {
            mdf = df;
        } else {
            mdf = dfS;
        }
        if (date != null && mdf != null) {
            return mdf.format(date);
        }
        return null;
    }

    public static Long getSecondTime(String tag) {
        Date date = null;
        try {
            date = df.parse(tag.split("\\_")[1]);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return date.getTime();
    }

    public static List<String> getOrderList(List<String> fileName, List<Long> secondTime) {
        int length = secondTime.size();
        for (int i = 0; i < length - 1; i ++) {
            for (int j = i + 1; j < length; j ++) {
                if (secondTime.get(i) < secondTime.get(j)) {
                    long tmp = secondTime.get(i);
                    secondTime.set(i, secondTime.get(j));
                    secondTime.set(j, tmp);
                    String tmp1 = fileName.get(i);
                    fileName.set(i, fileName.get(j));
                    fileName.set(j, tmp1);
                }
            }
        }
        return fileName;
    }

    public static LatLng gpsToBaidu(LatLng point) {
        CoordinateConverter converter = new CoordinateConverter();
        converter.from(CoordinateConverter.CoordType.GPS);
        converter.coord(point);
        return converter.convert();
    }

    public static ArrayList<String> getAllFileName() {
        File filesDS = new File(Constants.FILE_PATH + Constants.DS);
        File[] DS = filesDS.listFiles();
        File filesDH = new File(Constants.FILE_PATH + Constants.DH);
        File[] DH = filesDH.listFiles();

        if(DS == null || DH == null) {
            return null;
        }

        ArrayList<String> files = new ArrayList<>();

        for(File file : DS) {
            files.add(file.getName());
        }

        for(File file : DH) {
            files.add(file.getName());
        }

        return files;
    }
}
